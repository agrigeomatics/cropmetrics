define([
    "dojo/_base/array",
    "dojo/_base/Color",
    "dojo/_base/declare",
    "dojo/_base/lang"
], function(array, Color, declare, lang) {

    return declare([], {

        constructor: function (params, parentNode) {
            // jshint unused: false

            // Use one serie for all classes
            this.useOneSerie = false;
        },

        /**
         * Check if the chart has classes data input
         *
         * @return {boolean} return true if the chart has classes data input
         */
        useClasses: function() {
            return typeof this.classes !== "undefined";
        },

        hasClasses: function() {
            return array.some(this.classes, function(item) {
                return typeof item.data !== "undefined" && item.data.length > 0;
            });
        },

        /**
         * Create chart series data based on classes data input
         */
        createClasses: function() {
            var result = this._calculateClasses();
            this.stats = result.stats;
            var total =  result.total;

            var colors = this._getColors();

            var counterX = 1;

            this.minY = Infinity;
            this.minX = Infinity;
            this.maxX = -Infinity;
            this.maxY = -Infinity;

            var series = [];
            var serie = {name: "Serie A", values: []};

            if (this.useOneSerie) {
                series.push(serie);
            }

            for (var item in this.stats) {
                var label = item;
                if (this.labels) {
                    label = this.labels[item];
                }

                var value = result.stats[item];
                var percent = (value / total) * 100;
                if (this.percentages) {
                    value = percent;
                }
                var tooltipText = this.formatTooltipText(value, label);

                // Store min-max x & y
                if (this.minY >= value) {
                    this.minY = value;
                }
                if (this.maxY <= value) {
                    this.maxY = value;
                }
                if (this.minX >= counterX) {
                    this.minX = counterX;
                }
                if (this.maxX <= counterX) {
                    this.maxX = counterX;
                }

                if (!this.useOneSerie) {
                    serie = { name: label, values: []};
                }

                serie.values.push({
                    x: counterX,
                    y: value,
                    tooltip: tooltipText,
                    text: label,
                    stroke: { color: "black", width: 1 },
                    fill: colors[item] || this.defaultFill
                });

                if (!this.useOneSerie) {
                    series.push(serie);
                }

                counterX += 1;
            }
            array.forEach(series, lang.hitch(this, function(serie) {
                this.chart.addSeries(serie.name, serie.values);
            }));
        },

        /**
         * Read the geometries from classes geometry input and return graphics
         *
         * @return {Array} return graphics array
         */
        getGraphicsClasses: function() {
            var graphics = [];
            var colors = this._getColors();
            array.forEach(this.classes, lang.hitch(this, function(classParam) {
                var useGeometry = typeof classParam.geometry !== "undefined";
                if (useGeometry) {
                    var hasData = typeof classParam.data !== "undefined";
                    if (hasData) {
                        array.forEach(classParam.data, lang.hitch(this, function(feature) {
                            var geometry = feature[classParam.geometry];
                            var color = colors[feature[classParam.class]];
                            var graphic = this._createGraphic(geometry, color);
                            if (typeof graphic !== "undefined") {
                                graphics.push(graphic);
                            }
                        }));
                    }
                    else {
                        var geometry = classParam.geometry;
                        var color = colors[classParam.class];
                        var graphic = this._createGraphic(geometry, color);
                        if (typeof graphic !== "undefined") {
                            graphics.push(graphic);
                        }
                    }
                }
            }));
            return graphics;
        },

        _getColors: function() {
            var colors;
            if (this.colors) {
                colors = {};
                for (var key in this.colors) {
                    if (this.colors.hasOwnProperty(key)) {
                        var colorCode = this.colors[key];
                        var color = new Color(colorCode);
                        colors[key] = color;
                    }
                }
            }
            return colors;
        },

        _calculateClasses: function () {
            var dict = {};
            var total = 0;
            array.forEach(this.classes, lang.hitch(this, function(classParam) {
                var useClassesValues = typeof classParam.class_value !== "undefined";

                var hasData = typeof classParam.data !== "undefined";
                if (hasData) {
                    array.forEach(classParam.data, lang.hitch(this, function(feature) {
                        var category = feature[classParam.class];
                        if (useClassesValues) {
                            var value = feature[classParam.class_value];
                            if (dict.hasOwnProperty(category)) {
                                dict[category] = dict[category] + value;
                            }
                            else {
                                dict[category] = value;
                            }
                            total = total + value;
                        }
                        else {
                            if (dict.hasOwnProperty(category)) {
                                dict[category] = dict[category] + 1;
                            }
                            else {
                                dict[category] = 1;
                            }
                            total = total + 1;
                        }
                    }));
                } else if (useClassesValues) {
                    var category = classParam.class;
                    if (dict.hasOwnProperty(category)) {
                        var value = classParam.class_value;
                        if (dict.hasOwnProperty(category)) {
                            dict[category] = dict[category] + value;
                        }
                        else {
                            dict[category] = value;
                        }
                        total = total + value;
                    }
                }
            }));
            return { stats: dict, total: total };
        }
    });

});
