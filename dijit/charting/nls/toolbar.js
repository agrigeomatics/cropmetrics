define({
    root: {
        show_graphics: "Show map graphics",
        hide_graphics: "Hide map graphics",
        full_screen: "Open in full screen",
        close: "Close the chart"
    },
    fr: true
});
