define({
    root: {
        type: {
            pie: "Pie Chart",
            line: "Line Chart",
            column: "Column Chart",
            histogram: "Histogram"
        }
    }
});
