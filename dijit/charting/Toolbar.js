define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-geometry",
    "dojo/dom-style",
    "dojo/aspect",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/keys",
    "dojo/parser",
    "dojo/ready",
    "dijit/Dialog",
    "dijit/form/Button",
    "dijit/form/ToggleButton",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetBase",
    "dijit/_WidgetsInTemplateMixin",
    "dojox/widget/_Invalidating",
    "dojo/text!./templates/toolbar.html",
    "dojo/i18n!./nls/toolbar",
    "xstyle/css!./css/toolbar.css"
], function(array, declare, lang, domGeom, domStyle, aspect, domClass, domConstruct, keys, parser, ready, Dialog, Button,
            ToggleButton, _TemplatedMixin, _WidgetBase, _WidgetsInTemplateMixin, _Invalidating, template, i18n) {

    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, _Invalidating], {
        templateString: template,
        i18n: i18n,
        widgetsInTemplate: true,

       /**
       * Create a controls section
       *
       * @param  {Object} params
       *  mandatory: {Any} chart ColumnsChart, LineChart, PieChart or RasterHistogram
       * @param  {DOMNode | String} srcNodeRef
       */
        constructor: function (params, srcNodeRef) {
            //jshint unused:false
        },

        postCreate: function() {
            this.inherited(arguments);

            this.mapGraphicButton.set("label", i18n.show_graphics);
            this.fullscreenButton.set("label", i18n.full_screen);
            this.closeButton.set("label", i18n.close);

            var onChangeDraw = function (currentButton, isChecked) {
                //jshint unused:false
                this._buttonEsriChecked(this.mapGraphicButton);
                // Show or hide map graphics
                this.chart.toggleGraphics();
                // Change label
                this.toggleMapGraphicsLabel(isChecked);
            };
            this.own(aspect.after(this.mapGraphicButton, "onChange", lang.hitch(this, onChangeDraw, this.mapGraphicButton), true));
            this.own(this.mapGraphicButton.on("mouseenter", lang.hitch(this, this._buttonEsriHoverEnter, this.mapGraphicButton)));
            this.own(this.mapGraphicButton.on("mouseleave", lang.hitch(this, this._buttonEsriHoverLeave, this.mapGraphicButton)));
            this.own(this.mapGraphicButton.on("focus", lang.hitch(this, this._buttonEsriHoverEnter, this.mapGraphicButton)));
            this.own(this.mapGraphicButton.on("blur", lang.hitch(this, this._buttonEsriHoverLeave, this.mapGraphicButton)));

            // Create common mouse and keyboard events
            this.createDisplayEventsButton(this.fullscreenButton);
            this.createDisplayEventsButton(this.closeButton);

            this.own(this.fullscreenButton.on("click", lang.hitch(this, function () {
                this.showFullScreenDialog();
            })));

            this.own(this.closeButton.on("click", lang.hitch(this, function () {
                this.chart.destroy();
            })));
        },

        startup: function() {
            this.inherited(arguments);
        },

		resize: function(box){
			if(box){
				domGeom.setContentSize(this.domNode, box);
				this.invalidateRendering();
			}
		},

        toggleMapGraphicsLabel: function(isChecked) {
            if (isChecked) {
                this.mapGraphicButton.set("label", i18n.hide_graphics);
            }
            else {
                this.mapGraphicButton.set("label", i18n.show_graphics);
            }
        },

        _buttonEsriChecked: function (currentButton) {
            var isChecked = domClass.contains(currentButton.domNode, "esriButtonChecked");
            var isHover = domClass.contains(currentButton.domNode, "esriButtonHover");
            if (!isChecked) {
                domClass.add(currentButton.domNode, "esriButtonChecked");
                if (isHover) {
                    domClass.remove(currentButton.domNode, "esriButtonHover");
                }
            }
            else {
                domClass.remove(currentButton.domNode, "esriButtonChecked");
                if (!isHover) {
                    domClass.add(currentButton.domNode, "esriButtonHover");
                }
            }
        },

        _buttonEsriHoverEnter: function (currentButton, evt) {
            //jshint unused:false
            var isChecked = domClass.contains(currentButton.domNode, "esriButtonChecked");
            var isActive = domClass.contains(currentButton.domNode, "esriButtonActive");

            if (!isChecked && !isActive) {
                var isHover = domClass.contains(currentButton.domNode, "esriButtonHover");
                if (!isHover) {
                    domClass.add(currentButton.domNode, "esriButtonHover");
                    domClass.add(currentButton.domNode, "dijitHover");
                }
            }
        },

        _buttonEsriHoverLeave: function (currentButton, evt) {
            //jshint unused:false
            var isChecked = domClass.contains(currentButton.domNode, "esriButtonChecked");
            var isActive = domClass.contains(currentButton.domNode, "esriButtonActive");
            var isHover = domClass.contains(currentButton.domNode, "esriButtonHover");

            if (!isChecked && !isActive) {
                if (isHover) {
                    domClass.remove(currentButton.domNode, "esriButtonHover");
                    domClass.remove(currentButton.domNode, "dijitHover");
                }
            }
        },

        _buttonEsriHoverLeaveDesactivate: function (currentButton, evt) {
            //jshint unused:false
            this.buttonEsriDesactivate(currentButton);
            this._buttonEsriHoverLeave(currentButton);
        },

        buttonEsriActivate: function (currentButton, event) {
            //jshint unused:false

            var isActive = domClass.contains(currentButton.domNode, "esriButtonActive");
            if (!isActive) {
                domClass.add(currentButton.domNode, "esriButtonActive");
            }

            var isHover = domClass.contains(currentButton.domNode, "esriButtonHover");
            if (isHover) {
                domClass.remove(currentButton.domNode, "esriButtonHover");
            }
        },

        buttonEsriDesactivate: function (currentButton, event) {
            //jshint unused:false

            var isActive = domClass.contains(currentButton.domNode, "esriButtonActive");
            if (isActive) {
                domClass.remove(currentButton.domNode, "esriButtonActive");
            }

            if (currentButton.hovering) {
                var isHover = domClass.contains(currentButton.domNode, "esriButtonHover");
                if (!isHover) {
                    domClass.add(currentButton.domNode, "esriButtonHover");
                }
            }
        },

        createDisplayEventsToggle: function (button) {
            this.own(button.on("keydown", lang.hitch(this, function (evt) {
                var isEnterOrSpace = evt.keyCode === keys.SPACE || evt.keyCode === keys.ENTER;
                if (isEnterOrSpace) {
                    this._buttonEsriChecked(button, evt);
                }
            })));
            this.own(button.on("keyup", lang.hitch(this, function (evt) {
                var isEnterOrSpace = evt.keyCode === keys.SPACE || evt.keyCode === keys.ENTER;
                if (isEnterOrSpace) {
                    this._buttonEsriChecked(button, evt);
                }
            })));
        },

        createDisplayEventsButton: function (button) {
            this.own(button.on("mouseenter", lang.hitch(this, this._buttonEsriHoverEnter, button)));
            this.own(button.on("mouseleave", lang.hitch(this, this._buttonEsriHoverLeaveDesactivate, button)));
            this.own(button.on("focus", lang.hitch(this, this._buttonEsriHoverEnter, button)));
            this.own(button.on("blur", lang.hitch(this, this._buttonEsriHoverLeaveDesactivate, button)));
            this.own(button.on("mousedown", lang.hitch(this, this.buttonEsriActivate, button)));
            this.own(button.on("mouseup", lang.hitch(this, this.buttonEsriDesactivate, button)));
            this.own(button.on("keydown", lang.hitch(this, function (evt) {
                var isEnterOrSpace = evt.keyCode === keys.SPACE || evt.keyCode === keys.ENTER;
                if (isEnterOrSpace) {
                    this.buttonEsriActivate(button, evt);
                }
            })));
            this.own(button.on("keyup", lang.hitch(this, function (evt) {
                var isEnterOrSpace = evt.keyCode === keys.SPACE || evt.keyCode === keys.ENTER;
                if (isEnterOrSpace) {
                    this.buttonEsriDesactivate(button, evt);
                }
            })));
        },

        showFullScreenDialog: function () {
            var fullscreenDialog = new Dialog({
                style: "width: 80%; height: 80%; border: 1px solid #A8A8A8;",
                autofocus: true
            });
            fullscreenDialog.placeAt(document.body);
            fullscreenDialog.startup();
            fullscreenDialog.show();

            // Clone the chart
            var params = { showControls: false, fullscreen: true };
            var clone = this.chart.clone(params, fullscreenDialog.containerNode);
            clone.create();

            var dialogBoxParent = domGeom.getContentBox(fullscreenDialog.containerNode.parentNode);

            // remove padding 15px * 2 = 30px;
            var padding = 15;
            var w = clone.getWidth();
            var dialogTitleBarBox = domGeom.getContentBox(fullscreenDialog.titleBar);
            var h = dialogBoxParent.h - dialogTitleBarBox.h - padding * 4;

            var box = {w: w - 2, h: h};
            clone.resize(box);

            var handleDialog = fullscreenDialog.on("close", lang.hitch(this, function () {
                handleDialog.remove();
                clone.destroy();
                clone = null;
                fullscreenDialog.destroyRecursive(false);
                fullscreenDialog = null;
            }));
            this.own(handleDialog);
        }
    });
});
