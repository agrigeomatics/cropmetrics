define([
    "dojo/_base/array",
    "dojo/_base/Color",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/date/locale",
], function(array, Color, declare, lang, dateLocale) {

    return declare([], {

        /**
         * Check if the chart has series data input
         *
         * @return {boolean} return true if the chart has series data input
         */
        useSeries: function() {
            return typeof this.series !== "undefined";
        },

        useDateAxisX: function() {
            return typeof this.axisXDatePattern !== "undefined";
        },

        hasSeries: function() {
            return array.some(this.series, function(serie) {
                return typeof serie.data !== "undefined" && serie.data.length > 0;
            });
        },

        /**
         * This method is overidded in every chart plot type.
         */
        updateChart: function() {
            // store axis x labels by reading each unique axis x value
            this.labelsX = this._getLabelsX();
        },

        /**
         * Create chart series data based on series data input
         */
        createSeries: function() {
            this.minY = Infinity;
            this.minX = Infinity;
            this.maxY = -Infinity;
            this.maxX = -Infinity;

            array.forEach(this.series, lang.hitch(this, function(serieParam) {
                var serie = {
                    name: serieParam.name,
                    values: []
                };

                array.forEach(serieParam.data, lang.hitch(this, function(feature) {
                    var hasValue = typeof feature[serieParam.x] !== "undefined" &&
                        typeof feature[serieParam.y] !== "undefined";
                    if (hasValue) {
                        var x = feature[serieParam.x];
                        var y = feature[serieParam.y];

                        // fill color
                        var fill = this._getFillColor(serieParam);

                        // stroke
                        var stroke;
                        if (typeof serieParam.stroke !== "undefined") {
                            stroke = lang.clone(serieParam.stroke);
                            stroke.color = new Color(serieParam.stroke.color);
                        } else {
                            stroke = this.defaultStroke;
                        }

                        var label = x;
                        var useAxisXDate = this.useDateAxisX();
                        if (useAxisXDate) {
                            if (typeof this.labelsX !== "undefined") {
                                label = x;
                                array.some(this.labelsX, function(itemLabelX) {
                                    var found = itemLabelX.text === label;
                                    if (found) {
                                        x = itemLabelX.value;
                                    }
                                    return found;
                                });
                            }
                        }

                        if (this.labels) {
                            label = this.labels[x];
                        }

                        var tooltipText = this.formatTooltipText(y, label);

                        // Store min-max x & y
                        if (this.minY >= y) {
                            this.minY = y;
                        }
                        if (this.maxY <= y) {
                            this.maxY = y;
                        }
                        if (this.minX >= x) {
                            this.minX = x;
                        }
                        if (this.maxX <= x) {
                            this.maxX = x;
                        }

                        serie.values.push({
                            x: x,
                            y: y,
                            tooltip: tooltipText,
                            text: label,
                            fill: fill
                        });

                        serie.options = {
                            stroke: stroke
                        };
                    }
                }));
                this.chart.addSeries(serie.name, serie.values, serie.options);
            }));
        },

        _dateStep: function() {
            var dateOffset;
            if (this.axisXDatePattern === "yyyy" || this.axisXDatePattern === "yy") {
                dateOffset = 1000*60*60*24*365; // one year
            }
            else {
                dateOffset = 1000*60*60*24; // one day
            }
            return dateOffset;
        },

        _datediff: function (first, second) {
            if (this.axisXDatePattern === "yyyy" || this.axisXDatePattern === "yy") {
                return Math.floor((second-first)/(1000*60*60*24*365));
            }
            else {
                return Math.floor((second-first)/(1000*60*60*24));
            }
        },

        _getLabelsXDate: function(values) {
            var labels = [];
            // Convert each string date in Date object
            values = array.map(values, lang.hitch(this, function(value) {
                return {
                    value: value,
                    dateValue: dateLocale.parse(value, {selector: 'date', datePattern: this.axisXDatePattern})
                };
            }));

            // Sort dates using Date objects
            values.sort(function(a, b) {
                return a.dateValue - b.dateValue;
            });

            var previousDate;
            var xValue = 1;

            var minX = Infinity;
            var minDate = null;
            var maxX = -Infinity;
            var maxDate = null;

            array.forEach(values, lang.hitch(this, function(itemValue) {
                if (typeof previousDate !== "undefined") {
                    var dateValue = itemValue.dateValue;
                    var diff = this._datediff(previousDate, dateValue);
                    previousDate = dateValue;
                    xValue = xValue + diff;
                }
                else {
                    previousDate = itemValue.dateValue;
                }

                if (xValue <= minX) {
                    minX = xValue;
                    minDate = previousDate;
                }
                if (xValue >= maxX) {
                    maxX = xValue;
                    maxDate = previousDate;
                }

                labels.push({
                    text: itemValue.value + '',
                    value: xValue
                });
            }));

            // Don't ask for more tick than the number of data values as we don't
            // have date labels for decimal x values.
            var numberValues = Math.floor(maxX - minX);
            if (numberValues < this.numberTicksAxisX) {
                this.numberTicksAxisX = numberValues;
            }

            var xSteps = this.calculateSteps(minX, maxX, this.numberTicksAxisX);
            if (xSteps > 0) {
                var numberSteps = Math.floor((maxX - minX) / xSteps);
                var lastStep = numberSteps * xSteps;
                var offsetMax = lastStep + xSteps;
                var dateOffset = this._dateStep();

                // Is the last major step is smaller than the max value
                if (lastStep < maxX) {
                    numberSteps = numberSteps + 1; // will add a steps
                    var offsetMaxAdding = offsetMax - maxX;
                    // -(-) : + is string concat operator
                    var dateOffsetMax = new Date(maxDate - (-dateOffset * offsetMaxAdding));
                    var textMax = dateLocale.format(dateOffsetMax, {
                        selector: 'date',
                        datePattern: this.axisXDatePattern
                    });
                    labels.push({
                        text: textMax,
                        value: offsetMax
                    });
                }

                // Is the axis start on a step value ?
                var modulusMin = (offsetMax - minX) % xSteps;
                if (modulusMin > 0) {
                    var offsetMinSubstract = xSteps - modulusMin;
                    var offsetMin = minX - offsetMinSubstract;
                    var dateOffsetMin = new Date(minDate - (dateOffset * offsetMinSubstract));
                    var textMin = dateLocale.format(dateOffsetMin, {
                        selector: 'date',
                        datePattern: this.axisXDatePattern
                    });
                    labels.unshift({
                        text: textMin,
                        value: offsetMin
                    });
                }
            }
            return labels;
        },

        _getLabelsX: function() {
            var labels;
            var values = this._getUniqueValuesXAxis();
            var useAxisXDate = this.useDateAxisX();
            if (useAxisXDate) {
                labels = this._getLabelsXDate(values);
            }
            else {
                values.sort();

                labels = [];
                array.forEach(values, function(value, idx) {
                    labels.push({
                        text: value + '',
                        value: idx
                    });
                });
            }
            return labels;
        },

        _getFillColor: function(serieParam) {
            // fill color
            var fill;
            if (typeof serieParam.fill !== "undefined") {
                fill = new Color(serieParam.fill);
            } else {
                // default fill color
                fill = this.defaultFill;
            }
            return fill;
        },

        _getGraphicsSeries: function() {
            var graphics = [];
            array.forEach(this.series, lang.hitch(this, function(serieParam) {
                var useGeometry = typeof serieParam.geometry !== "undefined";
                if (useGeometry) {
                    array.forEach(serieParam.data, lang.hitch(this, function(feature) {
                        var geometry = feature[serieParam.geometry];
                        var color = this._getFillColor(serieParam);
                        var graphic = this._createGraphic(geometry, color);
                        if (typeof graphic !== "undefined") {
                            graphics.push(graphic);
                        }
                    }));
                }
            }));
            return graphics;
        },

        _getUniqueValuesXAxis: function() {
            var values = [];
            var useSeries = typeof this.series !== "undefined";
            if (useSeries) {
                array.forEach(this.series, lang.hitch(this, function(serieParam) {
                    var haxX = typeof serieParam.x !== "undefined";
                    if (haxX) {
                        array.forEach(serieParam.data, lang.hitch(this, function (feature) {
                            var hasValue = typeof feature[serieParam.x] !== "undefined";
                            if (hasValue) {
                                var value = feature[serieParam.x];
                                if (values.indexOf(value) === -1) {
                                    values.push(value);
                                }
                            }
                        }));
                    }
                }));
            }
            return values;
        }
    });
});
