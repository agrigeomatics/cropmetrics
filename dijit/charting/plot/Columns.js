/*
 * @external Geometry
 * @see {@link https://developers.arcgis.com/javascript/3/jsapi/geometry-amd.html Geometry}
 */

define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojox/charting/Chart2D",
    "dojox/charting/action2d/Tooltip",
    "dojox/charting/action2d/Highlight",
    "dojox/charting/themes/Shrooms",
    "../Legend",
], function(declare, lang, Chart2D, Tooltip, Highlight, Shrooms, Legend) {

    var Columns = declare([Legend], {

       /**
        * @constructor
        * @param  {Object} params
        *   mandatory: {Map} map: ESRI map
        *
        *   optional: {Array} classes : Array of objects having graphs values
        *     - mandatory: {string} class : Property name for class name
        *     - mandatory: {Array} data : Array of objects having classes
        *     - mandatory: {external:Geometry} geometry : Property name for geometry
        *     - optional: {string} class_value : Property name for class value. When using class_value, use this
        *                                        property value to calculate the y value. Without it, the y value
        *                                        is the class occurence (count or %)
        *   optional:  {object} colors : Dictionnary of class value / color. Works only with classes.
        *   optional:  {object} labels : Dictionnary of class value / label. Works only with classes.
        *
        *   optional: {Array} series : Array of objects having graphs values
        *     - mandatory: {string} name : Serie name for the legend
        *     - mandatory: {Array} data : Array of objects having serie values
        *     - mandatory: {string} x : Property name for x axis value
        *     - mandatory: {string} y : Property name for y axis value
        *     - optional: {external:Geometry} geometry : Property name for geometry
        *     - optional: {any} color : Color hex {string} or RBG {Array}
        *     - optional: {Object} stroke : stroke parameters ex: {width: 1, color: "#001DD6"}
        *
        *   optional: {Array} geometryData: Array of geometries to show on the map if no geometry specified
        *
        *   optional: {numeric} gap : Space between each series or classes
        *   optional: {string} axisXDatePattern: Specify how to format and sort axis x. (ex: M-dd)
        *
        *   optional: {Object} title : Chart title object properties
        *   optional: {Object} axisX: Parameters for x axis
        *   optional: {Object} axisY: Parameters for y axis
        *   optional: {function} tooltipFormat Function that format the value in the tooltip
        *   optional: {boolean} showControls : Show controls buttons
        *   optional: {boolean} percentages : Show y values in %
        * @param  {any} parentNode : The HTML node for the Chart
        */
        constructor: function (params, parentNode) {
            //jshint unused: false
            // Nothing to add yet
        },

        create: function () {
            var hasData = this.hasData();
            if (hasData) {
                this.inherited(arguments);
                this.createChart();
                this.createLegend();
                this.createControls();

                // Setting style after the chart have been rendered
                // because before, the chart isn't rendered correctly
                this.addBorder();
            }
        },

        removeHandles: function() {
            this.inherited(arguments);

            if (this.tooltip) {
                this.tooltip.disconnect();
            }
            if (this.highlight) {
                this.highlight.disconnect();
            }
        },

        /**
         * Create and render the chart
         *
         */
        createChart: function () {
            var titleParams = {
                title: "",
                titlePos: "top",
                titleGap: 25,
                titleFont: "normal normal bold 12pt sans-serif",
                titleFontColor: "black"
            };

            lang.mixin(titleParams, this.title);
            this.chart = new Chart2D(this.baseNode, titleParams);

            var gap = 5;
            if (typeof this.gap !== "undefined") {
                gap = this.gap;
            }

            var type = "Columns";
            if (this.series && this.series.length > 1) {
                type = "ClusteredColumns";
            }

            this.chart.addPlot("default", {
                type: type,
                tension: 3,
                gap: gap
            });

            this.updateChart();
        },

        updateChart: function () {
            this.inherited(arguments);

            this.tooltip = new Tooltip(this.chart, "default");
            this.highlight = new Highlight(this.chart, "default");

            var useSeries = this.useSeries();
            var useClasses = this.useClasses();

            if (useClasses) {
                this.createClasses();
            }
            else if (useSeries) {
                this.createSeries();
            }

            this.createXAxis();
            this.createYAxis(this.minY, this.maxY);

            // Set the default chart theme
            this.chart.setTheme(Shrooms);

            this.chart.render();
        },

        getType: function() {
            return this.i18n.type.column;
        }
    });

    Columns.prototype.clone = function(params, parentNode) {
        var newParams = {};
        lang.mixin(newParams, this.params);
        if (typeof params !== "undefined") {
            lang.mixin(newParams, params);
        }
        if (typeof parentNode === "undefined") {
            parentNode = this.parentNode;
        }
        return new Columns(newParams, parentNode);
    };


    return Columns;
});
