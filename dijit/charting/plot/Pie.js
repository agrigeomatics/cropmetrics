define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/_base/Color",
    "dojo/_base/lang",
    "dojo/number",
    "dojox/charting/Chart",
    "dojox/charting/themes/Shrooms",
    "dojox/charting/action2d/MoveSlice",
    "dojox/charting/action2d/Tooltip",
    "../Legend",
], function(array, declare, Color, lang, numberUtils, Chart, Shrooms, MoveSlice, Tooltip, Legend) {
    var Pie = declare([Legend], {
        /**
         * @constructor
         * @param  {Object} params
         *   mandatory: {Map} map: ESRI map
         *
         *   optional: {Array} classes : Array of objects having graphs values
         *     - mandatory: {string} class : Property name for class name
         *     - mandatory: {Array} data : Array of objects having classes
         *     - mandatory: {external:Geometry} geometry : Property name for geometry
         *     - optional: {string} class_value : Property name for class value. When using class_value, use this
         *                                        property value to calculate the y value. Without it, the y value
         *                                        is the class occurence (count or %)
         *   optional:  {object} colors : Dictionnary of class value / color. Works only with classes.
         *   optional:  {object} labels : Dictionnary of class value / label. Works only with classes.
         *
         *   optional: {Array} geometryData: Array of geometries to show on the map if no geometry specified
         *
         *   optional: {Object} title : Chart title object properties
         *   optional: {function} tooltipFormat Function that format the value in the tooltip
         *   optional: {boolean} showControls : Show controls buttons
         *   optional: {boolean} percentages : Show y values in %
         * @param  {any} parentNode : The HTML node for the Chart
         */
        constructor: function (params, parentNode) {
            // jshint unused: false

            // Always use percentages
            this.percentages = true;

            // Use one serie for all classes
            this.useOneSerie = true;
        },

        create: function () {
            this.inherited(arguments);

            var hasData = this.hasData();
            if (hasData) {
                this.createChart();
                this.createLegend();
                this.createControls();

                // Setting style after the chart have been rendered
                // because before, the chart isn't rendered correctly
                this.addBorder();
            }
        },

        removeHandles: function() {
            this.inherited(arguments);

            if (this.tooltip) {
                this.tooltip.disconnect();
            }
            if (this.moveSlice) {
                this.moveSlice.disconnect();
            }
        },

        createChart: function () {
            var titleParams = {
                title: "",
                titlePos: "top",
                titleGap: 25,
                titleFont: "normal normal bold 12pt sans-serif",
                titleFontColor: "black"
            };

            lang.mixin(titleParams, this.title);

            this.chart = new Chart(this.baseNode, titleParams);

            this.chart.addPlot("default", {
                type: "Pie",
                labelOffset: -30,
                radius: 90,
                labels: false
            });

            this.updateChart();
        },

        updateChart: function () {
            this.inherited(arguments);

            this.moveSlice = new MoveSlice(this.chart, "default");
            this.tooltip = new Tooltip(this.chart, "default");

            var useClasses = this.useClasses();

            if (useClasses) {
                this.createClasses();
            }

            // Set the default chart theme
            this.chart.setTheme(Shrooms);

            this.chart.render();
        },

        getType: function() {
            return this.i18n.type.pie;
        }
    });

    Pie.prototype.clone = function(params, parentNode) {
        var newParams = {};
        lang.mixin(newParams, this.params);
        if (typeof params !== "undefined") {
            lang.mixin(newParams, params);
        }
        if (typeof parentNode === "undefined") {
            parentNode = this.parentNode;
        }
        return new Pie(newParams, parentNode);
    };

    return Pie;
});
