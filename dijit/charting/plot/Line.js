define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojox/charting/Chart2D",
    "dojox/charting/action2d/Tooltip",
    "dojox/charting/action2d/Highlight",
    "dojox/charting/plot2d/Indicator",
    "dojox/charting/themes/Shrooms",
    "../Legend",
], function(array, declare, lang, Chart2D, Tooltip, Highlight, Indicator, Shrooms, Legend) {

    var Line = declare([Legend], {
        /**
         * @constructor
         * @param  {Object} params
         *   mandatory: {Map} map: ESRI map
         *
         *   optional: {Array} series : Array of objects having graphs values
         *     - mandatory: {string} name : Serie name for the legend
         *     - mandatory: {Array} data : Array of objects having serie values
         *     - mandatory: {string} x : Property name for x axis value
         *     - mandatory: {string} y : Property name for y axis value
         *     - optional: {external:Geometry} geometry : Property name for geometry
         *     - optional: {any} color : Color hex {string} or RBG {Array}
         *     - optional: {Object} stroke : stroke parameters ex: {width: 1, color: "#001DD6"}
         *
         *   optional: {Array} geometryData: Array of geometries to show on the map if no geometry specified
         *
         *   optional:  {Object} horizontalLines: horizontal lines param (value, color, width))
         *
         *   optional: {Object} title : Chart title object properties
         *   optional: {Object} axisX: Parameters for x axis
         *   optional: {Object} axisY: Parameters for y axis
         *   optional: {function} tooltipFormat Function that format the value in the tooltip
         *   optional: {boolean} showControls : Show controls buttons
         * @param  {any} parentNode : The HTML node for the Chart
         */
        constructor: function (params, parentNode) {
            //jshint unused: false
            // Nothing to add yet
        },

        /**
         * Create the chart and add a border
         *
         */
        create: function () {
            this.inherited(arguments);

            var hasData = this.hasData();
            if (hasData) {
                this.createChart();
                this.createLegend();
                this.createControls();
                // Setting style after the chart have been rendered
                // because before, the chart isn't rendered correctly
                this.addBorder();                
            }
        },

        removeHandles: function() {
            this.inherited(arguments);

            if (this.tooltip) {
                this.tooltip.disconnect();
            }
            if (this.highlight) {
                this.highlight.disconnect();
            }
        },

        createChart: function () {
            var titleParams = {
                title: "",
                titlePos: "top",
                titleGap: 25,
                titleFont: "normal normal bold 12pt sans-serif",
                titleFontColor: "black"
            };

            lang.mixin(titleParams, this.title);
            this.chart = new Chart2D(this.baseNode, titleParams);

            this.createHorizontalLines();

            this.chart.addPlot("default", {
                type: "Lines",
                markers: true
            });

            this.updateChart();
        },

        /**
         * Create the data series, x and y axis, set the theme and render the chart
         */
        updateChart: function () {
            this.inherited(arguments);

            // Chart UI features
            this.tooltip = new Tooltip(this.chart, "default");
            this.highlight = new Highlight(this.chart, "default");

            // Create the data serie
            this.createSeries();
            // Create the x and y axis
            this.createXAxis();
            this.createYAxis();

            // Set the default chart theme
            this.chart.setTheme(Shrooms);

            // Render the chart
            this.chart.render();
        },

        horizontalLabelFunc: function (horizontalLine, index, values, data, fixed, precision, labels) {
            //jshint unused: false
            return horizontalLine.label || values[0];
        },

        /**
         * Create all horizontal lines
         * @param  {string} Serie name
         */
        createHorizontalLines: function () {
            if (this.horizontalLines !== "undefined") {
                array.forEach(this.horizontalLines, lang.hitch(this, function (horizontalLine, idx) {
                    var hasValue = typeof horizontalLine.value !== "undefined";
                    if (hasValue) {
                        var name = "horizontal_" + idx;
                        var color = horizontalLine.color || "black";
                        var fontColor = horizontalLine.fontColor || "black";
                        var precision = horizontalLine.precision || 0;
                        var font = horizontalLine.font || "normal normal bold 10pt sans-serif";
                        var options = {
                            type: Indicator,
                            vertical: false,
                            font: font,
                            fontColor: fontColor,
                            lineStroke: { color: color, style: "ShortDash" },
                            stroke: { color: "transparent" },
                            outline: { color: "transparent" },
                            fill: "transparent",
                            offset: horizontalLine.offset || { y: -2, x: -10 },
                            labels: "line",
                            precision: precision,
                            values: horizontalLine.value,
                            labelFunc: lang.hitch(this, this.horizontalLabelFunc, horizontalLine)
                        };

                        this.chart.addPlot(name, options);
                    }
                }));
            }
        },

        getType: function() {
            return this.i18n.type.line;
        }

    });

    Line.prototype.clone = function(params, parentNode) {
        var newParams = {};
        lang.mixin(newParams, this.params);
        if (typeof params !== "undefined") {
            lang.mixin(newParams, params);
        }
        if (typeof parentNode === "undefined") {
            parentNode = this.parentNode;
        }
        return new Line(newParams, parentNode);
    };

    return Line;
});
