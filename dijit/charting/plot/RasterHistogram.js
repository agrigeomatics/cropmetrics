define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/number",
    "dojox/charting/Chart2D",
    "dojox/charting/action2d/Tooltip",
    "dojox/charting/action2d/Highlight",
    "dojox/charting/themes/Shrooms",
    "../Chart"
], function(array, declare, lang, numberUtils,
            Chart2D, Tooltip, Highlight, Shrooms, Chart) {

    var RasterHistogram = declare([Chart], {
        /**
        * Read a Image Server Raster Histogram query data and create a
        * histogram with it.
        *
        * @param  {any} params :
        *   mandatory: {object} histogram : histogram object returned by ArcGIS Image Server
        *   mandatory:  {Map} map: ESRI map
        *   optional: {string} geometry: Property name for geometry
        *   optional:  {Array} geometryData: Array of geometries to show in the map
        *   optional:  {function(string)} : replaceValue a function that replace value by an another
        *   optional:  {Object} title : Chart title object properties
        *   optional:  {Object} axisX: Parameters for x axis
        *   optional:  {Object} axisY: Parameters for y axis
        *   optional:  {boolean} showControls : Show controls buttons
        *   optional:  {number}: places: the x axis precision
        * @param  {any} parentNode : The HTML node for the Chart
        */
        constructor: function (params, parentNode) {
            // jshint unused: false
            // Nothing to add yet
            var defaultValues = { places: 1 };
            lang.mixin(defaultValues, this.params);
            this.params = defaultValues;
        },

        hasData: function() {
            if (this.histogram) {
                return this.histogram.size > 0;
            } else {
                return false;
            }
        },

        create: function () {
            this.inherited(arguments);
            if (this.histogram) {
                this.createChart();
                this.createControls();
                this.addBorder();
            }
        },

        removeHandles: function() {
            this.inherited(arguments);

            if (this.tooltip) {
                this.tooltip.disconnect();
            }
            if (this.highlight) {
                this.highlight.disconnect();
            }
        },

        createChart: function () {
            var titleParams = {
                title: "",
                titlePos: "top",
                titleGap: 25,
                titleFont: "normal normal bold 12pt sans-serif",
                titleFontColor: "black"
            };
            lang.mixin(titleParams, this.title);

            this.chart = new Chart2D(this.baseNode, titleParams);

            this.chart.addPlot("default", {
                type: "Columns"
            });
            this.updateChart();
        },

        linearScaleLabel: function (minCurrent, maxCurrent, minExpected, maxExpected, precision, text, currentValue) {
            var computed;

            // Translate by + 0.5
            if (minExpected === (minCurrent - 0.5) && maxExpected === (maxCurrent - 0.5)) {
                minExpected = minExpected + 0.5;
                maxExpected = maxExpected + 0.5;
            }

            // http://stackoverflow.com/questions/5294955/how-to-scale-down-a-range-of-numbers-with-a-known-min-and-max-value

            //       (b-a)(x - min)
            //f(x) = --------------  + a
            //          max - min

            var numerator = (maxExpected - minExpected) * (currentValue - minCurrent);
            var denominator = (maxCurrent - minCurrent);

            computed = (numerator / denominator) + minExpected;
            var format = numberUtils.round(computed, precision);

            if (typeof this.replaceValue !== "undefined") {
                format = this.replaceValue(format);
            }
            return format;
        },

        updateChart: function () {
            this.inherited(arguments);

            this.tooltip = new Tooltip(this.chart, "default");
            this.highlight = new Highlight(this.chart, "default");

            this.stats = this.histogram.counts;
            var series = [];
            var serie = { name: "Serie A", values: [] };

            var counterX = 0;
            var min = this.histogram.min;
            var max = this.histogram.max;
            this.minX = counterX;
            this.maxX = this.histogram.size;

            this.minY = Infinity;
            this.maxY = -Infinity;

            for (var stat in this.stats) {
                var labelX = this.linearScaleLabel(this.minX, this.maxX, min, max, this.places, " ", counterX);
                var labelY = numberUtils.format(this.stats[stat], { places: 0 });

                var y = parseFloat(this.stats[stat]);

                // Store min-max x & y
                if (this.minY >= y) {
                    this.minY = y;
                }
                if (this.maxY <= y) {
                    this.maxY = y;
                }

                serie.values.push({
                    x: counterX,
                    y: y,
                    tooltip: labelX + " : " + labelY
                });
                counterX += 1;
            }

            series.push(serie);

            var labelFunc = lang.hitch(this, this.linearScaleLabel, this.minX, this.maxX, min, max, this.places);

            this.createXAxis({labelFunc: labelFunc});

            this.createYAxis({title: this.titleY});

            array.forEach(series, lang.hitch(this, function (item) {
                this.chart.addSeries(item.name, item.values);
            }));

            // Set the default chart theme
            this.chart.setTheme(Shrooms);

            this.chart.render();
        },

        getType: function() {
            return this.i18n.type.histogram;
        }
    });

    RasterHistogram.prototype.clone = function(params, parentNode) {
        var newParams = {};
        lang.mixin(newParams, this.params);
        if (typeof params !== "undefined") {
            lang.mixin(newParams, params);
        }
        if (typeof parentNode === "undefined") {
            parentNode = this.parentNode;
        }
        return new RasterHistogram(newParams, parentNode);
    };
    return RasterHistogram;
});
