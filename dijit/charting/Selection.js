define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/aspect",
    "dojo/dom-construct",
    "dojo/on",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetBase",
    "dojo/text!./templates/selection.html",
    "dojo/i18n!./nls/selection"
], function(array, declare, lang, aspect, domConstruct, on, _TemplatedMixin, _WidgetBase, template, i18n) {

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        i18n: i18n,

        _charts: null,
        _displayedCharts: null,
        handlesDelete: null,
        count: 0,
        _displayedSequence: 0,

        constructor: function (params, srcNodeRef) {
            //jshint unused:false

            this._charts = [];
            this._displayedCharts = [];
            this.handlesDelete = [];
            this.count = 0;
            this._displayedSequence = 0;
        },

        destroy: function(preserveDom) {
            //jshint unused:false
            this.inherited(arguments);
            this.clear();
            this.domNode = null;
        },

        postCreate: function() {
            this.inherited(arguments);
            this.own(on(this.buttonShow, "click", lang.hitch(this, this.showChart)));
            this.own(on(this.buttonShowAll, "click", lang.hitch(this, this.showAll)));
            this.own(on(this.buttonClear, "click", lang.hitch(this, this.hideAll)));
            this.own(on(window, 'resize', lang.hitch(this, this.uniformizeCharts)));
        },

        _removeHandlesDelete: function() {
            if (this.handlesDelete) {
                array.forEach(this.handlesDelete, function (handle) {
                    handle.remove();
                });
                this.handlesDelete = [];
            }
        },

        sortSelect: function (select) {
            var temporary = [];
            for (var i = 0; i < select.options.length; i++) {
                temporary[i] = [];
                temporary[i][0] = select.options[i].text;
                temporary[i][1] = select.options[i].value;
            }
            temporary.sort();
            while (select.options.length > 0) {
                select.options[0] = null;
            }
            for (var j = 0; j < temporary.length; j++) {
                var op = new Option(temporary[j][0], temporary[j][1]);
                select.options[j] = op;
            }
        },

        addChart: function(chart) {
            var hasData = chart.hasData();
            if (hasData) {
                var id = this.count;
                this._charts.push({id: id, chart: chart});

                var label = chart.getTitle();
                var type = chart.getType();
                label = label + " - " + type;
                var attributesOption = { innerHTML: label, value: id };
                domConstruct.create("option", attributesOption, this.chartSelect, "last");

                this.count += 1;
                this.sortSelect(this.chartSelect);
            }
        },

        getSelectedChart: function() {
            var selected;
            if (this.chartSelect && this.chartSelect.options) {
                var selectedId = parseInt(this.chartSelect.options[this.chartSelect.selectedIndex].value);
                array.some(this._charts, function (item) {
                    var isFound = item.id === selectedId;
                    if (isFound) {
                        selected = item.chart;
                    }
                    return isFound;
                });
            }
            return selected;
        },

        clear: function() {
            this.hideAll();
            this.removeAll();
            if (this.chartSelect && this.chartSelect.options) {
                this.chartSelect.options.length = 0;
                this.count = 0;
            }
        },

        removeAll: function() {
            array.forEach(this._charts, function (item) {
                item.chart.destroy();
            });
            this._charts = [];
        },

        hideAll: function () {
            this._removeHandlesDelete();
            array.forEach(this._displayedCharts, function (item) {
                var chart = item.chart;
                if (chart) {
                    chart.destroy();
                }
            });
            this._displayedCharts = [];
            // Reset displayed chart sequence id
            this._displayedSequence = 0;
        },

        _removeDisplayed: function (id) {
            var index;
            array.some(this._displayedCharts, function (item, idx) {
                var isFound = item.id === id;
                if (isFound) {
                    index = idx;
                }
                return isFound;
            });
            if (typeof index !== "undefined") {
                this._displayedCharts[index] = null;
                this._displayedCharts.splice(index, 1);

                // Reset displayed chart sequence id if no more displayed charts
                if (this._displayedCharts.length === 0) {
                    this._displayedSequence = 0;
                }
            }
        },

        _cloneDisplayChart: function(chart, params) {
            // Change existing charts size depending on number of charts
            var nextLength = this._displayedCharts.length + 1;
            this.setColumns(nextLength);

            // Create a new chart
            if (typeof params === "undefined") {
                params = {};
            }
            if (nextLength > 0 && nextLength <= 3) {
                params.columns = nextLength;
            }
            var clone = chart.clone(params);
            clone.create();

            var id = this._displayedSequence;

            this._displayedCharts.push({id: id, chart: clone});

            // Listen for chart destroy
            var handle = aspect.after(clone, "destroy", lang.hitch(this, function(){
                this._removeDisplayed(id);
                handle.remove();

                var length = this._displayedCharts.length;
                this.setColumns(length);

                this.uniformizeCharts();
            }), false);

            this.own(handle);
            this.handlesDelete.push(handle);

            // Next displayed chart id
            this._displayedSequence += 1;
        },

        showAll: function() {
            this.hideAll();
            array.forEach(this._charts, lang.hitch(this, function(item) {
                this._cloneDisplayChart(item.chart);
            }));
            this.uniformizeCharts();
        },

        showChart: function() {
            var selectedChart = this.getSelectedChart();
            if (selectedChart) {
                var params = {position: "first"};
                this._cloneDisplayChart(selectedChart, params);
                this.uniformizeCharts();
            }
        },

        _getChartMaxBox: function() {
            var maxHeight = 0.0;
            var minWidth = 0.0;
            array.forEach(this._displayedCharts, function(item) {
                var chart = item.chart;

                var height = chart.getHeight();
                if (height > maxHeight) {
                    maxHeight = height;
                }

                var width = chart.getWidth();
                if (width > 0 && minWidth === 0) {
                    minWidth = width;
                }
                if (width > minWidth) {
                    minWidth = width;
                }
            });
            return {w: minWidth, h: maxHeight};
        },

        setColumns: function(length) {
            array.forEach(this._displayedCharts, function (item) {
                var chart = item.chart;
                chart.setColumns(length);
            });
        },

        uniformizeCharts: function() {
            var maxBox = this._getChartMaxBox();
            if (maxBox.w > 0 && maxBox.h > 0) {
                array.forEach(this._displayedCharts, function(item) {
                    item.chart.resize(maxBox);
                });
            }

            // uniformize charts twice, because the legend can change in size
            maxBox = this._getChartMaxBox();
            if (maxBox.w > 0 && maxBox.h > 0) {
                array.forEach(this._displayedCharts, function(item) {
                    item.chart.resize(maxBox);
                });
            }
        }
    });

});
