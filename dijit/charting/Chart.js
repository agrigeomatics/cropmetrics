define([
    "dojo/_base/array",
    "dojo/_base/Color",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/date/locale",
    "dojo/number",
    "esri/geometry/jsonUtils",
    "esri/graphic",
    "esri/layers/GraphicsLayer",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "./ClassesMixin",
    "./SeriesMixin",
    "./Toolbar",
    "dojo/i18n!./nls/chart",
],
function(array, Color, declare, lang, domClass, domConstruct, domStyle, dateLocale, numberUtils,
         jsonUtils, Graphic, GraphicsLayer, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol,
         ClassesMixin, SeriesMixin, Toolbar, i18n) {
    return declare([ClassesMixin, SeriesMixin], {

        constructor: function(params, parentNode) {
            var defaultValues = {
                title: "",
                showControls: true,
                fullscreen: false,
                position: "last",
                defaultFill: new Color("#FF0000"),
                defaultStroke: { width: 1, color: new Color("#000000"),
                defaultLineStroke: { width: 1, color: new Color("#000000") }}
            };
            this.columns = null;
            this.params = params;
            lang.mixin(defaultValues, params);
            lang.mixin(this, defaultValues);

            this.numberTicksAxisX = 12;
            this.numberTicksAxisY = 10;

            this.parentNode = parentNode;
            this.i18n = i18n;
        },

        hasData: function() {
            var useClasses = this.useClasses();
            var useSeries = this.useSeries();
            if (useClasses) {
                return this.hasClasses();
            }
            else if (useSeries) {
                return this.hasSeries();
            }
            else {
                return false;
            }
        },

        /**
         * Destroy all references
         *
         */
        destroy: function () {
            this.inherited(arguments);

            if (this.chart) {
                this.chart.destroy();
                this.chart = null;
            }

            if (this.toolbar) {
                this.toolbar.destroy();
                this.toolbar = null;
            }

            if (this.graphicLayer) {
                this.graphicLayer.clear();
                if (this.map) {
                    this.map.removeLayer(this.graphicLayer);
                }
                this.graphicLayer = null;
            }
            this.destroyNodes();
        },

        create: function () {
            if (this.parentNode) {
                var attr = { "class": "metric-chart" };
                if (this.fullscreen) {
                    attr.class = attr.class + " metric-chart-fullscreen";
                }

                this.chartAndLegendNode = domConstruct.create("div", null, this.parentNode, this.position);
                this.baseNode = domConstruct.create("div", attr, this.chartAndLegendNode, "last");

                if (this.showControls) {
                    this.controlsDiv = domConstruct.create("div", null, this.chartAndLegendNode, "first");
                }

                this.displayColumns();
            }
        },

        destroyNodes: function() {
            domConstruct.destroy(this.controlsDiv);
            domConstruct.destroy(this.baseNode);
            domConstruct.destroy(this.chartAndLegendNode);
            this.controlsDiv = null;
            this.baseNode = null;
            this.chartAndLegendNode = null;
        },

        /**
         * Create the chart axis x. Calculates an optimal ticks step value.
         * Used for charts having y axis.
         *
         * @param  {Object=} params Added axis x parameters
         */
        createYAxis: function (params) {
            var defaultY = {
                vertical: true,
                majorLabels: true,
                majorTicks: true,
                minorLabels: false,
                minorTicks: false,
                titleOrientation: "axis",
                fixLower: "major",
                fixUpper: "major"
            };

            if (typeof params !== "undefined") {
                lang.mixin(defaultY, params);
            }

            if (typeof this.axisY !== "undefined" &&
                typeof this.axisY.min !== "undefined") {
                this.minY = this.axisY.min;
            }

            if (typeof this.axisY !== "undefined" &&
                typeof this.axisY.max !== "undefined") {
                this.maxY = this.axisY.max;
            }

            var ySteps = this.calculateSteps(this.minY, this.maxY, this.numberTicksAxisY);
            if (typeof ySteps !== "undefined" && ySteps !== 0) {
                defaultY.majorTickStep = ySteps;
            }

            if (typeof this.axisY !== "undefined") {
                lang.mixin(defaultY, this.axisY);
            }
            this.chart.addAxis("y", defaultY);
        },

        createMinMaxFromLabel: function() {
            var hasAxisXConfig = typeof this.axisX !== "undefined";
            if (hasAxisXConfig) {
                var getValueFromText = lang.hitch(this, function(text) {
                    var found;
                    if (typeof this.labelsX !== "undefined") {
                        array.some(this.labelsX, function(item) {
                            var hasText = item.text === text;
                            if (hasText) {
                                found = item.value;
                            }
                            return hasText;
                        });
                    }
                    return found;
                });

                var hasMinText = typeof this.axisX.minText !== "undefined";
                if (hasMinText) {
                    var min = getValueFromText(this.axisX.minText);
                    if (typeof min !== "undefined") {
                        this.axisX.min = min;
                    }
                }
                var hasMaxText = typeof this.axisX.maxText !== "undefined";
                if (hasMaxText) {
                    var max = getValueFromText(this.axisX.maxText);
                    if (typeof max !== "undefined") {
                        this.axisX.max = max;
                    }
                }
            }
        },

        /**
         * Creates the chart axis x. Calculates an optimal ticks step value.
         * Used for charts having x axis.
         *
         * @param  {Object=} params Added axis x parameters
         */
        createXAxis: function (params) {
            var defaultX = {
                majorTicks: true,
                majorLabels: true,
                minorTicks: false,
                minorLabels: false,
                microTicks: false,
                titleOrientation: "away",
                fixLower: "major",
                fixUpper: "major"
            };

            if (typeof params !== "undefined") {
                lang.mixin(defaultX, params);
            }

            if (typeof this.axisX !== "undefined") {
                this.createMinMaxFromLabel();
                lang.mixin(defaultX, this.axisX);
            }

            if (typeof this.labelsX !== "undefined" && this.labelsX.length > 0) {
                defaultX.labels = this.labelsX;
            }

            if (typeof this.axisX !== "undefined" &&
                typeof this.axisX.min !== "undefined") {
                this.minX = this.axisX.min;
            }

            if (typeof this.axisX !== "undefined" &&
                typeof this.axisX.max !== "undefined") {
                this.maxX = this.axisX.max;
            }

            // Calculate steps only if using series
            var useClasses = this.useClasses();
            var useDateAxisX = this.useDateAxisX();
            if (useClasses || useDateAxisX) {
                // Don't ask for more tick than the number of data values as we don't
                // have labels for decimal x values.
                var numberValues = Math.floor(this.maxX - this.minX);
                if (numberValues < this.numberTicksAxisX) {
                    this.numberTicksAxisX = numberValues;
                }
            }

            var xSteps = this.calculateSteps(this.minX, this.maxX, this.numberTicksAxisX);
            if (typeof xSteps !== "undefined" && xSteps !== 0) {
                defaultX.majorTickStep = xSteps;
            }

            this.chart.addAxis("x", defaultX);
        },

        /**
         * Calculates the step value for a axis given min, max and tick count values.
         * The step value is calculated to produce rounded ticks values.
         * @param  {number} min The minimum value
         * @param  {number} max The maximum value
         * @param  {number} tickCount The desired number of ticks on the axis
         * @return {number} The step value between ticks axis
         */
        calculateSteps: function (min, max, tickCount) {
            if (min > max) {
                return undefined;
            }
            var span = max - min,
                step = Math.pow(10, Math.floor(Math.log(span / tickCount) / Math.LN10)),
                err = tickCount / span * step;

            // Filter ticks to get closer to the desired count.
            if (err <= 0.15) { step *= 10; }
            else if (err <= 0.35) { step *= 5; }
            else if (err <= 0.75) { step *= 2; }

            return step;
        },

        displayColumns: function () {
            if (this.baseNode && this.chartAndLegendNode) {
                domClass.remove(this.baseNode, ["metric-chart-one", "metric-chart-two", "metric-chart-three"]);
                domClass.remove(this.chartAndLegendNode, ["metric-chart-one", "metric-chart-two", "metric-chart-three"]);

                if (!this.fullscreen && this.columns) {
                    if (this.columns === 1) {
                        domClass.add(this.baseNode, "metric-chart-one");
                        domClass.add(this.chartAndLegendNode, "metric-chart-one");
                    }
                    else if (this.columns === 2) {
                        domClass.add(this.baseNode, "metric-chart-two");
                        domClass.add(this.chartAndLegendNode, "metric-chart-two");
                    }
                    else if (this.columns === 3) {
                        domClass.add(this.baseNode, "metric-chart-three");
                        domClass.add(this.chartAndLegendNode, "metric-chart-three");
                    }
                }
            }
        },

        setColumns: function(columns) {
            this.columns = null;
            if (typeof columns !== "undefined") {
                if (columns >= 1 && columns <= 3) {
                    this.columns = columns;
                }
            }
            this.displayColumns();
        },

        /**
         * Remove border class
         *
         */
        removeBorder: function () {
            domClass.remove(this.chartAndLegendNode, "metric-chart-border");
            if (this.fullscreen) {
                domClass.remove(this.chartAndLegendNode, "metric-chart-border-fullscreen");
            }
        },

        /**
         * Add border class
         *
         */
        addBorder: function () {
            domClass.add(this.chartAndLegendNode, "metric-chart-border");
            if (this.fullscreen) {
                domClass.add(this.chartAndLegendNode, "metric-chart-border-fullscreen");
            }
        },

        resize: function (box) {
            if (box && box.w && box.h) {
                box.w = Math.floor(box.w);

                var heightChartAndLegend = this.getHeight();
                var heightChart = this.getHeightChart();
                var legendHeight = heightChartAndLegend - heightChart;
                var newHeight = box.h - legendHeight;

                var width = box.w;
                if (this.chart) {
                    this.removeBorder();
                    this.chart.resize({ w: width, h: newHeight });
                    this.addBorder();

                    if (this.toolbar) {
                        this.toolbar.resize({ w: width });
                    }
                }
            }
        },

        _getGraphics: function () {
            var useSeries = this.useSeries();
            var useClasses = this.useClasses();

            if (useSeries) {
                return this._getGraphicsSeries();
            }
            else if (useClasses) {
                return this.getGraphicsClasses();
            }
            else {
                return [];
            }
        },

        createGraphicLayer: function () {
            if (this.map) {
                this.graphicLayer = new GraphicsLayer({
                    opacity: 0.5,
                    visible: false
                });
                this.map.addLayer(this.graphicLayer);

                var isArray = typeof this.geometryData !== "undefined" &&
                              this.geometryData instanceof Array;

                var graphics = this._getGraphics();
                if (graphics.length > 0) {
                    array.forEach(graphics, lang.hitch(this, function(graphic) {
                        this.graphicLayer.add(graphic);
                    }));
                }
                else if (isArray) {
                    array.forEach(this.geometryData, lang.hitch(this, function(geometry) {
                        var color = this.defaultFill;
                        var graphic = this._createGraphic(geometry, color);
                        if (typeof graphic !== "undefined") {
                            this.graphicLayer.add(graphic);
                        }
                    }));
                }
                else if (typeof this.geometryData !== "undefined") {
                    var color = this.defaultFill;
                    var graphic = this._createGraphic(this.geometryData, color);
                    if (typeof graphic !== "undefined") {
                        this.graphicLayer.add(graphic);
                    }
                }
            }
        },

        _createGraphic: function (geometry, color) {
            var graphic;
            if (typeof geometry !== "undefined") {
                var symbol;

                if (typeof geometry.type === "undefined") {
                    geometry = jsonUtils.fromJson(geometry);
                }

                var style;
                var style_outline = SimpleLineSymbol.STYLE_SOLID;
                var width_ouline = 1;
                var outline = new SimpleLineSymbol(style_outline, color, width_ouline);

                switch (geometry.type) {
                    case "point":
                    case "multipoint":
                        style = SimpleMarkerSymbol.STYLE_CIRCLE;
                        var size = 4;
                        symbol = new SimpleMarkerSymbol(style, size, outline, color);
                        break;
                    case "polyline":
                        style = SimpleLineSymbol.STYLE_SOLID;
                        var width = 1;
                        symbol = new SimpleLineSymbol(style, color, width);
                        break;
                    default:
                        style = SimpleFillSymbol.STYLE_SOLID;
                        symbol = new SimpleFillSymbol(style, outline, color);
                        break;
                }
                graphic = new Graphic(geometry, symbol);
            }
            return graphic;
        },

        toggleGraphics: function () {
            if (this.graphicLayer) {
                if (this.graphicLayer.visible) {
                    this.graphicLayer.hide();
                }
                else {
                    this.graphicLayer.show();
                }
            }
        },

        formatTooltipText: function(value, label) {
            var tooltipText;
            if (this.percentages) {
                tooltipText = numberUtils.format(value, {
                    places: 0
                });
                tooltipText = label + ": " + tooltipText + " %";
            }
            else {
                tooltipText = label + ": " + value;
                if (typeof this.tooltipFormat !== "undefined") {
                    tooltipText = label + ": " + this.tooltipFormat(value);
                }
            }
            return tooltipText;
        },

        createControls: function () {
            // Create the graphic layer
            this.createGraphicLayer();
            // Create the toolbar
            if (this.controlsDiv) {
                this.toolbar = new Toolbar({
                    map: this.map,
                    chart: this
                }, this.controlsDiv);
                this.toolbar.startup();
            }
        },

        /**
         * The chart title text
         *
         */
        getTitle: function() {
            var title = "";
            if (typeof this.title !== "undefined" &&
                typeof this.title.title !== "undefined") {
                    title = this.title.title;
            }
            return title;
        },

        getHeight: function() {
            var height = 0;
            if (this.chartAndLegendNode) {
                height = domStyle.get(this.chartAndLegendNode, "height");
            }
            return height;
        },

        getWidth: function() {
            var width = 0;
            if (this.chartAndLegendNode) {
                width = domStyle.get(this.chartAndLegendNode, "width");
            }
            return width;
        },

        getHeightChart: function() {
            var height = 0;
            if (this.baseNode) {
                height = domStyle.get(this.baseNode, "height");
            }
            return height;
        },

        getWidthChart: function() {
            var width = 0;
            if (this.baseNode) {
                width = domStyle.get(this.baseNode, "width");
            }
            return width;
        },

        getType: function() {
            return "";
        }
    });
});
