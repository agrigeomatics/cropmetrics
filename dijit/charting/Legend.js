define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/dom-geometry",
    "dojo/on",
    "dojo/query",
    "dojox/charting/widget/Legend",
    "./Chart",
],
function(array, declare, lang, domClass, domConstruct, domGeom, on, query, Legend, Chart) {
    return declare([Chart], {

        handlesLegend: null,

        constructor: function(params, parentNode) {
            //jshint unused:false
            this.handlesLegend = [];
        },

        create: function () {
            this.inherited(arguments);
        },

        destroy: function () {
            this.inherited(arguments);
            this.destroyLegend();
        },

        resize: function (box) {
            this.inherited(arguments);

            if (box && box.w && box.h) {
                box.w = Math.floor(box.w);

                domGeom.setContentSize(this.legendContainer, {w: box.w });
            }
        },

        removeHandles: function () {
            this.inherited(arguments);

            this.removeHandlesLegend();
        },

        removeHandlesLegend: function() {
            array.forEach(this.handlesLegend, function (handle) { handle.remove(); });
            this.handlesLegend = [];
        },

        destroyNodes: function() {
            this.destroyNodesLegend();

            this.inherited(arguments);
        },

        destroyNodesLegend: function() {
            if (this.legendTable) {
                domConstruct.destroy(this.legendTable);
            }
            if (this.legendContainer) {
                domConstruct.destroy(this.legendContainer);
            }
            this.legendTable = null;
        },

        destroyLegend: function () {
            if (this.legend) {
                this.removeHandlesLegend();
                this.legend.destroy();
                this.legend = null;
                this.destroyNodesLegend();
            }
        },

        createLegend: function () {
            if (this.chartAndLegendNode) {
                if (!this.legendContainer) {
                    var attrs = {class: "metric-chart-legend"};
                    this.legendContainer = domConstruct.create("div", attrs, this.chartAndLegendNode, "last");
                }
                if (!this.legendTable) {
                    this.legendTable = domConstruct.create("table", null, this.legendContainer, "last");
                }
                this.legend = new Legend({ chart: this.chart, horizontal: false }, this.legendTable);
                this.updateLegend();
            }
        },

        fireEvent: function (text, type) {
            var foundIndex = -1;
            var serieName;
            var series = this.chart.series;

            var useClasses = this.useClasses();
            if (useClasses) {
                array.some(series, function (serie) {
                    var found = false;
                    array.some(serie.data, function(item, idx) {
                        found = item.text === text;
                        if (found) {
                            foundIndex = idx;
                            serieName = serie.name;
                        }
                    });
                    return found;
                });
            }

            this.chart.fireEvent(
                serieName,
                type,
                foundIndex
            );
        },

        updateLegend: function () {
            this.legend.refresh();

            // add tabindex on tr
            var legendBodyNode = this.legend.legendBody;

            var tds = query("td", legendBodyNode);

            var getValueFromLabel = lang.hitch(this, function (evt) {
                var value;
                var target = evt.target;
                var label = query("label.dojoxLegendText", target)[0];
                var textContent = label.textContent;
                if (textContent) {
                    value = textContent;
                }
                return value;
            });

            array.forEach(tds, lang.hitch(this, function (td) {
                td.setAttribute("tabindex", "0");
                this.handlesLegend.push(on(td, "blur", lang.hitch(this, function (evt) {
                    var value = getValueFromLabel(evt);
                    if (typeof value !== "undefined") {
                        this.fireEvent(value, "onmouseout");
                    }
                })));

                this.handlesLegend.push(on(td, "focus", lang.hitch(this, function (evt) {
                    var value = getValueFromLabel(evt);
                    if (typeof value !== "undefined") {
                        this.fireEvent(value, "onmouseover");
                    }
                })));
            }));
        },


        removeBorder: function() {
            this.inherited(arguments);
        },

        addBorder: function() {
            this.inherited(arguments);
        }
    });
});
