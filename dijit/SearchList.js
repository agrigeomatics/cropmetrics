define([
    "require",
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/aspect",
    "dojo/dom",
    "dojo/dom-construct",
    "dojo/Evented",
    "dojo/on",
    "dojo/promise/all",
    "dojo/query",
    "dojo/string",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/a11y",
    "dijit/a11yclick",
    "dijit/focus",
    "dijit/form/Button",
    "dijit/registry",
    "esri/Color",
    "esri/graphic",
    "esri/dijit/Search",
    "esri/layers/GraphicsLayer",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/tasks/query",
    "dojo/i18n!./nls/searchList",
    "dojo/text!./templates/searchList.html",
    "xstyle/css!./css/searchList.css"
], function (require, array, lang, declare, aspect, dom, domConstruct, Evented, on, all, query, string,
    _WidgetBase, _TemplatedMixin, a11y, a11yclick, focus, Button, registry, Color, Graphic,
    Search, GraphicsLayer, SimpleLineSymbol, SimpleFillSymbol, Query,
    i18n, template) {

        var declared = declare([_WidgetBase, _TemplatedMixin, Evented], {
            i18n: i18n,
            templateString: template,
            id: "metrics-dijit-search-list",
            map: null,
            sources: [],
            allPlaceholder: "",
            maxFeatureList: 50,

            mapIcon: null,

            // handles related to the feature list
            handlesFeatureList: [],

            // templates, associated to sources, in the same array index order
            templates: [],

            // function that add & modify attributes / values
            transforms: [],

            graphicLayer: null,

            highlightSymbol: null,

            lastFocused: null,

            lastSection: null,
            lastSourceIdx: null,

            constructor: function (params, srcNodeRef) {
                //jshint unused:false
                this.mapIcon = require.toUrl("./images/searchListMap.png");
                this.lastFocused = null;
            },

            postCreate: function () {
                this.inherited(arguments);

                var params = {
                    enableButtonMode: false, //this enables the search widget to display as a single button
                    map: this.map,
                    expanded: false,
                    minCharacters: 3,
                    allPlaceholder: this.allPlaceholder,
                    enableSourcesMenu: false,
                    enableHighlight: false,
                    autoNavigate: false,
                    autoSelect: false
                };

                this.highlightSymbol = new SimpleFillSymbol(
                    SimpleFillSymbol.STYLE_SOLID,
                    new SimpleLineSymbol(
                        SimpleLineSymbol.STYLE_SOLID,
                        new Color([255, 0, 0]), 3
                    ),
                    new Color([125, 125, 125, 0.35])
                );

                // Create the Graphic Layer
                this.graphicLayer = new GraphicsLayer();

                // Add it to the map
                this.map.addLayer(this.graphicLayer);

                if (this.sources.length > 1) {
                    params.enableSourcesMenu = true;
                }

                this.searchWidget = new Search(params, this.searchAttributeNode);

                if (typeof this.sources !== "undefined") {
                    this.searchWidget.set("sources", this.sources);
                }

                this.initSearch();

            },

            changeSearchParams: function(params) {
                this.templates = params.templates;
                this.i18nTemplates = params.i18nTemplates;

                this.sources = params.sources;
                this.searchWidget.set("sources", this.sources);

                this.transforms = params.transforms;

                this.clear();
            },

            startup: function () {
                this.inherited(arguments);

                this.searchWidget.startup();
            },

            destroy: function (preserveDom) {
                //jshint unused:false
                if (this.searchWidget) {
                    this.searchWidget.destroy();
                }

                this.inherited(arguments);
            },

            refocus: function () {
                if (this.lastFocused) {
                    focus.focus(this.lastFocused);
                }
            },

            showGraphics: function () {
                if (this.graphicLayer && !this.graphicLayer.visible) {
                    this.graphicLayer.show();
                }
            },

            hideGraphics: function () {
                if (this.graphicLayer && this.graphicLayer.visible) {
                    this.graphicLayer.hide();
                }
            },

            initSearch: function () {
                this.searchWidget.on('search-results', lang.hitch(this, function (e) {
                    if (e && e.results) {
                        var features = [];
                        var idxSources = [];

                        var putFeature = function (ikey, item) {
                            features.push(item.feature);
                            idxSources.push(ikey);
                        };

                        for (var key in e.results) {
                            if (e.results.hasOwnProperty(key)) {
                                var element = e.results[key];
                                var ikey = parseInt(key);
                                array.forEach(element, lang.partial(putFeature, ikey));
                            }
                        }

                        this.updateList(features, idxSources);
                    }
                }));

                /**
                 * When the search is cleared update the
                 * list and refocus the search input
                 */
                this.own(this.searchWidget.on('clear-search', lang.hitch(this, function (e) {
                    if (e && e.results) {
                        var features = [];

                        for (var key in e.results) {
                            if (e.results.hasOwnProperty(key)) {
                                var element = e.results[key];
                                features.push(element.feature);
                            }
                        }

                        this.updateList(features);
                        this.searchWidget.focus();
                    }
                })));

                this.own(on(this.searchExtent, a11yclick, lang.hitch(this, function () {
                    this._searchAllWithinMap();
                })));
                this.own(on(this.searchWidget, "focus", lang.hitch(this, function () {
                    this.lastFocused = this.searchWidget.inputNode;
                })));
            },

            clear: function () {

                // Clear list
                array.forEach(this.handlesFeatureList, function (item) {
                    item.remove();
                    item = null;
                });
                this.handlesFeatureList = [];

                while (this.list.firstChild) {
                    this.list.removeChild(this.list.firstChild);
                }

                this.lastFocused = null;

            },

            updateList: function (features, idxSources) {
                this.clear();

                var count = 0;

                array.forEach(features, lang.hitch(this, function (feature, idxFeature) {
                    // Apply a limit
                    if (count >= this.maxFeatureList) {
                        return;
                    }

                    var attributes = feature.attributes || feature.feature.attributes;
                    var scope = {};

                    var isSourceFound = false;
                    var sourceIdx = -1;

                    if (idxSources) {
                        sourceIdx = idxSources[idxFeature];
                        isSourceFound = true;
                    } else {
                        // determine which source
                        isSourceFound = array.some(this.sources, function (item, idx) {
                            var isSame = feature._layer === item.featureLayer;
                            if (isSame) {
                                sourceIdx = idx;
                            }
                            return isSame;
                        });
                    }

                    if (isSourceFound) {
                        for (var attribute in attributes) {
                            if (attributes.hasOwnProperty(attribute)) {
                                // Replace . by _ in attribute name when having join
                                // the dot don't work well with string.substitute
                                scope[attribute.replace(".", "_")] = attributes[attribute];
                            }
                        }
                        // Transform attribute value
                        if (this.transforms && this.transforms[sourceIdx]) {
                            this.transforms[sourceIdx](scope);
                        }

                        var template = lang.clone(this.templates[sourceIdx]);

                        var substituteScope = scope;
                        if (this.i18nTemplates && this.i18nTemplates[sourceIdx]) {
                            substituteScope.i18nTemplate = this.i18nTemplates[sourceIdx];
                        }

                        var attrs = {
                            innerHTML: string.substitute(template, substituteScope)
                        };

                        var node = domConstruct.create("div", attrs, this.list);
                        var sections = query("section", node);
                        if (sections && sections.length && sections.length > 0) {
                            var handles = [];
                            handles.push(on(sections[0], "focus", lang.hitch(this, this.centerFeature, sections[0], sourceIdx)));
                            var buttons = query("button[name='metrics-search-list-select']", node);
                            if (buttons && buttons.length && buttons.length > 0) {
                                // button select focus
                                handles.push(on(buttons[0], "focus", lang.hitch(this, this.centerFeature, sections[0], sourceIdx)));
                                // button select click
                                handles.push(on(buttons[0], a11yclick, lang.hitch(this, this.selectFeature, sections[0], sourceIdx)));
                            }

                            array.forEach(handles, lang.hitch(this, function (handle) {
                                this.own(handle);
                                this.handlesFeatureList.push(handle);
                            }));
                        }

                        count += 1;
                    }
                }));

                var liveRegionText = count + " " + i18n.results + ".";
                this.results.innerText = liveRegionText;
                return count;
            },

            queryFeature: function (section, sourceIdx, e) {
                var promise = null;
                if (section &&
                    section.dataset) {

                    this.lastFocused = e.currentTarget;

                    var id = section.dataset.id;
                    var idName = section.dataset.idName;

                    if (id && idName) {
                        var featureLayer = this.sources[sourceIdx].featureLayer;
                        var query = new Query();
                        query.outFields = this.sources[sourceIdx].outFields;
                        query.returnGeometry = true;
                        query.where = idName + "=" + id;
                        query.outSpatialReference = this.map.spatialReference;

                        var searchQueryParams = this.sources[sourceIdx].searchQueryParams;
                        if (searchQueryParams) {
                            lang.mixin(query, searchQueryParams);
                        }

                        promise = featureLayer.queryFeatures(query).then(lang.hitch(this, function (results) {
                            if (results && results.features && results.features.length > 0) {
                                return results.features[0];
                            }
                        }));
                    }
                }
                return promise;
            },

            centerFeature: function (section, sourceIdx, e) {
                // don't do the same query twice
                if (this.lastSection !== null &&
                    this.lastSourceIdx !== null &&
                    this.lastSection === section &&
                    this.lastSourceIdx === sourceIdx) {
                    return;
                }

                this.lastSection = section;
                this.lastSourceIdx = sourceIdx;

                if (section && section.dataset) {
                    var query = this.queryFeature(section, sourceIdx, e);
                    if (query) {
                        query.then(lang.hitch(this, function (feature) {
                            if (feature && feature.geometry) {
                                this.centerAt(feature.geometry);
                            }
                        }));
                    }
                }
            },

            selectFeature: function (section, sourceIdx, e) {
                if (section && section.dataset) {
                    var query = this.queryFeature(section, sourceIdx, e);
                    if (query) {
                        query.then(lang.hitch(this, function (feature) {
                            if (feature && feature.geometry) {
                                this.emit("feature-select", {
                                    feature: feature
                                });
                            }
                        }));
                    }
                }
            },

            centerAt: function (geometry) {
                var isPolygon = geometry.type === "polygon";
                var isExtent = geometry.type === "extent";
                var isPoint = geometry.type === "point";

                if (!this.graphicLayer.visible) {
                    this.graphicLayer.show();
                }

                if (isPolygon || isExtent) {
                    this.map.centerAt(geometry.getCentroid());
                } else if (isPoint) {
                    this.map.centerAt(geometry);
                }
                this.graphicLayer.clear();
                var highlightGraphic = new Graphic(geometry, this.highlightSymbol);
                this.graphicLayer.add(highlightGraphic);
            },

            updateListQuery: function (results) {
                var features = [];
                array.forEach(results, lang.hitch(this, function (result) {
                    // replace field names by field aliases
                    array.forEach(result.features, lang.hitch(this, function (feature) {
                        for (var key in result.fieldAliases) {
                            if (result.fieldAliases.hasOwnProperty(key)) {
                                var fieldAlias = result.fieldAliases[key];
                                var hasKey = key in feature.attributes;
                                if (hasKey) {
                                    feature.attributes[fieldAlias] = feature.attributes[key];
                                }
                            }
                        }
                        features.push(feature);
                    }));
                }));

                return this.updateList(features);
            },

            focusFirstItemInList: function () {
                var firstFocusableItem = a11y.getFirstInTabbingOrder(this.list);
                if (firstFocusableItem) {
                    focus.focus(firstFocusableItem);
                }
            },

            _searchAllWithinMap: function () {
                var promises = [];

                var sources = this.sources;
                if (this.searchWidget.activeSourceIndex !== "all") {
                    sources = [sources[this.searchWidget.activeSourceIndex]];
                }

                array.forEach(sources, lang.hitch(this, function (source, sourceIdx) {
                    var featureLayer = source.featureLayer;
                    if (featureLayer) {
                        var query = new Query();
                        query.geometry = this.map.extent;
                        query.outFields = source.outFields;
                        query.returnGeometry = false;
                        query.spatialRelationship = Query.SPATIAL_REL_ENVELOPEINTERSECTS;

                        var searchQueryParams = this.sources[sourceIdx].searchQueryParams;
                        if (searchQueryParams) {
                            lang.mixin(query, searchQueryParams);
                        }
                        promises.push(featureLayer.queryFeatures(query));
                    }
                }));
                all(promises).then(lang.hitch(this, function (results) {
                    var count = this.updateListQuery(results);
                    if (count > 0) {
                        // focus the first item in the list
                        this.focusFirstItemInList();
                    } else {
                        this.lastFocused = this.searchExtent;
                    }
                }));
            }

        });

        return declared;

    });
