define([
        "dojo/dom-construct",
        "CM2/dijit/LayersMetricsConfig",
        "CM2/dijit/LayersMetrics",
        "dojo/domReady!"
       ],
       function(domConstruct,
                LayersMetricsConfig, LayersMetrics)
    {

        function loadMetricsWidget() {
            var paramsMetrics = { };
            var nodeMetrics = domConstruct.create("div", {id: "divNodeMetrics"}, document.body, "first");

            CM2.layersMetricsWidget = new LayersMetrics(paramsMetrics, nodeMetrics);
            CM2.LayersMetrics.startup();
        }

        return  {

            init: function () {

                // Load layers metrics config
                CM2.metricsConfig = new LayersMetricsConfig({ path: location.pathname.replace(/\/[^\/]+$/, "") + "/config/config.json" });
                var promiseConfig = CM2.metricsConfig.load();

                //loadMetricsWidget();
                promiseConfig.then(function (results) {
                    LayersMetrics.startup();
                });
            
            }

        };

    }
);
