define({
    root: {
        reset: "Reset",
        reset_title: "Reset the application to it's initial state.",
        refresh: "Refresh",
        refresh_title: "Refresh the report using selected options."
    },
    fr: true
});
