define({
    root: {
        widget: {
            point: "Point",
            polygon: "Polygon",
            car: "CAR",

            query_tab: "Search",
            report_tab: "Report",
            help_tab: "Help",

            map_draw_select: "Draw and Select",
            locate_search: "Search",
            options: "Settings",

            query_mode: "Drawing or Selection Method:",
            query_mode_point: "Draw a point on the map",
            query_mode_polygon: "Draw a polygon on the map",
            query_mode_selection: "Select a polygon on the map",

            instructions: "Instructions:",
            instructions_point: "Please draw a point by clicking on the map.",
            instructions_polygon: "Please draw a polygon on the map.",
            instructions_select: "Please select a polygon by clicking on the map.",

            no_data: "Data unavailable at this location. Please try again.",

            not_available: "Not Available",

            about: {
                about: "About",
                version: "Version",
                last_modified: "Last modified",
                organization: "Agriculture and Agri-Food Canada"
            },

            help: {
                help: "Help",
                instructions: "Help Page"
            }
        },
        units: {
            kilometer: {
                abbr: "km",
                singular: "kilometer",
                plurial: "kilometers"
            },
            meter: {
                abbr: "m",
                singular: "meter",
                plurial: "meters"
            },
            centimeter: {
                abbr: "cm",
                singular: "centimeter",
                plurial: "centimeters"
            },
            millimeter: {
                abbr: "mm",
                singular: "millimeter",
                plurial: "millimeters"
            },
            inch: {
                abbr: "in",
                singular: "inch",
                plurial: "inches"
            },
            foot: {
                abbr: "ft",
                singular: "foot",
                plurial: "feet"
            },
            yard: {
                abbr: "yd",
                singular: "foot",
                plurial: "yards"
            },
            mile: {
                abbr: "mi",
                singular: "mile",
                plurial: "miles"
            },
            nautical_mile: {
                abbr: "nmi",
                singular: "nautical mile",
                plurial: "nautical miles"
            },
            percent: {
                abbr: "%",
                singular: "percents",
                plurial: "percent"
            },
            person: {
                abbr: "pers.",
                singular: "person",
                plurial: "persons"
            },
            per_capita: {
                abbr: "per capita"
            },
            millimeter_per_capita: {
                abbr: "mm per capita",
                singular: "millimeter per capita",
                plurial: "millimeters per capita"
            },
            ton_hectare: {
                abbr: "T / ha",
                singular: "ton / hectare",
                plurial: "tonnes / hectare"
            },
            bushel_acre: {
                abbr: "bu / ac",
                singular: "bushel / acre",
                plurial: "bushels / acre"
            },
            gdd: {
                abbr: "GDD",
                singular: "Growing Degree-Days",
                plurial: "Growing Degree-Days"
            },
            chu: {
                abbr: "CHU",
                singular: "Crop Heat Unit",
                plurial: "Crop Heat Units"
            },
            kelvin: {
                abbr: "K",
                singular: "Kelvin",
                plurial: "Kelvin"
            },
            celcius: {
                abbr: "°C",
                singular: "°C",
                plurial: "°C"
            }
        }
    },
    fr: true
});
