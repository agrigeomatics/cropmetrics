define([
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/Deferred",
    "dojo/promise/all",
    "dojo/when",
], function (array, lang, declare, Deferred, all, when) {
    return declare([], {

        path: null,

        config: null,

        // {Object.<string, function>}
        transform: null,

        // {{i18n: i18n options file}}
        optionsFiles: null,

        // Array
        reportsFiles: null,

        // {Object.<number, Object.<string, Object>>}
        // key : additionalLayer config index
        // value : {i18n: i18n file || null }
        additionalLayersFiles: null,

        // {Object.<number, Object.<string, Object>>}
        // key : searchList config index
        // value : {{i18n: i18n file || null, template: template file || null}}
        searchListFiles: null,

        constructor: function (params) {
            //jshint unused:false

            lang.mixin(this, params);

            if (!this.path) {
                throw "LayersMetricsConfig: invalid parameter: path";
            }
        },

        /**
         * Get files requirements referenced in the config file.
         * (Function file and/or options i18n)
         *
         * @returns {external:Promise} config file requirements promise
         */
        load: function () {
            var deferred = new Deferred();
            require(["dojo/text!" + this.path], lang.hitch(this, function (config) {
                this.config = JSON.parse(config);
                deferred.resolve(this.config);
            }));

            return deferred.then(lang.hitch(this, function () {
                var promiseTransform = this.loadTransform();
                var promiseOptionsi18n = this.loadOptionsi18n();
                var promiseReportsFiles = this.loadReportsFiles();
                var promiseAdditionalLayers = this.loadAdditionalLayersFiles();
                var promiseSearchList = this.loadSearchListFiles();

                return all([
                    promiseTransform,
                    promiseOptionsi18n,
                    promiseReportsFiles,
                    promiseAdditionalLayers,
                    promiseSearchList
                ]).then(lang.hitch(this, function() {
                    return this;
                }));
            }));
        },

        loadTransform: function () {
            var deferred = new Deferred();

            this.transform = {};

            if (this.config.functionFile) {

                require([this.config.functionFile], lang.hitch(this, function (functionFile) {
                    this.transform = functionFile;

                    deferred.resolve(this.transform);
                }));

            } else {
                deferred.resolve(this.transform);
            }

            return deferred.promise;
        },

        loadOptionsi18n: function () {
            var deferred = new Deferred();

            this.optionsFiles = { i18n: null };

            if (this.config.options && this.config.options.i18n) {

                require(["dojo/i18n!" + this.config.options.i18n], lang.hitch(this, function (i18n) {
                    this.optionsFiles.i18n = i18n;

                    deferred.resolve(this.optionsFile);
                }));

            } else {
                deferred.resolve(this.optionsFile);
            }

            return deferred.promise;
        },

        /**
         * Load all specified reports files (templates, i18n) specified in the config file
         * @return {Array.<external:Promise>} Reports files promises
         */
        loadReportsFiles: function () {
            var promise;

            this.reportsFiles = [];

            if (this.config.reports) {

                var promises = [];
                array.forEach(this.config.reports, lang.hitch(this, function (configReport) {
                    var promise = this._getReportFiles(configReport);
                    promises.push(promise);
                }));

                promise = all(promises).then(lang.hitch(this, function (reportsFiles) {
                    this.reportsFiles = reportsFiles;

                    return this.reportsFiles;
                }));
            } else {
                promise = when(this.reportsFiles);
            }

            return promise;
        },

        /**
         * Prepare path and dict to require config
         *
         * @param  {Array} itemsConfig Config array
         * @param  {Array.{{name: string, prefix: string}}>} properties path name & prefix
         * @param  {Array.<string>} paths path to require files
         * @param  {Object.<number, Object.<number, number>>} dict key : path property index : dict(key config index; value : path index )
         */
        _prepareRequire: function (itemsConfig, properties, paths, dict) {
            // Filter
            array.forEach(itemsConfig, function (config, indexConfig) {
                array.forEach(properties, function (pathPropertie, indexProperty) {
                    if (!dict[indexProperty]) {
                        dict[indexProperty] = {};
                    }
                    var pathName;
                    if (pathPropertie.prefix) {
                        pathName = pathPropertie.prefix + config[pathPropertie.name];
                    } else {
                        pathName = config[pathPropertie.name];
                    }

                    var indexPath = paths.indexOf(pathName);

                    if (pathName && indexPath < 0) {
                        indexPath = (paths.push(pathName) - 1);
                    }

                    dict[indexProperty][indexConfig] = indexPath;
                });
            });

        },

        /**
         * Do a require to get a module given a path, return a promise
         *
         */
        _getModule: function () {
            var paths;
            // if first argument is an array
            if (arguments.length > 0 && lang.isArray(arguments[0])) {
                paths = arguments[0];
            } else {
                paths = [];
                array.forEach(arguments, function (path) {
                    if (path) {
                        paths.push(path);
                    }
                });
            }
            if (paths.length === 0) {
                return when([]);
            } else {
                var deferred = new Deferred();
                require(paths, function () {
                    deferred.resolve(arguments);
                });
                return deferred.promise;
            }
        },

        loadSearchListFiles: function () {
            var promise;

            // key : searchList config index
            // value : {{i18n: i18n file || null, template: template file || null}}
            this.searchListFiles = {};

            if (this.config.searchList) {

                // {Array.<string>} paths path to require files
                var paths = [];

                // {Object.<number, Object.<number, number>>} dict key : indexProperty : dict(key indexConfig; value : indexPath )
                var dict = {};

                var properties = [{ name: "i18n", prefix: "dojo/i18n!" },
                                  { name: "template", prefix: "dojo/text!" }];

                this._prepareRequire(
                    this.config.searchList,
                    properties,
                    paths,
                    dict);

                promise = this._getModule(paths).then(lang.hitch(this, function (items) {
                    array.forEach(this.config.searchList, lang.hitch(this, function (searchList, indexConfig) {
                        var item = {};
                        array.forEach(properties, lang.hitch(this, function (property, indexProperty) {
                            var indexPath = dict[indexProperty][indexConfig];
                            item[property.name] = items[indexPath] || null;
                        }));
                        this.searchListFiles[indexConfig] = item;
                    }));
                    return this.searchListFiles;
                }));

            } else {
                promise = when(this.searchListFiles);
            }
            return promise;
        },

        loadAdditionalLayersFiles: function () {
            var promise;

            // key : additionalLayer config index
            // value : {{i18n: i18n file || null }}
            this.additionalLayersFiles = {};

            if (this.config.additionalLayer) {

                // {Array.<string>} paths path to require files
                var paths = [];

                // {Object.<number, Object.<number, number>>} dict key : indexProperty : dict(key indexConfig; value : indexPath )
                var dict = {};

                var properties = [{ name: "i18n", prefix: "dojo/i18n!" }];

                this._prepareRequire(
                    this.config.additionalLayer,
                    properties,
                    paths,
                    dict);

                promise = this._getModule(paths).then(lang.hitch(this, function (items) {
                    array.forEach(this.config.additionalLayer, lang.hitch(this, function (additionalLayer, indexConfig) {
                        var item = {};
                        array.forEach(properties, lang.hitch(this, function (property, indexProperty) {
                            var indexPath = dict[indexProperty][indexConfig];
                            item[property.name] = items[indexPath] || null;
                        }));
                        this.additionalLayersFiles[indexConfig] = item;
                    }));
                    return this.additionalLayersFiles;
                }));

            } else {
                promise = when(this.additionalLayersFiles);
            }
            return promise;
        },

        /**
         * Load the specified report files (templates, i18n) specified in the config file
         *
         * @param  {Object} configReport
         * @return {external:Promise} Report files promise
         */
        _getReportFiles: function (configReport) {
            var paths = [];

            var indexes = {
                templateDefault: -1,
                templateNoData: -1,
                templateData: -1,
                i18nReport: -1
            };

            if (typeof configReport.templates !== "undefined") {
                // HTML default
                if (typeof configReport.templates.default !== "undefined") {
                    indexes.templateDefault = (paths.push("dojo/text!" + configReport.templates.default) - 1);
                }
                // HTML without data
                if (typeof configReport.templates.no_data !== "undefined") {
                    indexes.templateNoData = (paths.push("dojo/text!" + configReport.templates.no_data) - 1);
                }
                // HTML with data
                if (typeof configReport.templates.data !== "undefined") {
                    indexes.templateData = (paths.push("dojo/text!" + configReport.templates.data) - 1);
                }
            }

            // i18n
            if (typeof configReport.i18n !== "undefined") {
                indexes.i18nReport = (paths.push("dojo/i18n!" + configReport.i18n) - 1);
            }

            var promise;
            if (paths.length > 0) {
                promise = this._getModule(paths).then(function (items) {
                    var files = {};
                    if (indexes.templateDefault >= 0) {
                        files.templateDefault = items[indexes.templateDefault];
                    }
                    if (indexes.templateNoData >= 0) {
                        files.templateNoData = items[indexes.templateNoData];
                    }
                    if (indexes.templateData >= 0) {
                        files.templateData = items[indexes.templateData];
                    }
                    if (indexes.i18nReport >= 0) {
                        files.i18nReport = items[indexes.i18nReport];
                    }
                    lang.mixin(files, { configReport: configReport });
                    return files;
                });
            }
            else {
                promise = when({ configReport: configReport });
            }
            return promise;
        }
    });

});
