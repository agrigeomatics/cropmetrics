define(["require",
        "dojo/_base/array",
        "dojo/_base/declare",
        "dojo/_base/lang",
        "dojo/aspect",
        "dojo/Deferred",
        "dojo/dom",
        "dojo/dom-construct",
        "dojo/dom-style",
        "dojo/on",

        "dojo/parser",
        "dojo/promise/all",
        "dojo/query",
        "dojo/ready",
        "dojo/request/xhr",
        "dojo/string",
        "dojo/number",
        "dojo/store/Memory",
        "dojo/when",
        "dojo/Evented",

        "dijit/layout/ContentPane",
        "dijit/layout/LayoutContainer",
        "dijit/layout/TabContainer",
        "dijit/layout/TabController",
        "dijit/registry",

        "esri/config",
        "esri/tasks/GeometryService",
        "esri/tasks/ProjectParameters",
        "esri/layers/FeatureLayer",
        "esri/layers/MosaicRule",
        "esri/layers/RasterFunction",

        "esri/TimeExtent",
        "esri/tasks/ImageServiceIdentifyTask",
        "esri/tasks/ImageServiceIdentifyParameters",
        "esri/tasks/QueryTask",
        "esri/tasks/query",
        "esri/tasks/RelationshipQuery",

        "esri/geometry/geometryEngine",
        "esri/geometry/jsonUtils",
        "esri/geometry/Point",
        "esri/geometry/webMercatorUtils",
        "esri/SpatialReference",

        "./SearchList",
        "./Settings",
        "./Report",
        "../toolbars/Draw",

        "dojo/i18n!./nls/layersMetrics",
        "xstyle/css!./css/layersMetrics.css"
       ],
       function (
            require, array, declare, lang, aspect, Deferred, dom, domConstruct, domStyle, on,
            parser, all, query, ready, xhr, string, numberUtils, Memory, when, Evented,
            ContentPane, LayoutContainer, TabContainer, TabController, registry,
            esriConfig, GeometryService, ProjectParameters, FeatureLayer, MosaicRule, RasterFunction,
            TimeExtent, ImageServiceIdentifyTask, ImageServiceIdentifyParameters, QueryTask, EsriQuery, RelationshipQuery,
            geometryEngine, geometryJsonUtils, Point, webMercatorUtils, SpatialReference,
            SearchList, Settings, Report, Draw, i18n)

    {
        return {

            // Widget internationalization file
            i18n: i18n,

            initialExtent: null,

            // config loader : has loaded all required files
            configLoader: undefined,

            // config shorcut
            transform: undefined,

            isBusy: false,

            config: undefined,

            map: undefined,
            reports: null,
            ready: null,

            _resultsTabsShowHandle: null,
            _searchListSelectFeatureHandle: null,
            _searchListLayerAddedHandle: null,

            // Different query types
            tasks: {
                IMAGE_SERVICE_HISTOGRAM: "computeHistograms",
                INTERPOLATE_CSV: "interpolate_csv",
                IMAGE_SERVICE_ATTRIBUTE_TABLE: "rasterattributetable",
                IMAGE_IDENTIFY: "imageServiceIdentify",
                QUERY: "query",
                WMS_FEATURE_INFO: "wmsFeatureInfo",
                GET_SAMPLES: "getSamples",
            },

            // Subprocess within a query
            subprocess: {
                MASK_IDENTIFY_IMAGE_SERVICE: "imageServiceIdentifyMask",
                MASK_GET_SAMPLES: "getSamplesMask",
            },

            // Chart type in config
            reportTypes: {
                CHART_LINE: "chart_line",
                CHART_PIE: "chart_pie",
                CHART_BAR: "chart_bar",
                CHART_HISTOGRAM: "chart_histogram",
                GAUGE_BAR: "gauge_bar"
            },

            ESRI_FIELD_TYPE_OID: "esriFieldTypeOID",

            layerServiceType: {
                IMAGE_SERVICE: "IMAGE_SERVICE",
                MAP_SERVICE: "MAP_SERVICE",
                FEATURE_SERVICE: "FEATURE_SERVICE",
                WMS_SERVICE: "wms"
            },

            selectionModeType: {
                point: "point",
                polygon: "polygon",
                selection: "selection"
            },

            Xconstructor: function (params, srcNodeRef) {
                //jshint unused:false
                this.reports = [];
            },

            Xdestroy: function (preserveDom) {
                //jshint unused:false
                this.inherited(arguments);
                this.reports = [];
            },

            XpostMixInProperties: function () {
                /* CSS default values */
                if (this.configLoader) {
                    this.config = this.configLoader.config;
                }
                this.inherited(arguments);
            },

            XpostCreate: function () {
                this.inherited(arguments);

                this.geometryService = new GeometryService(esriConfig.defaults.geometryService.url);

                // used for config.functionName && config.tooltipFormat
                this.transform = this.configLoader.transform;

                var replace = { i18n: this.i18n };
                if (this.configLoader.optionsFiles && this.configLoader.optionsFiles.i18n) {
                    replace.i18nOptions = this.configLoader.optionsFiles.i18n;
                }

                var params = {
                    config: this.config.options,
                    replace: replace,
                };

                if (this.config.options && this.config.options.functionName && this.transform) {
                    params.transform = this.transform[this.config.options.functionName];
                }

                this.settings = new Settings(params, this.nodeSettings);
                this.own(this.settings);

                domStyle.set(this.tabsNode, "visibility", "hidden");
            },

            startup: function () {
                this.map = CM2.map;
                this.reports = [];
                this.configLoader = CM2.metricsConfig;
                this.config = this.configLoader.config;

                var url = "http://sgeo3/arcgis/rest/services/Utilities/Geometry/GeometryServer";
                this.geometryService = new GeometryService(url);

                // used for config.functionName && config.tooltipFormat
                this.transform = this.configLoader.transform;
                                
                var replace = { i18n: this.i18n };
                if (this.configLoader.optionsFiles && this.configLoader.optionsFiles.i18n) {
                    replace.i18nOptions = this.configLoader.optionsFiles.i18n;
                }
                
                var params = {
                    config: this.config.options,
                    replace: replace,
                }; 
               
                if (this.config.options && this.config.options.functionName && this.transform) {
                    params.transform = this.transform[this.config.options.functionName];
                }
                
                this.settings = new Settings(params/*, this.nodeSettings*/);
                
                CM2.LayerMetrics = this;
            },

            resize: function () {
            },

            listenSettingsChange: function (e) {
            },

            /**
             * Add a tab role on each TabContainer tab
             *
             */
            _accessibilityTabsIcons: function () {
                var iconNode = query("." + this.tabQuery.iconClass, this.domNode)[0];
                iconNode.tabIndex = 0;
                iconNode.setAttribute("role", "tab");

                iconNode = query("." + this.tabResults.iconClass, this.domNode)[0];
                iconNode.tabIndex = 0;
                iconNode.setAttribute("role", "tab");

                iconNode = query("." + this.tabHelp.iconClass, this.domNode)[0];
                iconNode.tabIndex = 0;
                iconNode.setAttribute("role", "tab");
            },


            /**
             * Set the default drawing mode (selection)
             *
             */
            _activateDefaultDrawingMode: function () {
                this.selectionModeInstructions.innerHTML = this.i18n.widget.instructions_select;
                this._activateSelection(this.draw.GEOMETRY_TYPE_POINT);
                this.selectionModeSelection.checked = true;
            },

            /**
             * Reset the widget view and beviaour at initial state
             */
            reset: function () {
                // desactivate drawing
                if (this.draw) {
                    this.draw.clear();
                    this.draw.desactivateDraw();
                    this._activateDefaultDrawingMode();
                }

                // Select the first tab
                this.tabs.selectChild(this.tabResults, true);
                this.domNode.scrollTop = 0;

                this._showReportsDefaultView();

                if (this.initialExtent) {
                    this.map.setExtent(this.initialExtent, true);
                }
            },


            /**
             * Refresh the application by reapplying another query using the current draw geometry
             *
             */
            refresh: function () {
                if (this.draw) {
                    var graphics = this.draw.getSelectedGraphics();
                    if (graphics && graphics.length > 0) {

                        // Select the first tab
                        this.tabs.selectChild(this.tabQuery, true);
                        this.domNode.scrollTop = 0;

                        // Query metrics
                        this._showMetrics(graphics);
                    }
                }
            },

            /**
             * Read the search list config and return params to create feature layers
             *
             */
            _getSearchListFeatureLayerParams: function () {
                var params = [];

                var optionsData = this.settings.getOptions();

                array.forEach(this.config.searchList, lang.hitch(this, function (item) {
                    var config = lang.clone(item);
                    var param = {
                        layer: {},
                        config: config
                    };
                    var isUrl = typeof config.url !== "undefined";
                    var isId = typeof config.id !== "undefined";
                    if (isUrl) {
                        var url = config.url;
                        if (optionsData) {
                            url = string.substitute(url, { options: optionsData });
                        }
                        param.layer.url = url;
                    } else if (isId) {
                        param.layer.layerId = config.id;
                        param.layer.map = this.map;
                    }
                    params.push(param);
                }));
                return params;
            },

            _getSearchListParams: function (featureLayer, config, files) {
                var template = files.template;
                var i18nTemplate = files.i18n;

                // Options
                var optionsData = this.settings.getOptions();

                // Replace some search parameters by options values
                var source = lang.clone(config.source);

                source = JSON.stringify(source);
                source = string.substitute(source, { options: optionsData, i18nTemplate: i18nTemplate });
                source = JSON.parse(source);

                source.featureLayer = featureLayer;

                // i18n substitute template
                var templateSubstitute = string.substitute(template, { options: optionsData, i18nTemplate: i18nTemplate });
                var params = { source: source, template: templateSubstitute, i18nTemplate: i18nTemplate, transform: null };

                // functionName
                if (typeof config.functionName !== "undefined" &&
                    typeof this.transform !== "undefined" &&
                    typeof this.transform[config.functionName] !== "undefined") {
                    var transform = this.transform[config.functionName];
                    params.transform = lang.partial(transform, { options: optionsData, i18nTemplate: i18nTemplate });
                }
                return params;
            },

            _initSearchList: function () {
                var params = this._getSearchListFeatureLayerParams();

                // {Object.<number, Object.<string, Object>>}
                // key : searchList config index
                // value : {{i18n: i18n file || null, template: template file || null}}
                var searchListFiles = this.configLoader.searchListFiles;

                var promises = [];
                // For each feature layer search list parameter
                array.forEach(params, lang.hitch(this, function (param, indexConfig) {
                    var promise = layersUtils.createFeatureLayer(param.layer).then(lang.hitch(this, function (featureLayer) {
                        var files = searchListFiles[indexConfig];
                        return this._getSearchListParams(featureLayer, param.config, files);
                    }));
                    promises.push(promise);
                }));

                var searchListParams = {
                    templates: [],
                    i18nTemplates: [],
                    sources: [],
                    transforms: [],
                    map: this.map
                };

                all(promises).then(lang.hitch(this, function (searchParams) {
                    array.forEach(searchParams, lang.hitch(this, function (searchParam) {
                        searchListParams.templates.push(searchParam.template);
                        searchListParams.i18nTemplates.push(searchParam.i18nTemplate);
                        searchListParams.sources.push(searchParam.source);
                        searchListParams.transforms.push(searchParam.transform);
                    }));

                    // SearchList
                    if (!this.searchList) {
                        this.searchList = new SearchList(searchListParams, this.nodeSearchList);
                        this.searchList.startup();

                        this.initFocusEvents();

                    } else {
                        this.searchList.changeSearchParams(searchListParams);
                    }

                    // Connect SearchList selectFeature with drawing select feature
                    this._searchListSelectFeature();

                    // Connect if layer added in map is same as in the config, init search list again
                    this._searchListLayerAdded();

                }));
            },

            initFocusEvents: function () {
                var show = lang.hitch(this, function () {
                    // re-focus
                    this.searchList.refocus();
                    // show search list graphics
                    this.searchList.showGraphics();
                    this.draw.hide();
                });

                var hide = lang.hitch(this, function () {
                    // hide search list graphics
                    this.searchList.hideGraphics();
                    this.draw.show();
                });

                this.own(aspect.after(this.tabs, "selectChild", lang.hitch(this, function () {
                    var selected = this.tabQuery.selected && this.searchContentPane.selected;
                    if (selected) {
                        show();
                    } else {
                        hide();
                    }

                    if (this.tabResults.selected) {
                        if (!this.isBusy) {
                            var graphics = this.draw.getSelectedGraphics();
                            if (graphics && graphics.length > 0) {
                                this.centerAt(graphics[0].geometry);
                            }
                        }
                    }

                }), true));

            },


            /**
             * Connect SearchList selectFeature with drawing select feature
             *
             */
            _searchListSelectFeature: function () {
                if (!this._searchListSelectFeatureHandle) {
                    // Connect select feature with drawing
                    this._searchListSelectFeatureHandle = on(this.searchList, "feature-select", lang.hitch(this, function (e) {
                        if (this.draw) {
                            var geometry = geometryJsonUtils.fromJson(e.feature.geometry);
                            this.draw.addGraphics([geometry]);
                        }
                    }));
                    this.own(this._searchListSelectFeatureHandle);
                }
            },


            /**
             * Connect if layer added in map is same as in the config : init search list again
             */
            _searchListLayerAdded: function () {
                if (!this._searchListLayerAddedHandle) {
                    // Listen layer re-added to the map reassing it to select list
                    this._searchListLayerAddedHandle = on(this.map, "layer-add-result", lang.hitch(this, function (layerAdded, error) {
                        if (!error) {

                            var hasSameId = array.some(this.config.searchList, lang.hitch(this, function (config) {
                                var isId = typeof config.id !== "undefined";
                                return isId && layerAdded.layer.id === config.id;
                            }));

                            if (hasSameId) {
                                this._initSearchList();
                            }
                        }
                    }));
                    this.own(this._searchListLayerAddedHandle);
                }
            },

            /**
             * Init drawing capabilities and drawing events
             */
            _initDrawingTool: function () {
                var paramsDraw = {
                    map: this.map,
                    selectDrawing: true,
                    selectionMode: "new",
                    drawingMode: "one",
                    activateOnLoad: "point",
                    featureLayer: null
                };

                this.draw = new Draw(paramsDraw);
                this.own(this.draw);

                // When the drawing tool is ready
                this.draw.promiseReady.then(lang.hitch(this, function () {
                    // Activate the default drawing method
                    this._activateDefaultDrawingMode();
                }));

                this.own(on(this.draw, "selection-complete", lang.hitch(this, this._showMetrics), false));
                this.own(on(this.selectionModePoint, "change", lang.hitch(this, "_selectionModeChanged")));
                this.own(on(this.selectionModePolygon, "change", lang.hitch(this, "_selectionModeChanged")));
                this.own(on(this.selectionModeSelection, "change", lang.hitch(this, "_selectionModeChanged")));
            },

            _selectionModeChanged: function () {
                if (this.selectionModePoint.checked) {
                    this.selectionModeInstructions.innerHTML = this.i18n.widget.instructions_point;
                    this._activateDrawing(this.draw.GEOMETRY_TYPE_POINT);
                }
                else if (this.selectionModePolygon.checked) {
                    this.selectionModeInstructions.innerHTML = this.i18n.widget.instructions_polygon;
                    this._activateDrawing(this.draw.GEOMETRY_TYPE_POLYGON);
                }
                else if (this.selectionModeSelection.checked) {
                    this.selectionModeInstructions.innerHTML = this.i18n.widget.instructions_select;
                    this._activateSelection(this.draw.GEOMETRY_TYPE_POINT);
                }
            },

            /**
             * Change map visibility depending on options
             *
             */
            changeLayersVisibility: function () {
                var foundOptionData = this.settings.getOptions();
                for (var optionId in foundOptionData) {
                    if (foundOptionData.hasOwnProperty(optionId)) {
                        var option = foundOptionData[optionId];
                        var hasVisibility = typeof option.visibility !== "undefined";
                        if (hasVisibility) {

                            for (var layerId in option.visibility) {
                                if (option.visibility.hasOwnProperty(layerId)) {
                                    var mapLayer = this.map.getLayer(layerId);
                                    if (mapLayer) {
                                        var visibility = option.data[option.visibility[layerId].visibility];
                                        if (typeof visibility !== "undefined" && mapLayer.visible !== visibility) {
                                            mapLayer.setVisibility(visibility);
                                        }
                                        var visibleLayers = option.data[option.visibility[layerId].visible_layers];
                                        if (typeof visibleLayers !== "undefined" && mapLayer.visibleLayers !== visibleLayers) {
                                            mapLayer.setVisibleLayers(visibleLayers);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },

            addDynamicMapService: function (config, i18nAdditional) {
                var params = lang.clone(config);

                if (!i18nAdditional) {
                    i18nAdditional = {};
                }

                // variable substitution
                var options = this.settings.getOptions();

                params = JSON.stringify(params);
                params = string.substitute(params, { i18nAdditional: i18nAdditional, options: options, i18n: i18n });
                params = JSON.parse(params);

                var others = { map: this.map };
                lang.mixin(params, others);
                return layersUtils.addDynamicMapService(params);
            },

            addJoinLayer: function (config, i18nAdditional) {
                var params = lang.clone(config);

                if (!i18nAdditional) {
                    i18nAdditional = {};
                }

                // variable substitution
                var options = this.settings.getOptions();

                params = JSON.stringify(params);
                params = string.substitute(params, { i18nAdditional: i18nAdditional, options: options, i18n: i18n });
                params = JSON.parse(params);

                var others = { map: this.map };
                lang.mixin(params, others);
                return layersUtils.addJoinLayer(params);
            },

            addImageLayer: function (config, i18nAdditional) {
                var params = lang.clone(config);
                // variable substitution
                var options = this.settings.getOptions();

                if (!i18nAdditional) {
                    i18nAdditional = {};
                }

                if (params.renderingRule) {
                    params.renderingRule = this._substituteScopeElement(params.renderingRule, options);
                }

                params = JSON.stringify(params);
                params = string.substitute(params, { i18nAdditional: i18nAdditional, options: options, i18n: i18n });
                params = JSON.parse(params);

                var others = { map: this.map };
                lang.mixin(params, others);
                return layersUtils.addImageService(params);
            },

            addLayers: function () {
                var additionalLayers = this.config.additionalLayer;
                if (additionalLayers && additionalLayers.length > 0) {

                    // {Object.<number, Object.<string, Object>>}
                    // key : additionalLayer config index
                    // value : {i18n: i18n file || null }
                    // additionalLayersFiles: null

                    var additionalLayersFiles = this.configLoader.additionalLayersFiles;

                    var addLayer = lang.hitch(this, function (additionalLayer, indexConfig) {

                        var i18nAdditional = additionalLayersFiles[indexConfig].i18n;
                        if (!i18nAdditional) {
                            i18nAdditional = {};
                        }

                        var isJoin = additionalLayer.type && additionalLayer.type === "join";
                        var isImage = additionalLayer.type && additionalLayer.type === "image";
                        var isDynamicMap = additionalLayer.type && additionalLayer.type === "dynamic-map";

                        var promise;
                        if (isJoin) {
                            promise = this.addJoinLayer(additionalLayer, i18nAdditional);
                        } else if (isImage) {
                            promise = this.addImageLayer(additionalLayer, i18nAdditional);
                        } else if (isDynamicMap) {
                            promise = this.addDynamicMapService(additionalLayer, i18nAdditional);
                        } else {
                            promise = when(null);
                        }

                        return promise.then(lang.hitch(this, function (layer) {
                            if (layer) {
                                this.emit("layer-add", { layer: layer });
                            }
                        }));
                    });

                    var index = additionalLayers.length - 1;
                    // loop on each added layer, wait before each layer added to the map before adding the next one
                    var next = function () {
                        if (index >= 0) {
                            return addLayer(additionalLayers[index], index).always(function () {
                                index -= 1;
                                return next();
                            });
                        }
                    };
                    next();
                }
            },

            layerAdded: function (layer) {
                //jshint unused:false
            },

            /**
             * Change map time extent based on the option selection.
             *
             */
            _changeMapTimeExtent: function () {
                var foundOptionData = this.settings.getOptions();
                for (var optionId in foundOptionData) {
                    if (foundOptionData.hasOwnProperty(optionId)) {
                        var option = foundOptionData[optionId];
                        var hasTime = typeof option.time !== "undefined";
                        if (hasTime) {
                            var startArray = option.data[option.time.start];
                            var endArray = option.data[option.time.end];
                            var start = this.dateArrayToDate(startArray);
                            var end = this.dateArrayToDate(endArray);
                            var timeExtent = new TimeExtent(start, end);
                            this.map.setTimeExtent(timeExtent);
                        }
                    }
                }
            },

            /**
             * Activate drawing given a geometry type (point, polygon, etc)
             *
             * @param  {string} type The geometry drawing type
             */
            _activateDrawing: function (type) {
                this.draw.activateDraw(type);
                this.draw.selectLayer = false;
            },

            /**
             * Activate drawing and selection. Use the select layer specified in config for the selection.
             *
             * @param  {string} type The geometry drawing type
             */
            _activateSelection: function (type) {
                if (typeof this.config.selectLayer !== "undefined") {
                    this.draw.activateDraw(type);
                    this.draw.selectLayer = true;

                    var isId = typeof this.config.selectLayer.id !== "undefined";
                    var isUrl = typeof this.config.selectLayer.url !== "undefined";

                    var params = {};
                    if (this.config.selectLayer.options) {
                        params.options = this.config.selectLayer.options;
                        params.options.mode = FeatureLayer.MODE_SELECTION;
                    }

                    if (isUrl) {
                        var url = this.config.selectLayer.url;
                        var optionsData = this.settings.getOptions();
                        if (optionsData) {
                            url = this._substituteScopeElement(url, optionsData);
                        }
                        params.url = url;
                    } else if (isId) {
                        params.layerId = this.config.selectLayer.id;
                        params.map = this.map;
                    }

                    layersUtils.createFeatureLayer(params).then(lang.hitch(this, function (featureLayer) {
                        this.draw.set("featureLayer", featureLayer);
                        if (isId) {
                            // Listen layer re-added to the map reassing it to the drawing tool
                            this.own(on(this.map, "layer-add-result", lang.hitch(this, function (layerAdded, error) {
                                if (!error && layerAdded.layer.id === params.layerId) {
                                    layersUtils.createFeatureLayer(params).then(lang.hitch(this, function (featureLayer) {
                                        this.draw.set("featureLayer", featureLayer);
                                    }));
                                }
                            })));
                        }
                    }));
                }
                else {
                    this.draw.desactivateDraw();
                }
            },

            /**
             * Search the given value within a rasterattributetable result object
             * and return the attribute value if a correspondence if found.
             *
             * @param  {Object} attributeTable
             * @param  {string} attributeName
             * @param  {any} value
             */
            searchRasterAttributeTable: function (attributeTable, attributeName, value) {
                var replaced = value;
                // Check if input is valid
                var isInputValid = attributeTable && attributeName && attributeTable.fields && attributeTable.features;
                if (isInputValid) {
                    // Check if the given attribute name is within the attribute table
                    var isFieldFound = array.some(attributeTable.fields, function (field) {
                        return field.name === attributeName;
                    });
                    if (isFieldFound) {
                        // Search the given value in the 'Value' attribute
                        array.some(attributeTable.features, function (feature) {
                            var isValueFound = false;
                            // Check if the feature has the given attribute and the 'Value' attribute
                            var hasAttribute = feature.attributes &&
                                typeof feature.attributes.Value !== "undefined" &&
                                typeof feature.attributes[attributeName] !== "undefined";
                            if (hasAttribute) {
                                isValueFound = feature.attributes.Value === value;
                                // Get the searched attribute value if found
                                if (isValueFound) {
                                    replaced = feature.attributes[attributeName];
                                }
                            }
                            return isValueFound;
                        });
                    }
                }
                return replaced;
            },

            /**
             * String ends with
             *
             * @param  {string} content The string to test
             * @param  {string} end The string to test with
             * @param  {boolean} ignoreCase Ignore case
             * @return {boolean} if 'str' ends with 'end'
             */
            _endsWith: function (content, end, ignoreCase) {
                if (ignoreCase) {
                    content = content.toLowerCase();
                    end = end.toLowerCase();
                }
                if ((content.length - end.length) < 0) {
                    return false; // boolean
                }
                return content.lastIndexOf(end) === content.length - end.length;
            },

            /**
             * String start with
             *
             * @param  {string} content The string to test
             * @param  {string} start The string to test with
             * @param  {boolean} ignoreCase Ignore case
             * @return {boolean} if 'str' starts with 'start'
             */
            _startsWith: function (content, start, ignoreCase) {
                if (ignoreCase) {
                    content = content.toLowerCase();
                    start = start.toLowerCase();
                }
                return content.indexOf(start) === 0;
            },


            /**
             * Read a string before a character last occurence
             *
             * @param  {string} content The string to read
             * @param  {string} before The substring to read before
             * @return {string} The string before the specified character
             */
            _readBefore: function (content, before) {
                return content.substring(0, content.lastIndexOf(before));
            },

            /**
             * Read a string before a character last occurence
             *
             * @param  {string} content The string to read
             * @param  {string} after The substring to read after
             * @return {string} The string after the specified character
             */
            _readAfter: function (content, after) {
                return content.substring(content.lastIndexOf(after) + 1);
            },

            /**
             * Remove unneccessary spaces within a given string
             *
             * @param  {string} content
             */
            _leaveOneSpace: function (content) {
                //remove leading spaces
                content = content.replace(/^\s+/g, '');
                //remove trailing spaces
                content = content.replace(/\s+$/g, '');
                //remove whitespaces in the middle
                content = content.replace(/\s+/g, ' ');
                return content;
            },

            /**
             * Execute a regular expression
             *
             * @param  {string} expression
             * @param  {any} test
             * @return {boolean} if the regexp was found or not.
             */
            _executeRegExp: function (expression, test) {
                var result = expression.exec(test);
                if (result !== null) {
                    if (result.index === expression.lastIndex) {
                        expression.lastIndex++;
                    }
                }

                var found = (result !== null) && result.length > 0;
                return found;
            },

            /**
             * Get the ArcGIS Server REST service type given a service URL.
             * ImageService, FeatureService, MapService or WMS
             *
             * @param  {string} url
             * @return {string} service type
             */
            _getLayerType: function (url) {
                var exp = /(ImageServer\/?)$/i;
                var isImageService = this._executeRegExp(exp, url);
                if (isImageService) {
                    return this.layerServiceType.IMAGE_SERVICE;
                }

                exp = /FeatureServer((\/)(\d+)(\/)?)$/i;
                var isFeatureServer = this._executeRegExp(exp, url);
                if (isFeatureServer) {
                    return this.layerServiceType.FEATURE_SERVICE;
                }

                exp = /MapServer((\/)(\d+)(\/)?)$/i;
                var isMapServer = this._executeRegExp(exp, url);
                if (isMapServer) {
                    return this.layerServiceType.MAP_SERVICE;
                }

                exp = /(request=GetCapabilities\/?)$/i;
                var isWMS = this._executeRegExp(exp, url);
                if (isWMS) {
                    return this.layerServiceType.WMS_SERVICE;
                }
                return;
            },

            /**
             * Add units in the calculation result value as specified in config
             *
             * @param  {Object} calculationResult
             * @param  {Object} config
             * @param  {Object} scope
             */
            _addUnits: function (calculationResult, config, scope) {
                if (!config.units) {
                    return;
                }
                for (var configUnitName in config.units) {
                    var result = calculationResult[configUnitName];
                    var isDefined = config.units.hasOwnProperty(configUnitName) && typeof result !== "undefined";
                    if (isDefined) {
                        var formatted = result;
                        if (typeof result === "number") {
                            formatted = numberUtils.format(result, {});
                        }
                        if (formatted !== null) {
                            var configUnitValue = config.units[configUnitName];
                            this._addUnit(calculationResult, configUnitName, configUnitValue, scope);
                        }
                    }
                }
            },

            /**
             * Add a unit in the calculation result value as specified in config
             *
             * @param  {Object} calculationResult
             * @param  {string} configUnitName
             * @param  {string|Object} configUnitValue The unit config element
             * @param  {Object} scope
             */
            _addUnit: function (calculationResult, configUnitName, configUnitValue, scope) {
                var unitName;
                if (typeof config === "string") {
                    unitName = configUnitValue;
                } else {
                    unitName = configUnitValue.unit;
                }
                unitName = this._substituteScopeElement(unitName, scope.input);
                var foundUnit = this._findUnit(unitName);

                if (foundUnit) {
                    if (typeof config !== "string" && configUnitValue.fullname) {

                        var value = calculationResult[configUnitName];
                        var valueReplaced = value;
                        if (typeof value === "string") {
                            valueReplaced = value.replace(",", "");
                        }
                        if (valueReplaced > 1) {
                            calculationResult[configUnitName] = calculationResult[configUnitName] + " " + (foundUnit.plurial || foundUnit.abbr);
                        }
                        else {
                            calculationResult[configUnitName] = calculationResult[configUnitName] + " " + (foundUnit.singular || foundUnit.abbr);
                        }
                    }
                    else {
                        calculationResult[configUnitName] = calculationResult[configUnitName] + " " + foundUnit.abbr;
                    }
                }
            },

            /**
             * Find the unit name in the unit section of the internationalization file
             *
             * @param  {string} unitName The unit name
             * @return {Object} The unit internationalization file object
             */
            _findUnit: function (unitName) {
                var foundUnit;
                for (var unit in i18n.units) {
                    if (i18n.units.hasOwnProperty(unit)) {
                        var found = (unit === unitName);
                        if (found) {
                            var unitItem = i18n.units[unit];
                            foundUnit = unitItem;
                            break;
                        }
                    }
                }
                return foundUnit;
            },

            /**
             * Check if the element specified in the config file is a string.
             * If it's a string, try to replace the element by the corresponding given substitute object value.
             *
             * @param {string|Object} element Value in config file.
             * @param {Object} substitution The substitution object
             * @return {Any} The value that may have been substitued
             */
            _substituteScopeElement: function (element, substitution) {
                var isSubstitution = typeof element === "string" && typeof substitution[element] !== "undefined";
                if (isSubstitution) {
                    return substitution[element];
                }
                else {
                    return element;
                }
            },

            /**
             * Add a timeExtent property to the given parameter if time is specified in config.
             *
             * @param  {Object} parameter The parameter that may have the timeExtent property.
             * @param  {Object} layerConfig Layer configuration section
             * @param  {Object} scope Calculation scope
             */
            _addTimeExtentParameter: function (parameter, layerConfig, scope) {
                var timeConfig = layerConfig.time;
                // Time Extent
                if (typeof timeConfig !== "undefined") {
                    var startArray = this._substituteScopeElement(timeConfig.start, scope.input);
                    var start = this.dateArrayToDate(startArray);

                    var endArray = this._substituteScopeElement(timeConfig.end, scope.input);
                    var end = this.dateArrayToDate(endArray);

                    var timeExtent = new TimeExtent(start, end);
                    parameter.timeExtent = timeExtent;
                }
            },

            /**
             * Execute an Image Service Identify task request
             * If specified in the config file, use a mosaic rule and a time extent parameters.
             *
             * @param  {Object} layerConfig Layer configuration section
             * @param  {Geometry} geometry Identify geometry
             * @param  {Object} scope Calculation scope
             * @return {external:Promise} The task promise
             */
            _identifyImageService: function (layerConfig, geometry, scope) {
                var url = this._substituteScopeElement(layerConfig.url, scope.input);

                var parameter = new ImageServiceIdentifyParameters();
                parameter.geometry = geometry;

                // Add a timeExtent property to the "parameter" if time is specified in config.
                this._addTimeExtentParameter(parameter, layerConfig, scope);

                // Mosaic Rule
                if (typeof layerConfig.mosaicRule !== "undefined") {
                    var mosaicRuleJson = lang.clone(layerConfig.mosaicRule);
                    if (typeof mosaicRuleJson.where !== "undefined") {
                        mosaicRuleJson.where = this._substituteScopeElement(mosaicRuleJson.where, scope.input);
                    }
                    var mosaicRule = new MosaicRule(mosaicRuleJson);
                    parameter.mosaicRule = mosaicRule;
                }

                if (layerConfig.renderingRule) {
                    var renderingRule = lang.clone(layerConfig.renderingRule);

                    var options = this.settings.getOptions();
                    if (options) {
                        renderingRule = this._substituteScopeElement(renderingRule, options);
                    }

                    renderingRule = JSON.stringify(renderingRule);
                    renderingRule = string.substitute(renderingRule, { options: scope.input });
                    renderingRule = JSON.parse(renderingRule);

                    parameter.renderingRule = new RasterFunction(renderingRule);
                }

                var task = new ImageServiceIdentifyTask(url);
                var promise = task.execute(parameter);
                return promise.then(lang.hitch(this, function (result) {
                    return this.project(result.location, geometry.spatialReference).then(function (projected) {
                        result.location = projected;
                        return result;
                    });
                }));
            },

            projectArray: function (geometries, outSpatialReference) {
                var canProject = array.every(geometries, function (geometry) {
                    return webMercatorUtils.canProject(geometry, outSpatialReference);
                });

                canProject = false;// test

                var projeted = [];
                if (canProject) {
                    array.forEach(geometries, function (geometry) {
                        projeted.push(webMercatorUtils.project(geometry, outSpatialReference));
                    });
                }
                else {
                    projeted = this.projectService(geometries, outSpatialReference);
                }
                return when(projeted);
            },

            projectGeometry: function (geometry, outSpatialReference) {
                var canProject = webMercatorUtils.canProject(geometry, outSpatialReference);
                canProject = false;// test
                var projeted;
                if (canProject) {
                    projeted = webMercatorUtils.project(geometry, outSpatialReference);
                }
                else {
                    projeted = this.projectService(geometry, outSpatialReference).then(function (projeted) {
                        return projeted[0];
                    });
                }
                return when(projeted);
            },

            project: function (geometry, outSpatialReference) {
                var isArray = geometry instanceof Array;
                if (isArray) {
                    return this.projectArray(geometry, outSpatialReference);
                }
                else {
                    return this.projectGeometry(geometry, outSpatialReference);
                }
            },

            projectService: function (geometry, outSpatialReference) {
                var params = new ProjectParameters();

                var isArray = geometry instanceof Array;
                if (isArray) {
                    params.geometries = geometry;
                }
                else {
                    params.geometries = [geometry];
                }

                params.outSR = outSpatialReference;
                //var geometryService = new GeometryService(esriConfig.defaults.geometryService.url);

                return this.geometryService.project(params);
            },

            /**
             * Find the objectid field within query result fields
             *
             * @param  {any} queryResult
             * @return {Object} The objectid field object found within the queryResult
             */
            _findOIDField: function (queryResult) {
                var foundField;
                array.some(queryResult.fields, lang.hitch(this, function (field) {
                    var isOID = field.type = this.ESRI_FIELD_TYPE_OID;

                    if (isOID) {
                        foundField = field;
                    }

                    return isOID;
                }));
                return foundField;
            },

            /**
             * Execute a relationship query task request
             *
             * @param  {Object} layerConfig
             * @param  {Object} queryResult
             * @param  {Object} calculation scope
             * @return {external:Promise} The task promise
             */
            _queryRelation: function (layerConfig, queryResult, scope) {
                var promises = [];

                var url = this._substituteScopeElement(layerConfig.url, scope.input);

                if (typeof layerConfig.relate !== "undefined") {
                    array.forEach(layerConfig.relate, lang.hitch(this, function (relationConfig) {
                        if (queryResult.fields &&
                            queryResult.features &&
                            queryResult.fields.length > 0 &&
                            queryResult.features.length > 0) {
                            var oidField = this._findOIDField(queryResult);
                            var objectIds = [];
                            // An OID field is found
                            if (typeof oidField !== "undefined") {
                                array.forEach(queryResult.features, function (feature) {
                                    if (feature.attributes &&
                                        typeof feature.attributes[oidField.name] !== "undefined") {
                                        objectIds.push(feature.attributes[oidField.name]);
                                    }
                                });

                                if (objectIds.length > 0) {
                                    var relationship = new RelationshipQuery();
                                    relationship.objectIds = objectIds;
                                    relationship.relationshipId = this._substituteScopeElement(relationConfig.relationshipId, scope.input);

                                    if (relationConfig.definitionExpression) {
                                        relationship.definitionExpression = this._substituteScopeElement(relationConfig.definitionExpression, scope.input);
                                    }

                                    // outFields
                                    if (relationConfig.outFields) {
                                        relationship.outFields = relationConfig.outFields;
                                    }
                                    else {
                                        relationship.outFields = ["*"];
                                    }

                                    // returnGeometry
                                    if (relationConfig.returnGeometry) {
                                        relationship.returnGeometry = relationConfig.returnGeometry;
                                    }
                                    else {
                                        relationship.returnGeometry = false;
                                    }

                                    var task = new QueryTask(url);

                                    var promise = task.executeRelationshipQuery(relationship);
                                    promise = promise.then(function (relationFeatureSet) {
                                        var result = {};
                                        result.relationFeatureSet = relationFeatureSet;
                                        result.relationConfig = relationConfig;
                                        return result;
                                    });
                                    promises.push(promise);
                                }
                            }
                        }
                    }));
                }
                return promises;
            },

            /**
             * Execute a query task request
             * If specified in the config file, use a spatial relationship and a time extent parameters.
             *
             * @param  {Object} layerConfig
             * @param  {external:Geometry} geometry
             * @param  {Object} scope Calculation result scope
             * @return {external:Promise} The task promise
             */
            _queryMapService: function (layerConfig, geometry, scope) {
                var parameter = new EsriQuery();
                parameter.geometry = geometry;
                parameter.outFields = ["*"];
                parameter.returnGeometry = true;
                parameter.outSpatialReference = this.map.spatialReference;

                if (typeof layerConfig.options !== "undefined") {
                    lang.mixin(parameter, layerConfig.options);
                }

                // Spatial Relation
                if (typeof layerConfig.spatialRelationship !== "undefined") {
                    parameter.spatialRelationship = EsriQuery[layerConfig.spatialRelationship];
                }

                // Add a timeExtent property to the "parameter" if time is specified in config.
                this._addTimeExtentParameter(parameter, layerConfig, scope);

                var url = this._substituteScopeElement(layerConfig.url, scope.input);

                var task = new QueryTask(url);
                var result = task.execute(parameter);
                return result.promise;
            },

            /**
             * Convert an array of numbers in a date object
             *
             * @param  {Array.<number>} dateArray
             * @return {date} The converted date
             */
            dateArrayToDate: function (dateArray) {
                var date;
                if (dateArray.length === 1) {
                    date = new Date(dateArray[0]);
                }
                else if (dateArray.length === 2) {
                    // -1 because months are 0 index based
                    date = new Date(dateArray[0], dateArray[1] - 1);
                }
                else if (dateArray.length === 3) {
                    date = new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
                }
                else if (dateArray.length === 4) {
                    date = new Date(dateArray[0], dateArray[1] - 1, dateArray[2], dateArray[3]);
                }
                else if (dateArray.length === 5) {
                    date = new Date(dateArray[0], dateArray[1] - 1, dateArray[2], dateArray[3], dateArray[4]);
                }
                else if (dateArray.length === 6) {
                    date = new Date(dateArray[0], dateArray[1] - 1, dateArray[2], dateArray[3], dateArray[4], dateArray[5]);
                }
                else if (dateArray.length === 7) {
                    date = new Date(dateArray[0], dateArray[1] - 1, dateArray[2], dateArray[3], dateArray[4], dateArray[5], dateArray[6]);
                }
                return date;
            },

            /**
             * Execute a Image Service Raster attribute table request
             *
             * @param  {Object} layerConfig
             * @param  {Object} scope
             * @return {external:Promise} The task promise
             */
            _queryRasterAttributeTable: function (layerConfig, scope) {
                var url = this._substituteScopeElement(layerConfig.url, scope.input);
                var endsWithSlash = this._endsWith(url, "/", true);
                if (!endsWithSlash) {
                    url = url + "/";
                }

                var data = {
                    f: "pjson"
                };

                var proxyUrl = this._getProxyUrl();

//              url = proxyUrl + url + this.tasks.IMAGE_SERVICE_ATTRIBUTE_TABLE;
                if (proxyUrl) {
                    url = proxyUrl + "?" + url + this.tasks.IMAGE_SERVICE_ATTRIBUTE_TABLE;
                }
                return xhr(url, {
                    method: "GET",
                    data: data,
                    timeout: 60000,
                    handleAs: "json"
                });
            },

            /**
             * Execute a Image Service Raster Histogram request
             *
             * @param  {Object} layerConfig
             * @param  {external:Geometry} geometry A geometry within which the histogram is computed.
             * @param  {Object} scope Calculation scope
             * @return {external:Promise} The request promise
             */
            _queryImageHistogram: function (layerConfig, geometry, scope) {
                var url = this._substituteScopeElement(layerConfig.url, scope.input);
                var endsWithSlash = this._endsWith(url, "/", true);
                if (!endsWithSlash) {
                    url = url + "/";
                }

                var data = {
                    geometryType: "esriGeometryPolygon",
                    geometry: JSON.stringify(geometry.toJson()),
                    f: "pjson"
                };

                var mosaicRuleJson = this._mosaicRuleJson(layerConfig, scope);
                if (typeof mosaicRuleJson !== "undefined") {
                    data.mosaicRule = mosaicRuleJson;
                }

                if (layerConfig.renderingRule) {
                    var renderingRule = lang.clone(layerConfig.renderingRule);

                    var options = this.settings.getOptions();
                    if (options) {
                        renderingRule = this._substituteScopeElement(renderingRule, options);
                    }


                    renderingRule = JSON.stringify(renderingRule);
                    renderingRule = string.substitute(renderingRule, { options: scope.input });
                    renderingRule = JSON.parse(renderingRule);

                    data.renderingRule = JSON.stringify(new RasterFunction(renderingRule).toJson());
                }

                if (layerConfig.pixelSize) {
                    data.pixelSize = JSON.stringify(layerConfig.pixelSize);
                }

                var proxyUrl = this._getProxyUrl();

//              url = proxyUrl + url + this.tasks.IMAGE_SERVICE_HISTOGRAM;
                if (proxyUrl) {
                    url = proxyUrl + "?" + url + this.tasks.IMAGE_SERVICE_HISTOGRAM;
                }
                return xhr(url, {
                    method: "POST",
                    data: data,
                    timeout: 60000,
                    handleAs: "json"
                });
            },

            _mosaicRuleJson: function (layerConfig, scope) {
                var mosaicRuleJson;
                if (typeof layerConfig.mosaicRule !== "undefined") {
                    var mosaicRule = lang.clone(layerConfig.mosaicRule);
                    if (typeof mosaicRule.where !== "undefined") {
                        mosaicRule.where = this._substituteScopeElement(mosaicRule.where, scope.input);
                    }
                    mosaicRuleJson = JSON.stringify(mosaicRule);
                }
                return mosaicRuleJson;
            },

            /**
             * Execute a Image Service Get Samples request
             * If specified in the config file, use a mosaic rule parameter
             *
             * @param  {Object} layerConfig
             * @param  {external:Geometry} geometry
             * @param  {Object} scope Calculation scope
             * @return {external:Promise} The request promise
             */
            _queryGetSamples: function (layerConfig, geometry, scope) {
                var url = this._substituteScopeElement(layerConfig.url, scope.input);
                var endsWithSlash = this._endsWith(url, "/", true);
                if (!endsWithSlash) {
                    url = url + "/";
                }

                var data = {
                    geometryType: "esriGeometryPolygon",
                    geometry: JSON.stringify(geometry.toJson()),
                    f: "json"
                };

                var mosaicRuleJson = this._mosaicRuleJson(layerConfig, scope);
                if (typeof mosaicRuleJson !== "undefined") {
                    data.mosaicRule = mosaicRuleJson;
                }

                if (typeof layerConfig.sampleCount !== "undefined") {
                    data.sampleCount = layerConfig.sampleCount;
                }

                var proxyUrl = this._getProxyUrl();

//              url = proxyUrl + url + this.tasks.GET_SAMPLES;
                if (proxyUrl) {
                    url = proxyUrl + "?" + url + this.tasks.GET_SAMPLES;
                }
                return xhr(url, {
                    method: "POST",
                    data: data,
                    timeout: 60000,
                    handleAs: "json"
                });
            },

            /**
             * If specified, get the default proxy url
             *
             * @return {string} The default proxy url defined in esriConfig
             */
            _getProxyUrl: function () {
                var proxyUrl = "";
                var isDefaultSpecified = esriConfig.defaults.io.alwaysUseProxy && esriConfig.defaults.io && esriConfig.defaults.io.proxyUrl;
                if (isDefaultSpecified) {
                    proxyUrl = esriConfig.defaults.io.proxyUrl;
                }
                return proxyUrl;
            },


            /**
             * Center the map at the specified geometry
             *
             * @param  {external:Geometry} geometry The geometry to center at
             */
            centerAt: function (geometry) {
                var isPolygon = geometry.type === "polygon";
                var isExtent = geometry.type === "extent";
                var isPoint = geometry.type === "point";

                if (isPolygon || isExtent) {
                    this.map.centerAt(geometry.getCentroid());
                } else if (isPoint) {
                    this.map.centerAt(geometry);
                }
            },

            /**
             * Create samples coordinates with a geometry (polygon or extent)
             *
             * @param  {number} numberOfPoints The maximum number of points inside the polygon or the extent.
             * @param  {external:Geometry} geometry The geometry
             * @return {Array.<external:Geometry>} Array of sample points
             */
            _createSamplePoints: function (numberOfPoints, geometry) {
                var isPolygon = geometry.type === "polygon";
                var isExtent = geometry.type === "extent";

                var extent;
                if (isPolygon) {
                    extent = geometry.getExtent();
                }

                if (isExtent) {
                    extent = geometry;
                }

                var width = extent.getWidth();
                var heigth = extent.getHeight();

                var pointsInGeometry = [];
                if (extent) {
                    var numberPointsOneLine = parseInt(Math.sqrt(numberOfPoints));

                    var distanceBetweenPointsWidth = width / numberPointsOneLine;
                    var distanceBetweenPointsHeight = heigth / numberPointsOneLine;

                    var smallestDistance;
                    if (distanceBetweenPointsWidth <= distanceBetweenPointsHeight) {
                        smallestDistance = distanceBetweenPointsWidth;
                    }
                    else {
                        smallestDistance = distanceBetweenPointsHeight;
                    }

                    var firstLinePoint = new Point(extent.xmin, extent.ymax, this.map.spatialReference);
                    var lastPoint = firstLinePoint;

                    do {
                        var intersects = geometryEngine.intersects(geometry, lastPoint);
                        if (intersects) {
                            pointsInGeometry.push(lastPoint);
                        }

                        var newX = lastPoint.x + smallestDistance;
                        if (newX > extent.xmax) {
                            firstLinePoint = new Point(firstLinePoint.x, firstLinePoint.y - smallestDistance, this.map.spatialReference);
                            lastPoint = firstLinePoint;
                        }
                        else {
                            lastPoint = new Point(newX, lastPoint.y, this.map.spatialReference);
                        }
                    }
                    while (lastPoint.x <= extent.xmax && lastPoint.y >= extent.ymin);
                }


                return pointsInGeometry;
            },

            /**
             * Transform points coordinates in ij coordinates in relation with a
             * reference geometry (polygon, extent or point).
             *
             * @param  {Array.<external:Point> points Points to transform in ij coordinates
             * @param  {external:Geometry} geometry The reference geometry for the ij position
             * @param {Array} I,J coordinates
             */
            _pointsToWMSij: function (points, geometry) {
                var isPolygon = geometry.type === "polygon";
                var isExtent = geometry.type === "extent";
                var isPoint = geometry.type === "point";

                var pointsInGeometry = [];
                if (isPolygon || isExtent) {
                    var extent;
                    if (isPolygon) {
                        extent = geometry.getExtent();
                    }
                    else if (isExtent) {
                        extent = geometry;
                    }
                    var width = extent.getWidth();
                    var heigth = extent.getHeight();
                    array.forEach(points, lang.hitch(this, function (point) {
                        var positionX = this._computeLinearPosition(0, width, extent.xmin, extent.xmax, point.x);
                        var positionY = this._computeLinearPosition(0, heigth, extent.ymax, extent.ymin, point.y);
                        pointsInGeometry.push({
                            i: positionX,
                            j: positionY
                        });
                    }));
                }
                else if (isPoint && points.length === 1) {
                    pointsInGeometry.push({
                        i: 0,
                        j: 0
                    });
                }
                return pointsInGeometry;
            },

            /**
             * Rescale a value with new minimum and maximum values.
             *
             * @param  {number} minExpected
             * @param  {number} maxExpected
             * @param  {number} minCurrent
             * @param  {number} maxCurrent
             * @param  {number} currentValue
             */
            _computeLinearPosition: function (minExpected, maxExpected, minCurrent, maxCurrent, currentValue) {
                var numerator = (maxExpected - minExpected) * (currentValue - minCurrent);
                var denominator = maxCurrent - minCurrent;
                var computed = (numerator / denominator) + minExpected;
                return computed;
            },

            /**
             * Execute a GetFeatureInfo WMS request
             *
             * @param  {Object} layerConfig
             * @param  {external:Geometry} geometry
             * @param  {Object} scope Calculation scope
             * @return {external:Promise} The request promise
             */
            _queryWMSGetFeatureInfo: function (layerConfig, geometry, scope) {
                var url = this._substituteScopeElement(layerConfig.url, scope.input);
                url = this._readBefore(url, "?");

                var proxyUrl = this._getProxyUrl();

//              url = proxyUrl + url + "?";
                if (proxyUrl) {
                    url = proxyUrl + "?" + url + "?";
                }

                return this._createWMSBBox(geometry, layerConfig.srs).then(lang.hitch(this, function (bbox) {
                    if (bbox) {
                        var widthHeight = this._getWidthHeight(geometry);

                        var promiseSamples = this._getSamplesPoints(layerConfig, geometry, scope);
                        return promiseSamples.then(lang.hitch(this, function (points) {
                            var wmsIJ = this._pointsToWMSij(points, geometry);
                            var promises = [];
                            array.forEach(wmsIJ, lang.hitch(this, function (samplePoint) {
                                if (!isNaN(samplePoint.i) && !isNaN(samplePoint.j)) {
                                    var data = {
                                        SERVICE: "WMS",
                                        VERSION: layerConfig.version || "1.1.1",
                                        REQUEST: "GetFeatureInfo",
                                        BBOX: bbox,
                                        SRS: "EPSG:" + layerConfig.srs,
                                        WIDTH: widthHeight.w,
                                        HEIGHT: widthHeight.h,
                                        LAYERS: layerConfig.layer,
                                        STYLES: layerConfig.styles,
                                        INFO_FORMAT: "text/xml",
                                        I: samplePoint.i,
                                        J: samplePoint.j,
                                        FEATURE_COUNT: 10
                                    };
                                    promises.push(xhr(url, {
                                        method: "GET",
                                        query: data,
                                        timeout: 60000,
                                        handleAs: "xml"
                                    }));
                                }
                            }));

                            return all(promises).then(lang.hitch(this, function (results) {
                                var features = [];
                                array.forEach(results, lang.hitch(this, this._parseWmsFeatureInfoXml, features, layerConfig));
                                return this._projectBackWMS(features).then(lang.hitch(this, function (projectResult) {
                                    return {
                                        type: this.tasks.WMS_FEATURE_INFO,
                                        wmsFeatureInfoResult: projectResult,
                                        config: layerConfig
                                    };
                                }));
                            }));
                        }));
                    }
                }));
            },

            _projectBackWMS: function (features) {
                var queryResults = [];
                var promises = [];
                array.forEach(features, lang.hitch(this, function (feature) {
                    var result = {};
                    lang.mixin(result, feature);
                    // Add geometry
                    var hasCoordinates = typeof feature.coordinates !== "undefined";
                    var hasSRS = typeof feature.srs !== "undefined";
                    if (hasCoordinates && hasSRS) {
                        var coordinates = feature.coordinates.split(",");
                        if (coordinates.length === 4) {
                            var xMin = parseFloat(coordinates[0]);
                            var yMin = parseFloat(coordinates[1]);
                            var spatialReference = new SpatialReference(feature.srs);
                            var geometry = new Point(xMin, yMin, spatialReference, spatialReference);
                            var promise = this.project(geometry, this.map.spatialReference).then(function (projeted) {
                                return {
                                    result: result,
                                    projeted: projeted
                                };
                            });
                            promises.push(promise);
                        }
                    }
                }));
                return all(promises).then(function (items) {
                    array.forEach(items, function (item) {
                        var geometries = {
                            geometry: item.projeted
                        };
                        lang.mixin(item.result, geometries);
                        queryResults.push(item.result);
                    });
                    return queryResults;
                });
            },

            /**
             * Get the geometry width and height. If the geometry is a point, width and height is set to 1.
             *
             * @param  {external:Geometry} geometry
             * @return {Object} Geometry width and height
             */
            _getWidthHeight: function (geometry) {
                var extent;

                var isPolygon = geometry.type === "polygon";
                var isExtent = geometry.type === "extent";

                var width = 1;
                var heigth = 1;

                if (isPolygon) {
                    extent = geometry.getExtent();
                }
                if (isExtent) {
                    extent = geometry;
                }

                if (isPolygon || isExtent) {
                    width = extent.getWidth();
                    heigth = extent.getHeight();
                }

                return {
                    w: width,
                    h: heigth
                };
            },

            /**
             * Transform a geometry to a WMS GetFeatureInfo BBox
             *
             * @param  {external:Geometry} geometry
             * @param  {number} wmsWkid
             * @return {string} Comma-separated bounding box coordinates
             */
            _createWMSBBox: function (geometry, wmsWkid) {
                var isPolygon = geometry.type === "polygon";
                var isExtent = geometry.type === "extent";
                var isPoint = geometry.type === "point";

                var spatialReferenceWMS = new SpatialReference(wmsWkid);

                var extent;
                if (isPolygon) {
                    extent = geometry.getExtent();
                }
                if (isExtent) {
                    extent = geometry;
                }

                var topLeft;
                var bottomLeft;
                var topRight;
                var bottomRight;

                var width = 1;
                var heigth = 1;

                if (isPolygon || isExtent) {
                    width = extent.getWidth();
                    heigth = extent.getHeight();

                    topLeft = new Point(extent.xmin, extent.ymax, this.map.spatialReference);
                    bottomLeft = new Point(extent.xmin, extent.ymin, this.map.spatialReference);
                    topRight = new Point(extent.xmax, extent.ymax, this.map.spatialReference);
                    bottomRight = new Point(extent.xmax, extent.ymin, this.map.spatialReference);
                }
                else if (isPoint) {
                    topLeft = geometry;
                    bottomLeft = geometry;
                    topRight = geometry;
                    bottomRight = geometry;
                }

                var cornersPoints = [topLeft, bottomLeft, topRight, bottomRight];
                var promises = [];
                array.forEach(cornersPoints, lang.hitch(this, function (point) {
                    var pointWMS = this.project(point, spatialReferenceWMS).then(function (projected) {
                        return projected;
                    });
                    promises.push(pointWMS);
                }));
                return all(promises).then(function (cornersPointsProjected) {
                    var bbox;
                    if (cornersPointsProjected.length === 4) {
                        bbox = cornersPointsProjected[0].x + "," +
                            cornersPointsProjected[1].y + "," +
                            cornersPointsProjected[2].x + "," +
                            cornersPointsProjected[0].y;
                    }
                    return bbox;
                });
            },

            /**
             * Add WMS feature infos in the feature array given a WMS GetFeatureInfo XMLDocument
             *
             * @param  {Array} features
             * @param  {Object} layerConfig
             * @param  {external:XMLDocument} xml
             */
            _parseWmsFeatureInfoXml: function (features, layerConfig, xml) {
                var rootNode = xml.documentElement;
                array.forEach(rootNode.childNodes, lang.hitch(this, function (layerNode) {
                    var isLayer = layerNode.tagName === layerConfig.layer + "_layer";
                    if (isLayer) {
                        array.forEach(layerNode.childNodes, lang.hitch(this, function (featureNode) {
                            var isFeature = featureNode.tagName === layerConfig.layer + "_feature";
                            if (isFeature) {
                                var feature = {};

                                array.forEach(layerConfig.attributes, lang.hitch(this, function (attribute) {
                                    array.some(featureNode.childNodes, lang.hitch(this, function (attributeNode) {
                                        var isAttributeNode = attributeNode.tagName === attribute;
                                        if (isAttributeNode) {
                                            feature[attribute] = attributeNode.textContent;
                                        }
                                        return isAttributeNode;
                                    }));
                                }));

                                array.forEach(featureNode.childNodes, lang.hitch(this, function (attributeNode) {
                                    var isGmlBoundedBy = attributeNode.tagName === "gml:boundedBy";
                                    if (isGmlBoundedBy) {
                                        if (attributeNode.childNodes.length > 0) {
                                            array.some(attributeNode.childNodes, lang.hitch(this, function (gmlBox) {
                                                var isGmlBox = gmlBox.tagName === "gml:Box";
                                                if (isGmlBox) {
                                                    var hasSrsName = gmlBox.hasAttribute("srsName");
                                                    if (hasSrsName) {
                                                        var srsName = gmlBox.getAttribute("srsName");
                                                        var srs = this._readAfter(srsName, ":");
                                                        feature.srs = parseInt(srs);
                                                    }
                                                    array.some(gmlBox.childNodes, lang.hitch(this, function (gmlCoordinates) {
                                                        var isGmlCoordinates = gmlCoordinates.tagName === "gml:coordinates";
                                                        if (isGmlCoordinates) {
                                                            feature.coordinates = gmlCoordinates.textContent;
                                                        }
                                                        return isGmlCoordinates;
                                                    }));
                                                }
                                                return isGmlBox;
                                            }));
                                        }
                                    }
                                }));
                                features.push(feature);
                            }
                        }));
                    }
                }));
            },

            /**
             * Create a dojo/store query for the relates csv data used for the csv interpolation.
             * Check is a date is between from and to dates.
             *
             * @param  {string} day Day attribute name
             * @param  {string} month Month attribute name
             * @param  {string} year Year attribute name
             * @param  {string} dateFrom Minimum date in MM/DD/YYYY format
             * @param  {string} dateTo Maximum date in MM/DD/YYYY format
             * @param  {Object} object to check having a day, month and year properties
             * @return {boolean} if specified object date is between from and to date inclusively
             */
            _storeQueryBetweenTwoDates: function (day, month, year, dateFrom, dateTo, object) {
                var dayCheck = object[day];
                var monthCheck = object[month] - 1; // -1 because months are from 0 to 11
                var yearCheck = object[year];

                if (typeof dayCheck !== "undefined" &&
                    typeof monthCheck !== "undefined" &&
                    typeof yearCheck !== "undefined") {

                    var d1 = dateFrom.split("/");
                    var d2 = dateTo.split("/");

                    //new Date(year,month,date[,hour,minute,second,millisecond ])

                    var fromDate = new Date(d1[2], d1[0] - 1, d1[1]);  // -1 because months are from 0 to 11
                    var toDate = new Date(d2[2], d2[0] - 1, d2[1]); // -1 because months are from 0 to 11

                    var dateCheck = new Date(yearCheck, monthCheck, dayCheck);
                    var isBetween = dateCheck >= fromDate && dateCheck <= toDate;
                    return isBetween;
                } else {
                    return false;
                }
            },

            /**
             * Create the samples points used for the csv interpolation.
             * If the geometry type is extent or polygon, samples points are centroid if sampleCount in config <= 1.
             * If sampleCount > 1, samples points created within the geometry.
             * Is there is a Image Service mask defined, return samples that are only in the mask.
             *
             * @param  {Object} layerConfig
             * @param  {external:Geometry} geometry The geometry used to interpolate values. Can be point, extent or polygon.
             *                             If geometry type is extent or polygon, use centroid or samples points.
             * @param  {Object} scope Calculation scope
             * @return {Array.<external:Point>} Samples points
             */
            _getSamplesPoints: function (layerConfig, geometry, scope) {
                var isPoint = geometry.type === "point";
                var isPolygon = geometry.type === "polygon";
                var isExtent = geometry.type === "extent";

                var hasMask = typeof layerConfig.mask !== "undefined" && typeof layerConfig.mask.url !== "undefined";

                var samplesPoints;

                var excludeSample = function (sampleResult) {
                    var samplePoint;
                    var hasResult = typeof sampleResult !== "undefined" && typeof sampleResult.location !== "undefined";
                    if (hasResult) {
                        var exclude = layerConfig.mask.exclude || "NoData";
                        if (sampleResult.value !== exclude) {
                            samplePoint = geometryJsonUtils.fromJson(sampleResult.location);
                        }
                    }
                    return samplePoint;
                };

                // Geometry is Point
                if (isPoint) {
                    if (hasMask) {
                        samplesPoints = this._identifyImageService(layerConfig.mask, geometry, scope).then(function (result) {
                            var maskSamplesPoints = [];
                            var sample = excludeSample(result);
                            if (sample) {
                                maskSamplesPoints.push(sample);
                            }
                            return maskSamplesPoints;
                        });
                    }
                    else {
                        // Return the point geometry directly
                        samplesPoints = [geometry];
                    }
                } else if (isPolygon || isExtent) {

                    // sampleCount: default value 100
                    if (typeof layerConfig.sampleCount === "undefined") {
                        layerConfig.sampleCount = 100;
                    }

                    // if mask url is defined
                    if (hasMask) {
                        samplesPoints = this._querySamplePoints(layerConfig, geometry, scope).then(function (result) {
                            var maskSamplesPoints = [];
                            array.forEach(result, function (item) {
                                var sample = excludeSample(item);
                                if (sample) {
                                    maskSamplesPoints.push(sample);
                                }
                            });
                            return maskSamplesPoints;
                        });
                    }
                    else {
                        // Creates samples points within the polygon or extent
                        samplesPoints = this._createSamplePoints(layerConfig.sampleCount, geometry);
                    }
                }
                return when(samplesPoints);
            },

            /**
             * Get sample points by using the mask in config
             *
             * @param  {Object} layerConfig
             * @param  {external:Geometry} geometry Polygon or extent geometry
             */
            _querySamplePoints: function (layerConfig, geometry, scope) {
                var getSampleConfig = lang.clone(layerConfig);
                getSampleConfig.url = this._substituteScopeElement(layerConfig.mask.url, scope.input);

                if (layerConfig.mask.mosaicRule) {
                    getSampleConfig.mosaicRule = layerConfig.mask.mosaicRule;
                    if (getSampleConfig.mosaicRule.where) {
                        getSampleConfig.mosaicRule.where = this._substituteScopeElement(getSampleConfig.mosaicRule.where, scope.input);
                    }
                }

                var sampleCount;
                if (typeof layerConfig.mask.sampleCount !== "undefined") {
                    // Use the sampleCount in the mask config
                    sampleCount = layerConfig.mask.sampleCount;
                }
                else {
                    // Use the sampleCount in the layer config
                    sampleCount = layerConfig.sampleCount;
                }

                // Quering only one sample doesn't works.
                // Ask for 100 samples and return only the middle one.
                var onlyOneSample = sampleCount === 1;
                if (onlyOneSample) {
                    getSampleConfig.sampleCount = 100;
                }

                var promise = this._queryGetSamples(getSampleConfig, geometry, scope).then(lang.hitch(this, function (result) {
                    var samplesPoints = [];
                    var hasResults = typeof result !== "undefined" && typeof result.samples !== "undefined";
                    if (hasResults) {
                        if (onlyOneSample) {
                            var middleIndex = result.samples.length / 2;
                            if (middleIndex > 0) {
                                middleIndex = Math.ceil(middleIndex) - 1;
                                var sample = result.samples[middleIndex];
                                samplesPoints.push(sample);
                            }
                        }
                        else {
                            samplesPoints = result.samples;
                        }
                    }
                    return samplesPoints;
                }));
                return promise;
            },

            /**
             * Interpolates values by using csv files.
             * There is one csv file with all points reference (having an id, lat, long).
             * Each reference point have a separate csv file with values for the interpolation.
             *
             * @param  {Object} layerConfig
             * @param  {external:Geometry} geometry The geometry used to interpolate values. Can be point, extent or polygon.
             *                             If geometry type is extent or polygon, use centroid or samples points.
             * @param {Object} The calculation scope
             */
            _interpolateCsv: function (layerConfig, geometry, scope) {
                var promise;
                var configValid = layerConfig.index && layerConfig.indexId &&
                    layerConfig.x && layerConfig.y && layerConfig.srs;
                if (configValid) {
                    var params = {
                        index: layerConfig.index,
                        indexId: layerConfig.indexId,
                        srs: layerConfig.srs,
                        x: layerConfig.x,
                        y: layerConfig.y,
                        directory: layerConfig.directory,
                        outputSrs: this.map,
                        interpolationCount: layerConfig.interpolation_count,
                        relatePattern: layerConfig.relate_pattern,
                        uniquePattern: layerConfig.unique_pattern,
                        uniqueName: layerConfig.unique_name,
                        uniqueLabel: layerConfig.unique_label,
                        interpolationAttributes: layerConfig.interpolation_attributes,
                        filterRelates: lang.hitch(this, this._filterInterpolationRelates, layerConfig, scope)
                    };

                    if (typeof layerConfig.keep_geometry !== "undefined") {
                        params.keepGeometry = layerConfig.keep_geometry;
                    }
                    if (typeof layerConfig.unique_label_pattern !== "undefined") {
                        params.uniqueLabelPattern = layerConfig.unique_label_pattern;
                    }
                    if (typeof layerConfig.group_by_sort !== "undefined") {
                        params.groupBySort = layerConfig.group_by_sort;
                    }

                    var proxyUrl = this._getProxyUrl();
                    if (proxyUrl) {
                        params.proxyUrl = proxyUrl;
                    }

                    var deferred = new Deferred();
                    var promiseModule = deferred.promise;
                    require(["../interpolation/CsvIDW"], function (CsvIDW) {
                        deferred.resolve(CsvIDW);
                    });

                    promise = promiseModule.then(lang.hitch(this, function (CsvIDW) {
                        var interpolation = new CsvIDW(params);
                        var promiseSamples = this._getSamplesPoints(layerConfig, geometry, scope);
                        return interpolation.interpolate(promiseSamples)
                            .then(lang.hitch(this, this._formatInterpolationResults, layerConfig))
                            .then(lang.hitch(this, function (results) {
                                return {
                                    type: this.tasks.INTERPOLATE_CSV,
                                    csvInterpolationResults: results,
                                    config: layerConfig
                                };
                            }));
                    }));

                }
                return promise;
            },

            /**
             * If specified in config, format the interpolation results
             *
             * @param  {any} layerConfig
             * @param  {Array} results Interpolation results
             */
            _formatInterpolationResults: function (layerConfig, results) {
                array.forEach(results, lang.hitch(this, function (result) {
                    // Format the interpolation result as specified in config file
                    if (layerConfig.interpolation_format) {
                        for (var variableName in layerConfig.interpolation_format) {
                            if (layerConfig.interpolation_format.hasOwnProperty(variableName)) {
                                var configFormat = layerConfig.interpolation_format[variableName];
                                // Apply number formatting when required (if specified in config)
                                this._applyNumberFormatting(result, variableName, configFormat);
                            }
                        }
                    }
                }));
                return results;
            },

            /**
             * Create a dojo/store query for the relates csv data used for the csv interpolation.
             *
             * @param  {Object} layerConfig
             * @param  {Object} scope Calculation scope
             * @return {string | function} dojo/store query
             */
            _getInterpolationRelateQuery: function (layerConfig, scope) {
                var relateQuery;
                if (layerConfig.dates_query) {
                    var datesQuery = this._substituteScopeElement(layerConfig.dates_query, scope.input);
                    var day = datesQuery.day;
                    var month = datesQuery.month;
                    var year = datesQuery.year;
                    var dateFrom = datesQuery.from;
                    var dateTo = datesQuery.to;
                    relateQuery = lang.hitch(this, this._storeQueryBetweenTwoDates, day, month, year, dateFrom, dateTo);
                }
                else if (layerConfig.relate_query) {
                    relateQuery = this._substituteScopeElement(layerConfig.relate_query, scope.input);
                }
                return relateQuery;
            },

            /**
             * Process calculations on relates, before the interpolation or return data as is.
             *
             * @param  {Object} layerConfig
             * @param  {Object} scope Calculation scope
             * @param  {Csv} store Relates CSV Store
             * @return {Array} relate Related calculation result data
             */
            _filterInterpolationRelates: function (layerConfig, scope, store) {
                var hasRelateSelect = typeof layerConfig.relate_select !== "undefined" &&
                    typeof layerConfig.relate_select.query !== "undefined" &&
                    typeof layerConfig.relate_select.select !== "undefined";

                var queryAll = this._getInterpolationRelateQuery(layerConfig, scope);
                var currentRelates = store.query(queryAll);

                var relate;
                if (hasRelateSelect) {
                    relate = {};
                    if (currentRelates.length > 0) {
                        var firstRelate = currentRelates[0];
                        lang.mixin(relate, firstRelate);
                    }
                    var querySelect = this._substituteScopeElement(layerConfig.relate_select.query, scope.input);
                    var selectData = store.query(querySelect);
                    array.forEach(layerConfig.relate_select.select, lang.hitch(this, function (item) {
                        //jshint unused:false
                        var getValue = function (itemValue, idx, arr) { return itemValue[item.attribute]; };
                        relate[item.selection] = array.map(selectData, getValue);
                        this._evaluateScope(relate, item);
                    }));
                    relate = [relate];
                }
                else {
                    relate = currentRelates;
                }
                return relate;
            },

            /**
             * Execute a Image Service Histogram, Get Sample or a Identity request depending
             * on the config and on the input geometry. May also query a Attribute Table.
             *
             * @param  {Object} layerConfig The config for the layer
             * @param  {external:Geometry} geometry The area of interest. Can be polygon, extent or point.
             * @return {external:Promise} The request promise
             */
            _queryImageService: function (layerConfig, geometry, scope) {
                var promise;
                // Raster Histogram
                var isPolygonOrExtent = geometry.type === "polygon" || geometry.type === "extent";
                var isHistogram = layerConfig.task && layerConfig.task === this.tasks.IMAGE_SERVICE_HISTOGRAM;
                if (isPolygonOrExtent && isHistogram) {

                    var deferred = new Deferred();
                    var promiseModule = deferred.promise;
                    require(["../raster/Histogram"], function (Histogram) {
                        deferred.resolve(Histogram);
                    });
                    var promiseHistogram = this._queryImageHistogram(layerConfig, geometry, scope).then(lang.hitch(this, function (result) {
                        return {
                            type: this.tasks.IMAGE_SERVICE_HISTOGRAM,
                            histograms: result.histograms,
                            config: layerConfig
                        };
                    }));
                    promise = all([promiseModule, promiseHistogram]).then(function (items) {
                        var Histogram = items[0];
                        var result = items[1];
                        array.forEach(result.histograms, function (item) {
                            var values = new Histogram({ histogram: item }).interpolate();
                            item.values = values;
                        });
                        return result;
                    });
                }
                else if (isPolygonOrExtent) {
                    promise = this._queryGetSamples(layerConfig, geometry, scope).then(lang.hitch(this, function (result) {
                        return {
                            type: this.tasks.GET_SAMPLES,
                            imageServiceSampleResult: result.samples,
                            config: layerConfig
                        };
                    }));
                }
                else {
                    // Image Service Identify Task
                    promise = this._identifyImageService(layerConfig, geometry, scope).then(lang.hitch(this, function (result) {
                        return {
                            type: this.tasks.IMAGE_IDENTIFY,
                            imageServiceIdentifyResult: result,
                            config: layerConfig
                        };
                    }));
                }

                // Raster Attribute Table
                var isRasterAttributeTable = typeof layerConfig[this.tasks.IMAGE_SERVICE_ATTRIBUTE_TABLE] !== "undefined";
                if (isRasterAttributeTable) {
                    // Query Raster Attribute Table
                    var promiseAttributeTable = this._queryRasterAttributeTable(layerConfig, scope);
                    promise = all([promise, promiseAttributeTable]).then(function (promisesResults) {
                        promisesResults[0].attributeTable = promisesResults[1];
                        return promisesResults[0];
                    });
                }
                return promise;
            },

            /**
             * Execute a query request
             * May execute a query relation request if specified in the config.
             * May execute a Image Service Indentify or Get Sample request if a mask is specified in config.
             *
             * @param  {Object} layerConfig The config for the layer
             * @param  {external:Geometry} geometry The area of interest. Can be polygon, extent or point.
             * @param  {Object} scope Calculation scope
             * @return {external:Promise} The request promise
             */
            _queryMapOrFeatureService: function (layerConfig, geometry, scope) {
                // Query Task
                var promiseMapService = this._queryMapService(layerConfig, geometry, scope);
                var promises = {
                    promiseMapService: promiseMapService
                };
                var promiseMask = this._queryMask(layerConfig, geometry, scope);
                if (promiseMask) {
                    promises.promiseMask = promiseMask;
                }
                return all(promises).then(lang.hitch(this, function (result) {
                    var featureSet = result.promiseMapService;
                    var queryData = {
                        type: this.tasks.QUERY,
                        featureSet: featureSet,
                        config: layerConfig
                    };
                    if (promiseMask) {
                        var maskSamples = result.promiseMask;
                        queryData.maskFeatureSet = maskSamples;
                    }
                    return queryData;
                })).then(lang.hitch(this, function (result) {
                    var relationPromises = this._queryRelation(layerConfig, result.featureSet, scope);
                    if (relationPromises && relationPromises.length > 0) {
                        // Relations
                        return all(relationPromises).then(function (relationFeatureSets) {
                            result.relationFeatureSets = relationFeatureSets;
                            return result;
                        });
                    }
                    return result;
                }));
            },

            /**
             * Query mask information using a Image Service Identify or Get Samples.
             * Mask information points will be added in a select store.
             *
             * @param  {Object} layerConfig
             * @param  {external:Geometry} geometry The area of interest. Can be polygon, extent or point.
             * @param  {Object} scope The calculation scope
             * @return {external:Promise} mask Mask query result promise
             */
            _queryMask: function (layerConfig, geometry, scope) {
                var promise;
                var hasMask = typeof layerConfig.mask !== "undefined";
                var isPolygon = geometry.type === "polygon";
                if (hasMask) {
                    var isPoint = geometry.type === "point";
                    var isExtent = geometry.type === "extent";
                    if (isPoint) {
                        // Identify Image Service (mask)
                        promise = this._identifyImageService(layerConfig.mask, geometry, scope).then(lang.hitch(this, function (result) {
                            return {
                                type: this.subprocess.MASK_IDENTIFY_IMAGE_SERVICE,
                                result: [result]
                            };
                        }));
                    }
                    else if (isPolygon || isExtent) {
                        // Get Samples Image Service (mask)
                        promise = this._queryGetSamples(layerConfig.mask, geometry, scope).then(lang.hitch(this, function (result) {
                            return {
                                type: this.subprocess.MASK_GET_SAMPLES,
                                result: result.samples
                            };
                        }));
                    }
                }
                return promise;
            },

            _queryForGeometry: function (layerConfig, geometry, scope) {

                var promise;
                if (layerConfig.geometry) {

                    var config = layerConfig.geometry;
                    var url = this._substituteScopeElement(config.url, scope.input);
                    var type = this._getLayerType(url);

                    var isMapServiceOrFeatureService = type === this.layerServiceType.MAP_SERVICE ||
                        type === this.layerServiceType.FEATURE_SERVICE;
                    if (isMapServiceOrFeatureService) {
                        promise = this._queryMapOrFeatureService(config, geometry, scope).then(lang.hitch(this, function (result) {
                            var geometry = null;
                            if (result && result.featureSet.features.length > 0) {
                                geometry = result.featureSet.features[0].geometry;
                            }
                            return geometry;
                        }));
                    }
                }
                return promise;
            },

            /**
             * Query a service (Image Service, Map Service, WMS, CSV files)
             *
             * @param  {Object} layerConfig
             * @param  {external:Geometry} geometry The area of interest
             * @param  {Object} scope Calculation scope
             * @return {external:Promise} promise The query promise result
             */
            _queryService: function (layerConfig, geometry, scope) {
                var promise;

                var isInterpolateCsvTask = layerConfig.task && layerConfig.task === this.tasks.INTERPOLATE_CSV;
                if (isInterpolateCsvTask) {
                    promise = this._interpolateCsv(layerConfig, geometry, scope);
                }
                else {
                    var url = this._substituteScopeElement(layerConfig.url, scope.input);
                    var type = this._getLayerType(url);
                    if (typeof type === "undefined") {
                        return promise;
                    }
                    // ImageService
                    var isImageService = type === this.layerServiceType.IMAGE_SERVICE;
                    if (isImageService) {
                        var promiseGeometry = this._queryForGeometry(layerConfig, geometry, scope);
                        if (promiseGeometry) {
                            // pre-process an other query before querying the image service
                            promise = promiseGeometry.then(lang.hitch(this, function (geometry) {
                                return this._queryImageService(layerConfig, geometry, scope);
                            }));

                        } else {
                            promise = this._queryImageService(layerConfig, geometry, scope);
                        }
                    }

                    // Map Service or Feature Service
                    var isMapServiceOrFeatureService = type === this.layerServiceType.MAP_SERVICE ||
                        type === this.layerServiceType.FEATURE_SERVICE;
                    if (isMapServiceOrFeatureService) {
                        promise = this._queryMapOrFeatureService(layerConfig, geometry, scope);
                    }

                    var isWMS = type === this.layerServiceType.WMS_SERVICE;
                    if (isWMS) {
                        promise = this._queryWMSGetFeatureInfo(layerConfig, geometry, scope);
                    }
                }
                return promise;
            },

            /**
             * Check if we must do it or ignore it based on the config
             *
             * @param  {Object} config
             * @param  {Object} scope
             * @return {Boolean}  Is the config condition is the same as current input data
             */
            _filterCondition: function (config, scope) {
                var isSame = true;
                var hasCondition = typeof config.condition !== "undefined";
                if (hasCondition) {
                    var isInConditionArray = function (conditionKey, value) {
                        return scope.input[conditionKey] === value;
                    };
                    for (var conditionKey in config.condition) {
                        if (config.condition.hasOwnProperty(conditionKey)) {
                            var conditionValue = config.condition[conditionKey];
                            var isArray = conditionValue instanceof Array;
                            if (isArray) {
                                isSame = array.some(conditionValue, lang.hitch(this, isInConditionArray, conditionKey));
                            }
                            else {
                                isSame = scope.input[conditionKey] === conditionValue;
                            }
                            if (!isSame) {
                                break;
                            }
                        }
                    }
                }
                return isSame;
            },

            /**
             * Calculate metrics on each layers in the config
             *
             * @param  {Object} scope Calculation scope
             * @return {Array.<external:Promise>} Promise with the query data result
             */
            _calculateLayersMetrics: function (scope) {
                var geometry = scope.input.input_geometry;

                var calculate = lang.hitch(this, function (queryData) {
                    if (queryData.config && queryData.config.results) {
                        // Read results as is, bypass calculations
                        var queryResults = this._readResults(queryData);
                        scope.calc[queryData.config.results] = queryResults;
                        scope.keep.push(queryData.config.results);
                    }
                    else {
                        // Calculate
                        this._calculateLayerMetric(queryData, scope);
                    }

                    // Format calculation results
                    this._applyFormatting(scope.calc, queryData.config, scope);
                    return queryData;
                });

                var showError = function (error) {
                    console.log(error);
                };

                var promises = [];
                array.forEach(this.config.layers, lang.hitch(this, function (layerConfig) {
                    var doIt = this._filterCondition(layerConfig, scope);
                    if (doIt) {
                        var promise = this._queryService(layerConfig, geometry, scope);
                        if (typeof promise !== "undefined") {
                            promises.push(promise.then(calculate, showError));
                        }
                    }
                }));
                return promises;
            },

            /**
             * Return a new object with object2 property names not in object1.
             *
             * @param  {Object} object1
             * @param  {Object} object2
             * @return {Object} The differences
             */
            _getObjectPropertyDifferences: function (object1, object2) {
                var diffObject;
                for (var key in object2) {
                    if (!object1.hasOwnProperty(key)) {
                        if (typeof diffObject === "undefined") {
                            diffObject = {};
                        }
                        diffObject[key] = object2[key];
                    }
                }
                return diffObject;
            },

            /**
             * Add related attribute value (from Raster Attribute Table object).
             * Add the attribute and attribute value in the layerScope objects.
             *
             * @param  {Object} attributeTable
             * @param  {Object} config
             * @param  {Object} queryResult
             * @param  {Object} layerScope
             */
            _addAttributeTableValues: function (attributeTable, config, queryResult, layerScope) {
                if (attributeTable && config.rasterattributetable) {

                    if (typeof config.rasterattributetable === "string") {
                        var attribute = config.rasterattributetable;
                        var attributeValue = this.searchRasterAttributeTable(attributeTable,
                            attribute,
                            queryResult.value);
                        layerScope[attribute] = attributeValue;
                    }
                    else {
                        array.forEach(config.rasterattributetable, lang.hitch(this, function (attributeName) {
                            var attributeValue = this.searchRasterAttributeTable(attributeTable,
                                attributeName,
                                queryResult.value);
                            layerScope[attribute] = attributeValue;
                        }));
                    }
                }
            },

            /**
             * Depending on request type, read the result in a uniform manner
             *
             * @param  {Object} queryData
             * @return {Array} The request data received
             */
            _readResults: function (queryData) {
                var type = queryData.type;
                var queryResults = [];
                switch (type) {
                    case this.tasks.IMAGE_IDENTIFY:
                        queryResults = this._readImageIdentifyResults(queryData);
                        break;
                    case this.tasks.GET_SAMPLES:
                        queryResults = this._readImageSampleResults(queryData);
                        break;
                    case this.tasks.QUERY:
                        queryResults = this._readQueryFeatures(queryData);
                        break;
                    case this.tasks.WMS_FEATURE_INFO:
                        queryResults = this._readWMSFeatureInfoResults(queryData);
                        break;
                    case this.tasks.INTERPOLATE_CSV:
                        queryResults = this._readCSVInterpolate(queryData);
                        break;
                    case this.tasks.IMAGE_SERVICE_HISTOGRAM:
                        queryResults = this._readHistogramResults(queryData);
                        break;
                    default:
                        break;
                }
                return queryResults;
            },


            /**
             * Read Image Service identify request result in a uniform manner
             *
             * @param  {Object} queryData
             * @return {Array} The request data received
             */
            _readImageIdentifyResults: function (queryData) {
                var queryResults = [];

                // Check if value is defined
                var hasValue = typeof queryData.imageServiceIdentifyResult !== "undefined" &&
                    queryData.imageServiceIdentifyResult.value !== "NoData";
                if (hasValue) {
                    queryResults = [{ value: queryData.imageServiceIdentifyResult.value }];
                }
                return queryResults;
            },

            /**
             * Read CSV interpolation request and calculation result in a uniform manner
             *
             * @param  {Object} queryData
             * @return {Array} The request data received
             */
            _readCSVInterpolate: function (queryData) {
                var queryResults = [];
                // check if value is defined
                var hasValues = typeof queryData.csvInterpolationResults !== "undefined" &&
                    queryData.csvInterpolationResults.length > 0;
                if (hasValues) {
                    queryResults = queryData.csvInterpolationResults;
                }
                return queryResults;
            },

            _readHistogramResults: function (queryData) {
                if (!queryData.histograms) {
                    return;
                }
                return [{ histograms: queryData.histograms }];
            },

            /**
             * Read WMS request result in a uniform manner
             *
             * @param  {Object} queryData
             * @return {Array} The request data received
             */
            _readWMSFeatureInfoResults: function (queryData) {
                return queryData.wmsFeatureInfoResult;
            },

            /**
             * Read Image Service requestGet Samples request result in a uniform manner
             *
             * @param  {Object} queryData
             * @return {Array} The request data received
             */
            _readImageSampleResults: function (queryData) {
                var queryResults = [];

                // check if value is defined
                var hasValues = typeof queryData.imageServiceSampleResult !== "undefined" &&
                    queryData.imageServiceSampleResult.length > 0;
                if (hasValues) {
                    array.forEach(queryData.imageServiceSampleResult, lang.hitch(this, function (sample) {
                        if (sample.value !== "NoData") {
                            if (typeof sample.value === "string") {
                                //sample.value = numberUtils.parse(sample.value.replace(",", "."));
                                sample.value = Number(sample.value.replace(",", "."));  // Laurier; 20170908
                            }
                        }
                        queryResults.push(sample);
                    }));
                }
                return queryResults;
            },

            /**
             * Read Map Service Query request result in a uniform manner
             *
             * @param  {Object} queryData
             * @return {Array} The request data received
             */
            _readQueryFeatures: function (queryData) {
                var queryResults = [];

                // Read Features attributes
                var featureSetExists = typeof queryData.featureSet !== "undefined" &&
                    typeof queryData.featureSet.features !== "undefined";
                if (featureSetExists) {
                    // Read Feature Set
                    array.forEach(queryData.featureSet.features, lang.hitch(this, function (feature) {
                        var attributesExists = typeof feature.attributes !== "undefined";
                        if (attributesExists) {
                            // Add attributes
                            var result = {};
                            lang.mixin(result, feature.attributes);

                            // Add geometry
                            var hasGeometry = typeof feature.geometry !== "undefined";
                            if (hasGeometry) {
                                var geometry = feature.geometry;
                                var geometries = { geometry: geometry };
                                lang.mixin(result, geometries);
                            }
                            queryResults.push(result);
                        }
                    }));
                }

                return queryResults;
            },

            /**
             * Read Map Service Query Relationship request result in a uniform manner
             *
             * @param  {Object} queryData
             * @return {Array} The request data received
             */
            _readRelates: function (queryData) {
                var type = queryData.type;

                var queryResults = [];
                if (type === this.tasks.QUERY) {
                    queryResults = this._readQueryRelates(queryData);
                }

                return queryResults;
            },

            /**
             * Read Query Relationship request
             *
             * @param  {Object} queryData
             * @return {Object.<number, Object>} Dictionary of feature class OID, relate attributes.
             */
            _readQueryRelates: function (queryData) {
                var relates = [];
                // Check if relate data exists
                var relatesExists = typeof queryData.relationFeatureSets !== "undefined";
                if (relatesExists) {
                    array.forEach(queryData.relationFeatureSets, function (result, idx) {
                        relates[idx] = {};
                        var featuresByOID = [];
                        // Loop on each Spatial Layer ObjectID having related features
                        for (var objectid in result.relationFeatureSet) {
                            if (result.relationFeatureSet.hasOwnProperty(objectid)) {
                                var features = result.relationFeatureSet[objectid].features;
                                featuresByOID.push({ oid: objectid, features: features });
                            }
                        }
                        // Loop on each OID
                        array.forEach(featuresByOID, function (item) {
                            var oid = item.oid;
                            if (typeof relates[idx][oid] === "undefined") {
                                relates[idx][oid] = [];
                            }
                            var features = item.features;
                            // Loop on each features
                            array.forEach(features, function (feature) {
                                // Add feature attributes in a dictionary (key is OID)
                                relates[idx][oid].push(feature.attributes);
                            });
                        });
                    });
                }
                return relates;
            },

            /**
             * Using the ObjectID value, copy relate attributes values in the
             * corresponding feature attributes values.
             *
             * @param  {Object} queryData
             * @param  {Array} queryResult
             * @param  {Object} scope Calculation scope
             */
            _copyRelateSelect: function (queryData, queryResult, scope) {
                var config = queryData.config;

                var isRelateInConfig = typeof config.relate !== "undefined";
                var isRelateInScope = typeof scope.relate !== "undefined";

                var findOID = lang.hitch(this, function () {
                    var oidValue;
                    var isFeatureSetDefined = typeof queryData.featureSet !== "undefined";
                    if (isFeatureSetDefined) {
                        var oidField = this._findOIDField(queryData.featureSet);
                        var isOIDFieldExists = typeof oidField !== "undefined" &&
                            typeof oidField.name !== "undefined" &&
                            typeof queryResult[oidField.name] !== "undefined";
                        if (isOIDFieldExists) {
                            oidValue = queryResult[oidField.name];
                        }
                    }
                    return oidValue;
                });

                if (isRelateInConfig && isRelateInScope) {
                    var oidValue = findOID();
                    var isOIDValueExists = typeof oidValue !== "undefined";
                    if (isOIDValueExists) {
                        array.forEach(scope.relate, lang.hitch(this, function (scopeRelate) {
                            var isRelateSelectExists = typeof scopeRelate !== "undefined" &&
                                typeof scopeRelate[oidValue] !== "undefined";
                            if (isRelateSelectExists) {
                                var isSelectExists = typeof scopeRelate[oidValue].select !== "undefined";
                                if (isSelectExists) {
                                    if (typeof scope.select === "undefined") {
                                        scope.select = {};
                                    }
                                    var toTransfert = scopeRelate[oidValue].select;
                                    for (var key in toTransfert) {
                                        if (toTransfert.hasOwnProperty(key)) {
                                            if (typeof scope.select[key] === "undefined") {
                                                scope.select[key] = {};
                                            }
                                            scope.select[key][oidValue] = scopeRelate[oidValue].select[key];
                                        }
                                    }
                                }
                            }
                        }));
                    }
                }
            },

            /**
             * Calculate metrics on features
             *
             * @param  {Object} queryData
             * @param  {Object} scope Calculation scope
             */
            _calculateMetrics: function (queryData, scope) {
                var config = queryData.config;
				console.log(config.functionName, queryData.type);  // Laurier
                var queryResults = this._readResults(queryData);
                var attributeTable = queryData.attributeTable || undefined;
                array.forEach(queryResults, lang.hitch(this, function (queryResult) {
                    this._copyRelateSelect(queryData, queryResult, scope);
                }));
                var queryResult;
                // Use only the first query result
                if (queryResults && queryResults.length > 0) {
                    queryResult = queryResults[0];
                }
                this._calculateQueryResult(queryResult, config, scope, attributeTable);
            },

            /**
             * Clean all properties that are not in the specified keep object.
             *
             * @param  {Array} keep Properties names to keep
             * @param  {Object} clean Object to clean up
             */
            _cleanScope: function (keep, clean) {
                for (var key in clean) {
                    if (clean.hasOwnProperty(key)) {
                        var found = keep.indexOf(key) >= 0;
                        if (!found) {
                            clean[key] = null;
                        }
                    }
                }
            },

            /**
             * Clean up unused variable in scope setting them to null
             *
             * @param  {Object} queryData
             * @param  {Object} scope
             */
            _cleanCalculationMetrics: function (queryData, scope) {
                var config = queryData.config;

                // Is a keep property is specified in the config
                var mustKeepSpecified = typeof config.keep !== "undefined";

                var isCalcDefined = typeof scope.calc !== "undefined";

                if (mustKeepSpecified) {
                    scope.keep = scope.keep.concat(config.keep);
                }

                if (mustKeepSpecified && isCalcDefined) {
                    this._cleanScope(scope.keep, scope.calc);
                }

                var isSelect = typeof scope.select !== "undefined";
                if (mustKeepSpecified && isSelect) {
                    this._cleanScope(scope.keep, scope.select);
                }

                if (scope.relate) {
                    scope.relate = null;
                }
            },

            /**
             * Make calculation on each feature
             *
             * @param  {Array} queryResults
             * @param  {Object} scope
             * @param  {Object} configSelect
             * @return {Array} Calculation selection results
             */
            _calculateSelect: function (queryResults, scope, configSelect) {
                var selectionResults = [];

                // For each query result, add calculation result in an array
                array.forEach(queryResults, lang.hitch(this, function (queryResult) {

                    // Separate scope
                    var scopeSelect = {};
                    lang.mixin(scopeSelect, queryResult);

                    // Make input data available for calculation if it exists
                    if (typeof scope.input !== "undefined") {
                        lang.mixin(scopeSelect, scope.input);
                    }

                    this._evaluateScope(scopeSelect, configSelect);
                    selectionResults.push(scopeSelect);

                }));

                return selectionResults;
            },

            /**
             * Create a empty object store
             *
             * @param  {Object} scope
             * @param  {Object} configGroup
             */
            _createSelectStore: function (scope, configGroup) {
                var createStore = function () {
                    var data = [];
                    return new Memory({ data: data });
                };
                for (var groupName in configGroup) {
                    if (configGroup.hasOwnProperty(groupName)) {
                        var store = createStore();
                        var selecteds = {};
                        selecteds[groupName] = store;

                        if (typeof scope.select === "undefined") {
                            scope.select = {};
                        }
                        lang.mixin(scope.select, selecteds);
                    }
                }
            },

            /**
             * Add each calculation result in object store
             *
             * @param {Array} calculations Calculation selection results
             * @param {Object} scope
             * @param {Object} configGroup
             */
            _addToSelectStore: function (calculations, scope, configGroup) {
                var addToStore = function (store, attributes) {
                    // For each result
                    array.forEach(calculations, function (feature, idx) {
                        var obj = { id: idx };
                        // For each attributes
                        array.forEach(attributes, function (attribute) {
                            var hasProperty = feature.hasOwnProperty(attribute);
                            if (hasProperty) {
                                obj[attribute] = feature[attribute];
                            }
                        });
                        store.put(obj);
                    });
                };

                for (var groupName in configGroup) {
                    if (configGroup.hasOwnProperty(groupName)) {
                        var names = configGroup[groupName];
                        var store = scope.select[groupName];
                        addToStore(store, names);
                    }
                }
            },

            /**
             * Compute selection calculations, create a object store and add
             * proper attributes values in it.
             *
             * @param  {Array} queryResults
             * @param  {Object} config
             * @param  {Object} scope
             */
            _selectResult: function (queryResults, config, scope) {
                // Check if select is defined in config
                var selectIsDefined = typeof config.select !== "undefined" &&
                    typeof config.select.group !== "undefined";
                if (selectIsDefined) {
                    var calculations = this._calculateSelect(queryResults, scope, config.select);
                    this._createSelectStore(scope, config.select.group);
                    this._addToSelectStore(calculations, scope, config.select.group);
                }
            },

            /**
             * Read request results add compute selection calculations
             *
             * @param  {Object} queryData
             * @param  {Object} scope
             */
            _selectMetricsResults: function (queryData, scope) {
                var config = queryData.config;
                var queryResults = this._readResults(queryData);

                // Select Query Results for layers
                this._selectResult(queryResults, config, scope);
            },

            /**
             * Create a store with mask query data
             *
             * @param  {Object} queryData
             * @param  {Object} scope
             */
            _selectMetricsMask: function (queryData, scope) {
                var config = queryData.config;
                var hasMask = typeof queryData.maskFeatureSet !== "undefined" &&
                    typeof config.mask !== "undefined";
                if (hasMask) {
                    var scopeMask = {};
                    this._selectResult(queryData.maskFeatureSet.result, config.mask, scopeMask);
                    if (typeof scope.select === "undefined") {
                        scope.select = {};
                    }
                    lang.mixin(scope.select, scopeMask.select);
                }
            },

            /**
             * Create a store with relate query data
             *
             * @param  {Object} queryData
             * @param  {Object} scope
             */
            _selectMetricsRelates: function (queryData, scope) {
                var config = queryData.config;
                var hasRelate = typeof config.relate !== "undefined";
                if (hasRelate) {
                    var queryResultsRelates = this._readRelates(queryData);

                    var selectRelate = lang.hitch(this, function (oid, relates, idx) {
                        // Add selection in scope.relate[oid].select
                        if (!scope.relate) {
                            scope.relate = [];
                        }
                        var scopeRelate = {};
                        this._selectResult(relates, config.relate[idx], scopeRelate);

                        if (typeof scope.relate[idx] === "undefined") {
                            scope.relate[idx] = {};
                        }
                        if (typeof scope.relate[idx][oid] === "undefined") {
                            scope.relate[idx][oid] = {};
                        }
                        if (typeof scope.relate[idx][oid].select === "undefined") {
                            scope.relate[idx][oid].select = {};
                        }
                        lang.mixin(scope.relate[idx][oid].select, scopeRelate.select);
                    });

                    array.forEach(queryResultsRelates, lang.hitch(this, function (queryResultsRelate, idx) {
                        var hasRelates = false;
                        for (var oid in queryResultsRelate) {
                            if (queryResultsRelate.hasOwnProperty(oid)) {
                                var relates = queryResultsRelate[oid];
                                // Select Relates
                                selectRelate(oid, relates, idx);
                                hasRelates = true;
                            }
                        }
                        if (!hasRelates) {
                            this._createSelectStore(scope, config.relate[idx].select.group);
                        }
                    }));
                }
            },

            /**
             * When having multiple features for a query, create stores.
             *
             * @param  {Object} queryData
             * @param  {Object} scope
             */
            _selectMetrics: function (queryData, scope) {
                this._selectMetricsRelates(queryData, scope);

                this._selectMetricsMask(queryData, scope);

                this._selectMetricsResults(queryData, scope);
            },

            /**
             * Calculate metrics using formulas in config file for a query result
             *
             * @param  {Object} queryResult
             * @param  {Object} config
             * @param  {Object} scope
             * @param  {Object} attributeTable
             */
            _calculateQueryResult: function (queryResult, config, scope, attributeTable) {
                var layerScope = {};

                // Make input data available for calculation if it exists
                if (typeof scope.input !== "undefined") {
                    lang.mixin(layerScope, scope.input);
                }

                // Make select data available for calculation if it exists
                if (typeof scope.select !== "undefined") {
                    lang.mixin(layerScope, scope.select);
                }

                var calc;
                if (typeof queryResult !== "undefined") {
                    lang.mixin(layerScope, queryResult);
                    if (attributeTable) {
                        this._addAttributeTableValues(attributeTable, config, queryResult, layerScope);
                    }
                }
                else {
                    queryResult = {};
                }

                this._evaluateScope(layerScope, config);
                calc = this._getObjectPropertyDifferences(queryResult, layerScope);

                if (typeof calc !== "undefined") {
                    if (typeof scope.calc === "undefined") {
                        scope.calc = {};
                    }
                    lang.mixin(scope.calc, calc);
                }
            },

            /**
             * Substitute calculation result values given the i18n report config
             *
             * @param  {Object} config
             * @param  {Object} i18nReport
             * @param  {Object} scope Calculation scope
             */
            _i18nReplace: function (config, i18nReport, scope) {
                // Exemple in config:
                // "i18nReplace": {
                //     "precip_acc_class": "precip_acc_mean_class"
                // },

                if (typeof scope !== "undefined" &&
                    typeof config !== "undefined" &&
                    typeof config.i18nReplace !== "undefined") {
                    for (var i18nKey in config.i18nReplace) {
                        if (config.i18nReplace.hasOwnProperty(i18nKey)) {
                            var value = config.i18nReplace[i18nKey];
                            if (typeof scope[value] !== "undefined" &&
                                typeof i18nReport[i18nKey] !== "undefined" &&
                                typeof i18nReport[i18nKey][scope[value]] !== "undefined") {
                                scope[value] = i18nReport[i18nKey][scope[value]];
                            }
                        }
                    }
                }
            },

            /**
             * Using the function name in the config file, find and execute
             * the corresponding function in the config functions file.
             *
             * @param  {Object} scope
             * @param  {Object} config
             */
            _evaluateScope: function (scope, config) {
                if (typeof config.functionName !== "undefined" &&
                    typeof this.transform !== "undefined" &&
                    typeof this.transform[config.functionName] !== "undefined") {
                    this.transform[config.functionName](scope);
                }
            },

            /**
             * Show report default view
             *
             */
            _showReportsDefaultView: function () {

                // Show report only when the result tab is shown
                // If not, gauge report will fail drawing properly.

                var reportsFiles = this.configLoader.reportsFiles;

                var show = lang.hitch(this, function () {
                    this._clearReports();
                    var foundOptionData = this.settings.getOptions();

                    // Once all templates and i18n are resolved
                    array.forEach(reportsFiles, lang.hitch(this, function (reportFiles) {
                        if (reportFiles.configReport.type === this.reportTypes.GAUGE_BAR) {
                            this._showReportGaugeBar(reportFiles.configReport, reportFiles, {});
                        } else if (reportFiles.templateDefault) {
                            this._showReportDefaultView(reportFiles, foundOptionData);
                        }
                    }));
                });

                var selected = this.tabs.selectedChildWidget;
                if (!this._resultsTabsShowHandle && selected !== this.tabResults) {
                    this._resultsTabsShowHandle = aspect.after(this.tabResults, "onShow", lang.hitch(this, function () {
                        this._resultsTabsShowHandle.remove();
                        this._resultsTabsShowHandle = null;
                        show();
                    }), true);
                    this.own(this._resultsTabsShowHandle);
                } else {
                    show();
                }
            },

            _showReportDefaultView: function (reportFiles, foundOptionData) {
                var templateDefault = reportFiles.templateDefault;
                if (templateDefault) {
                    var params = { i18nWidget: i18n };
                    var i18nReport = reportFiles.i18nReport;

                    if (i18nReport) {
                        params.i18nReport = i18nReport;
                    }

                    var configReport = reportFiles.configReport;


                    var hasOptions = typeof foundOptionData !== "undefined";

                    params.options = {};
                    if (hasOptions) {
                        lang.mixin(params.options, foundOptionData);
                    }

                    var doIt = this._filterCondition(configReport, foundOptionData);
                    if (doIt) {
                        try {
                            var innerHTML = string.substitute(templateDefault, params);
                            this._showReport(innerHTML, configReport);
                        } catch (error) {
                            // Do Nothing
                        }
                    }
                }
            },

            /**
             * Apply number formatting when required (if specified in config)
             *
             * @param  {Object} calculationResult
             * @param  {string} variableName
             * @param  {Object} configFormat
             */
            _applyNumberFormatting: function (calculationResult, variableName, configFormat) {
                var isObject = (typeof configFormat.number === "object");
                var isBoolean = (typeof configFormat.number === "boolean");
                var isDefined = (isObject || isBoolean);
                if (isDefined) {
                    var options;
                    if (isObject) {
                        options = configFormat.number;
                    }
                    var result = calculationResult[variableName];
                    if (typeof result !== "undefined") {
                        var formatted = numberUtils.format(result, options);
                        if (formatted) {
                            calculationResult[variableName] = formatted;
                        }
                    }
                }
            },


            /**
             * Apply formatting to the calculation results given a config
             *
             * @param  {Object} calculationResult
             * @param  {Object} config
             */
            _applyConfigFormatting: function (calculationResult, config) {
                if (config.format) {
                    for (var variableName in config.format) {
                        if (config.format.hasOwnProperty(variableName)) {
                            var configFormat = config.format[variableName];

                            // Apply number formatting when required (if specified in config)
                            this._applyNumberFormatting(calculationResult, variableName, configFormat);
                        }
                    }
                }
            },

            /**
             * Apply formatting to the calculation results of each report
             *
             * @param  {Object} calculationResult
             * @param  {Object} config
             * @param  {Object} scope
             */
            _applyFormatting: function (calculationResult, config, scope) {
                this._applyConfigFormatting(calculationResult, config);
                this._addUnits(calculationResult, config, scope);
            },

            /**
             * Remove all reports in the reports view
             */
            _clearReports: function () {
                array.forEach(this.reports, function (report) {
                    report.clear();
                });
                this._cleanReports();
            },

            _cleanReports: function () {
                array.forEach(this.reports, function (report) {
                    report.clean();
                });
            },

            uniformizeCharts: function () {
                array.forEach(this.reports, function (report) {
                    report.uniformizeCharts();
                });
            },

            /**
             * Validate that all required calculated variables are present for the
             * specified report.
             *
             * @param  {string} configReport
             * @param  {Object} calculationResult
             * @return {boolean} if all required calculated variables are present
             */
            _areDependenciesValid: function (configReport, calculationResult) {
                if (typeof calculationResult === "undefined") {
                    return false;
                }

                var isValid = true;
                if (configReport.validation) {
                    isValid = array.every(configReport.validation, function (item) {
                        return calculationResult.hasOwnProperty(item);
                    });
                }
                return isValid;
            },

            /**
             * Calculate metrics for the layer
             *
             * @param  {Object} queryData
             * @param  {Object} scope Calculation scope
             */
            _calculateLayerMetric: function (queryData, scope) {
                // Select Metrics
                this._selectMetrics(queryData, scope);

                // Calculate metrics for features
                this._calculateMetrics(queryData, scope);

                // Clean up
                this._cleanCalculationMetrics(queryData, scope);
            },

            /**
             * Calculate metrics for each input in the config file
             *
             * @param  {Object} scope Calculation scope
             */
            _calculateInputsMetrics: function (scope) {
                var hasInputs = typeof this.config.inputs !== "undefined" &&
                    this.config.inputs.length > 0;
                if (hasInputs) {
                    // For each input in config file
                    array.forEach(this.config.inputs, lang.hitch(this, function (input) {
                        // Calculate metrics
                        this._evaluateScope(scope.input, input);
                    }));
                }

                var optionsData = this.settings.getOptions();
                if (optionsData) {
                    lang.mixin(scope.input, optionsData);
                }
            },

            /**
             * Calculate metrics for each report in the config
             *
             * @param  {Object} scope Calculation scope
             */
            _calculateReportsMetrics: function (scope) {
                var isScopeValid = typeof scope !== "undefined" && typeof scope.calc !== "undefined";

                if (isScopeValid) {
                    // For each report in config
                    array.forEach(this.config.reports, lang.hitch(this, function (report) {
                        var doIt = this._filterCondition(report, scope);
                        if (doIt) {
                            // Validate that all required calculated variables are present for the report.
                            var areDepensenciesValid = this._areDependenciesValid(report, scope.calc);
                            if (areDepensenciesValid) {
                                // Calculate global metrics
                                this._evaluateScope(scope.calc, report);
                                // Format calculation results
                                this._applyFormatting(scope.calc, report, scope);
                            }
                        }
                    }));
                }
            },

            /**
             * Show all reports specified in the config file
             *
             * @param  {Object} calculationResult
             */
            _showReports: function (calculationResult) {

                var reportsFiles = this.configLoader.reportsFiles;
                array.forEach(reportsFiles, lang.hitch(this, function (reportFiles) {

                    var configReport = lang.clone(reportFiles.configReport);

                    var doIt = this._filterCondition(configReport, calculationResult);
                    if (doIt) {
                        if (typeof reportFiles !== "undefined" &&
                            typeof reportFiles.i18nReport !== "undefined") {
                            var i18nReport = reportFiles.i18nReport;
                            this._i18nReplace(configReport, i18nReport, calculationResult);
                        }

                        // If there is a task defined, show report differently
                        if (typeof configReport.type !== "undefined") {
                            this._showReportSpecificType(reportFiles, configReport, calculationResult);
                        }
                        else {
                            // Show a report based on a calculation result
                            this._showReportCalculation(reportFiles, configReport, calculationResult);
                        }
                    }

                }));
            },

            /**
             * Show a report of a specific format (ex: Pie Chart, Line Chart)
             *
             * @param  {Object} reportFiles
             * @param  {Object} configReport
             * @param  {Object} calculationResult
             */
            _showReportSpecificType: function (reportFiles, configReport, calculationResult) {
                switch (configReport.type) {
                    case this.reportTypes.CHART_LINE:
                        this._showReportLineChart(configReport, reportFiles, calculationResult);
                        break;
                    case this.reportTypes.CHART_PIE:
                        this._showReportPieChart(configReport, reportFiles, calculationResult);
                        break;
                    case this.reportTypes.CHART_BAR:
                        this._showReportColumnsChart(configReport, reportFiles, calculationResult);
                        break;
                    case this.reportTypes.CHART_HISTOGRAM:
                        this._showReportHistogram(configReport, reportFiles, calculationResult);
                        break;
                    case this.reportTypes.GAUGE_BAR:
                        this._showReportGaugeBar(configReport, reportFiles, calculationResult);
                        break;
                    default:
                        break;
                }
            },

            /**
             * Create a metrics report widget if it doesn't exists.
             * Startup the report.
             *
             * @param  {Object} params
             * @param  {string} id
             * @return {Object} created of found report widget
             */
            _createOrReuseReport: function (params, id) {
                var node = dom.byId(id);
                var report = registry.byNode(node);

                if (typeof report === "undefined") {
                    var templateString = "<div data-dojo-attach-point='contentNode'></div>";
                    params.templateString = templateString;

                    report = new Report(params, node);
                    this.reports.push(report);
                }

                report.startup();

                return report;
            },

            _substitute: function (params, substituteScope) {
                if (typeof substituteScope === "undefined") {
                    substituteScope = {};
                }
                for (var key in params) {
                    if (params.hasOwnProperty(key)) {
                        var element = params[key];
                        if (typeof element === "string") {
                            var value = string.substitute(element, substituteScope);
                            params[key] = value;
                        }
                    }
                }
            },

            /**
             * Substitute each variable by the value in i18n files and
             * in the substitute scope.
             *
             * @param  {Object} params
             * @param  {Object} i18nReport
             */
            _substitutei18n: function (params, substituteScope) {
                if (typeof substituteScope === "undefined") {
                    substituteScope = {};
                }
                substituteScope.i18n = this.i18n;
                this._substitute(params, substituteScope);
            },

            /**
             * Create Pie Chart parameters
             *
             * @param  {Object} configReport
             * @param  {Object} reportFiles
             * @param  {Object} calculationResult
             * @return {Object} Parameters for the PieChart constructor
             */
            _createPieChartParams: function (configReport, reportFiles, calculationResult) {
                var i18nReport = reportFiles.i18nReport;

                // Mandatory parameters
                var params = {};

                var options = this.settings.getOptions();

                // title
                if (configReport.title) {
                    params.title = configReport.title;
                    this._substitutei18n(params.title, { i18nReport: i18nReport, options: options });
                }

                // classes
                var hasClasses = typeof configReport.classes !== "undefined";
                if (hasClasses) {
                    params.classes = configReport.classes;
                    array.forEach(params.classes, lang.hitch(this, function (item) {
                        var hasData = typeof item.data !== "undefined";
                        if (hasData) {
                            // data
                            item.data = calculationResult[item.data];
                        } else {
                            var hasGeometry = typeof item.geometry !== "undefined";
                            if (hasGeometry) {
                                item.geometry = calculationResult[item.geometry];
                            }
                            var hasClass = item.class;
                            var mustReplace = false;
                            if (hasClass) {
                                mustReplace = typeof calculationResult[item.class] !== "undefined";
                                if (mustReplace) {
                                    item.class = calculationResult[item.class];
                                }
                            }
                            var hasClassValue = item.class_value;
                            if (hasClassValue) {
                                mustReplace = typeof calculationResult[item.class_value] !== "undefined";
                                if (mustReplace) {
                                    item.class_value = calculationResult[item.class_value];
                                }
                            }
                        }
                    }));
                }

                // labels
                var hasLabels = typeof configReport.labels !== "undefined";
                if (hasLabels) {
                    if (configReport.labels && i18nReport) {
                        params.labels = i18nReport[configReport.labels];
                    }
                }

                // colors
                var hasColors = typeof configReport.colors !== "undefined";
                if (hasColors) {
                    params.colors = configReport.colors;
                }

                // geometries
                if (configReport.geometries && typeof calculationResult[configReport.geometries] !== "undefined") {
                    params.geometryData = calculationResult[configReport.geometries];
                }
                else {
                    params.geometryData = params.data;
                }

                // map
                if (this.map) {
                    params.map = this.map;
                }

                this._substitutei18n(params, { i18nReport: i18nReport, options: options });
                return params;
            },

            /**
             * Create Columns Chart parameters
             *
             * @param  {Object} configReport
             * @param  {Object} reportFiles
             * @param  {Object} calculationResult
             * @return {Object} Parameters for the ColumnsChart constructor
             */
            _createColumnsChartParams: function (configReport, reportFiles, calculationResult) {
                var i18nReport = reportFiles.i18nReport;

                // Mandatory parameters
                var params = {};

                var options = this.settings.getOptions();

                // title
                if (configReport.title) {
                    params.title = configReport.title;
                    this._substitutei18n(params.title, { i18nReport: i18nReport, options: options });
                }

                // classes
                var hasClasses = typeof configReport.classes !== "undefined";
                if (hasClasses) {
                    params.classes = configReport.classes;
                    array.forEach(params.classes, lang.hitch(this, function (item) {
                        var hasData = typeof item.data !== "undefined";
                        if (hasData) {
                            // data
                            item.data = calculationResult[item.data];
                        } else {
                            var hasGeometry = typeof item.geometry !== "undefined";
                            if (hasGeometry) {
                                item.geometry = calculationResult[item.geometry];
                            }
                            var hasClass = item.class;
                            var mustReplace = false;
                            if (hasClass) {
                                mustReplace = typeof calculationResult[item.class] !== "undefined";
                                if (mustReplace) {
                                    item.class = calculationResult[item.class];
                                }
                            }
                            var hasClassValue = item.class_value;
                            if (hasClassValue) {
                                mustReplace = typeof calculationResult[item.class_value] !== "undefined";
                                if (mustReplace) {
                                    item.class_value = calculationResult[item.class_value];
                                }
                            }
                        }
                    }));
                }

                // labels
                var hasLabels = typeof configReport.labels !== "undefined";
                if (hasLabels) {
                    if (configReport.labels && i18nReport) {
                        params.labels = i18nReport[configReport.labels];
                    }
                }

                // colors
                var hasColors = typeof configReport.colors !== "undefined";
                if (hasColors) {
                    params.colors = configReport.colors;
                }

                // series
                var hasSeries = typeof configReport.series !== "undefined";
                if (hasSeries) {
                    params.series = configReport.series;
                    array.forEach(params.series, lang.hitch(this, function (item) {
                        this._substitutei18n(item, { i18nReport: i18nReport, options: options });
                        item.data = calculationResult[item.data];
                    }));
                }

                var hasAxisXDatePattern = typeof configReport.axisXDatePattern !== "undefined";
                if (hasAxisXDatePattern) {
                    params.axisXDatePattern = configReport.axisXDatePattern;
                }

                var hasGap = typeof configReport.gap !== "undefined";
                if (hasGap) {
                    params.gap = configReport.gap;
                }

                // geometries
                if (configReport.geometries && typeof calculationResult[configReport.geometries] !== "undefined") {
                    params.geometryData = calculationResult[configReport.geometries];
                }
                else {
                    params.geometryData = params.data;
                }

                // map
                if (this.map) {
                    params.map = this.map;
                }

                // percentages
                if (configReport.percentages) {
                    params.percentages = true;
                }

                // Axis X and Axis Y dojox/Charting constructor parameters
                var hasAxisX = typeof configReport.axisX !== "undefined";
                var hasAxisY = typeof configReport.axisY !== "undefined";
                if (hasAxisX & hasAxisY) {
                    params.axisX = configReport.axisX;
                    this._substitutei18n(params.axisX, { i18nReport: i18nReport, options: options });
                    params.axisY = configReport.axisY;
                    this._substitutei18n(params.axisY, { i18nReport: i18nReport, options: options });
                }

                // tooltipFormat
                if (typeof configReport.tooltipFormat !== "undefined" &&
                    typeof this.transform !== "undefined" &&
                    typeof this.transform[configReport.tooltipFormat] !== "undefined") {
                    params.tooltipFormat = this.transform[configReport.tooltipFormat];
                }

                this._substitutei18n(params, { i18nReport: i18nReport, options: options });
                return params;
            },

            /**
            * Create a Pie Chart report
            *
            * @param  {Object} configReport
            * @param  {Object} reportFiles
            * @param  {Object} calculationResult
            */
            _showReportPieChart: function (configReport, reportFiles, calculationResult) {
                var hasParentNode = typeof configReport.parentNode !== "undefined";
                if (hasParentNode) {
                    var parentNodeId = string.substitute(configReport.parentNode, this);
                    var parentNode = dom.byId(parentNodeId);
                    var report = this._createOrReuseReport({}, parentNode);

                    // Create PieChart parameters
                    var params = this._createPieChartParams(configReport, reportFiles, calculationResult);

                    // Create the chart
                    require(["./charting/plot/Pie"], function (PieChart) {
                        var chart = new PieChart(params, parentNode);
                        report.addChart(chart);
                    });
                }
            },

            /**
            * Create a Bar Chart report
            *
            * @param  {Object} configReport
            * @param  {Object} reportFiles
            * @param  {Object} calculationResult
            */
            _showReportColumnsChart: function (configReport, reportFiles, calculationResult) {
                var hasParentNode = typeof configReport.parentNode !== "undefined";
                if (hasParentNode) {
                    var parentNodeId = string.substitute(configReport.parentNode, this);
                    var parentNode = dom.byId(parentNodeId);
                    var report = this._createOrReuseReport({}, parentNode);

                    // Create PieChart parameters
                    var params = this._createColumnsChartParams(configReport, reportFiles, calculationResult);

                    // Create the chart
                    require(["./charting/plot/Columns"], function (ColumnsChart) {
                        var chart = new ColumnsChart(params, parentNode);
                        report.addChart(chart);
                    });
                }
            },

            /**
             * Create Line Chart parameters
             *
             * @param  {any} configReport
             * @param  {any} reportFiles
             * @param  {any} calculationResult
             * @return {Object} Parameters for the LineChart constructor
             */
            _createLineChartParams: function (configReport, reportFiles, calculationResult) {
                var i18nReport = reportFiles.i18nReport;

                var params = {};

                var options = this.settings.getOptions();

                // Use series array instead of classes
                var hasSeries = typeof configReport.series !== "undefined";
                if (hasSeries) {
                    params.series = configReport.series;
                    array.forEach(params.series, lang.hitch(this, function (item) {
                        this._substitutei18n(item, { i18nReport: i18nReport, options: options });
                        item.data = calculationResult[item.data];
                    }));
                }

                // title
                if (configReport.title) {
                    params.title = configReport.title;
                    this._substitutei18n(params.title, { i18nReport: i18nReport, options: options });
                }

                // Axis X and Axis Y dojox/Charting constructor parameters
                var hasAxisX = typeof configReport.axisX !== "undefined";
                var hasAxisY = typeof configReport.axisY !== "undefined";
                if (hasAxisX & hasAxisY) {
                    params.axisX = configReport.axisX;
                    this._substitutei18n(params.axisX, { i18nReport: i18nReport, options: options });
                    params.axisY = configReport.axisY;
                    this._substitutei18n(params.axisY, { i18nReport: i18nReport, options: options });
                }

                // axisXDatePattern
                var hasAxisXDatePattern = typeof configReport.axisXDatePattern !== "undefined";
                if (hasAxisXDatePattern) {
                    params.axisXDatePattern = configReport.axisXDatePattern;
                }

                // Formatting method for the x axis labels
                var hasLabelFormatX = typeof configReport.labelFormatX !== "undefined";
                if (hasLabelFormatX) {
                    params.labelFormatX = configReport.labelFormatX;
                }

                // map
                if (this.map) {
                    params.map = this.map;
                }

                // geometries
                if (configReport.geometries && typeof calculationResult[configReport.geometries] !== "undefined") {
                    params.geometryData = calculationResult[configReport.geometries];
                }
                else {
                    params.geometryData = params.data;
                }

                // Horizontal lines
                var hasHorizontalLines = typeof configReport.horizontalLines !== "undefined";
                if (hasHorizontalLines) {
                    params.horizontalLines = [];
                    // For each horizontal lines in config
                    array.forEach(configReport.horizontalLines, lang.hitch(this, function (item) {
                        var hasValue = typeof item.value !== "undefined" &&
                            typeof calculationResult[item.value] !== "undefined";
                        if (hasValue) {
                            // Horizontal line value
                            var horizontalLineParam = {
                                value: calculationResult[item.value]
                            };
                            // Horizontal line color
                            var hasHorizontalLineColor = typeof item.color !== "undefined";
                            if (hasHorizontalLineColor) {
                                horizontalLineParam.color = item.color;
                            }
                            // Horizontal line width
                            var hasHorizontalLineWidth = typeof item.width !== "undefined";
                            if (hasHorizontalLineWidth) {
                                horizontalLineParam.width = item.width;
                            }
                            // Horizontal line label
                            var hasHorizontalLineLabel = typeof item.label !== "undefined";
                            if (hasHorizontalLineLabel) {
                                horizontalLineParam.label = item.label;
                            }
                            // Horizontal line precision
                            var hasHorizontalLinePrecision = typeof item.precision !== "undefined";
                            if (hasHorizontalLinePrecision) {
                                horizontalLineParam.precision = item.precision;
                            }
                            // Horizontal line offset
                            var hasHorizontalLineOffset = typeof item.offset !== "undefined";
                            if (hasHorizontalLineOffset) {
                                horizontalLineParam.offset = item.offset;
                            }

                            this._substitutei18n(horizontalLineParam, { i18nReport: i18nReport, options: options });

                            params.horizontalLines.push(horizontalLineParam);
                        }
                    }));
                }
                this._substitutei18n(params, { i18nReport: i18nReport, options: options });
                return params;
            },

            /**
             * Create a Line Chart Report
             *
             * @param  {Object} configReport
             * @param  {Object} reportFiles
             * @param  {Object} calculationResult
             */
            _showReportLineChart: function (configReport, reportFiles, calculationResult) {
                var hasParentNode = typeof configReport.parentNode !== "undefined";
                if (hasParentNode) {
                    var parentNodeId = string.substitute(configReport.parentNode, this);
                    var parentNode = dom.byId(parentNodeId);
                    var report = this._createOrReuseReport({}, parentNode);

                    // chart parameters
                    var params = this._createLineChartParams(configReport, reportFiles, calculationResult);

                    // Create the chart
                    require(["./charting/plot/Line"], function (LineChart) {
                        var chart = new LineChart(params, parentNode);
                        report.addChart(chart);
                    });
                }
            },

            /**
             * Create histogram chart parameters
             *
             * @param  {Object} configReport
             * @param  {Object} reportFiles
             * @param  {Object} histogram
             * @param  {Object} attributeTable
             * @param  {Object} calculationResult
             * @return {Object} Parameters for the RasterHistogram constructor
             */
            _createHistogramParams: function (configReport, reportFiles, histogram, attributeTable, calculationResult) {
                var i18nReport = reportFiles.i18nReport;

                // mandatory parameters
                var params = {
                    histogram: histogram
                };

                var options = this.settings.getOptions();

                // title
                if (configReport.title) {
                    params.title = configReport.title;
                    this._substitutei18n(params.title, { i18nReport: i18nReport, options: options });
                }

                // axisX
                var hasAxisX = typeof configReport.axisX !== "undefined";
                if (hasAxisX) {
                    params.axisX = configReport.axisX;
                    this._substitutei18n(params.axisX, { i18nReport: i18nReport, options: options });
                }

                // axisY
                var hasAxisY = typeof configReport.axisY !== "undefined";
                if (hasAxisY) {
                    params.axisY = configReport.axisY;
                    this._substitutei18n(params.axisY, { i18nReport: i18nReport, options: options });
                }

                // places
                var hasPlaces = typeof configReport.places !== "undefined";
                if (hasPlaces) {
                    params.places = configReport.places;
                }

                // geometries
                if (configReport.geometries && typeof calculationResult[configReport.geometries] !== "undefined") {
                    params.geometryData = calculationResult[configReport.geometries];
                }
                else {
                    params.geometryData = params.data;
                }


                // Raster Attribute Table
                var hasAttributeTable = typeof attributeTable !== "undefined" &&
                    typeof configReport[this.tasks.IMAGE_SERVICE_ATTRIBUTE_TABLE] !== "undefined";
                if (hasAttributeTable) {
                    var attributeName = configReport[this.tasks.IMAGE_SERVICE_ATTRIBUTE_TABLE];
                    params.replaceValue = lang.hitch(this, this.searchRasterAttributeTable, attributeTable, attributeName);
                }

                // map
                if (this.map) {
                    params.map = this.map;
                }

                this._substitutei18n(params, { i18nReport: i18nReport, options: options });
                return params;
            },

            /**
             * Show a histogram report.
             *
             * @param  {Object} configReport Report config section
             * @param  {Object} reportFiles Reports files
             * @param  {Object} calculationResult calculation result data
             */
            _showReportHistogram: function (configReport, reportFiles, calculationResult) {
                var hasParentNode = typeof configReport.parentNode !== "undefined";

                var histogramsName = configReport.histogram;

                if (histogramsName && calculationResult[histogramsName] && hasParentNode) {
                    var parentNodeId = string.substitute(configReport.parentNode, this);
                    var parentNode = dom.byId(parentNodeId);
                    var report = this._createOrReuseReport({}, parentNode);

                    var attributeDataName = configReport.attributeTable;

                    var attributeTable = calculationResult[attributeDataName];
                    array.forEach(calculationResult[histogramsName], lang.hitch(this, function (histogram) {
                        // Chart parameters
                        var params = this._createHistogramParams(configReport, reportFiles, histogram, attributeTable, calculationResult);
                        // Create the chart

                        require(["./charting/plot/RasterHistogram"], function (RasterHistogram) {
                            var chart = new RasterHistogram(params, parentNode);
                            report.addChart(chart);
                        });
                    }));
                }
            },

            _showReportGaugeBar: function (configReport, reportFiles, calculationResult) {

                var config = lang.clone(configReport);

                var i18nReport = reportFiles.i18nReport;
                var options = this.settings.getOptions();

                config = JSON.stringify(config);
                config = string.substitute(config, { i18nReport: i18nReport, options: options, id: this.id });
                config = JSON.parse(config);

                var valueNames = config.value;
                delete config.value;

                var values = [];
                array.forEach(valueNames, function (valueName) {
                    if (typeof calculationResult[valueName] !== "undefined") {
                        values.push(calculationResult[valueName]);
                    }
                });

                var defaultParams = {
                    minimum: 0,
                    maximum: 100
                };

                var params = {
                    values: values
                };

                lang.mixin(params, defaultParams);
                lang.mixin(params, config);

                var parentNodeId = string.substitute(config.parentNode, this);
                var parentNode = dom.byId(parentNodeId);
                var report = this._createOrReuseReport({}, parentNode);

                report.addGauge(params);
            },

            /**
             * Show a report based on a calculation result
             *
             * @param  {Object} reportFiles
             * @param  {Object} configReport
             * @param  {Object} calculationResult
             */
            _showReportCalculation: function (reportFiles, configReport, calculationResult) {
                var templateData = reportFiles.templateData;
                var templateNoData = reportFiles.templateNoData;
                var i18nReport = reportFiles.i18nReport;

                var isReportDataValid = this._areDependenciesValid(configReport, calculationResult);

                var options = this.settings.getOptions();

                var template;
                if (isReportDataValid) {
                    template = templateData;
                }
                else {
                    template = templateNoData;
                }
                var templateParams = {
                    result: calculationResult,
                    i18nWidget: i18n,
                    options: options
                };
                if (i18nReport) {
                    templateParams.i18nReport = i18nReport;
                }
                this._showReport(template, configReport, templateParams);
            },

            /**
             * Show a HTML report
             *
             * @param  {Object} template
             * @param  {Object} configReport
             * @param  {?Object} templateParams
             */
            _showReport: function (template, configReport, templateParams) {
                try {
                    var hasParentNode = typeof configReport.parentNode !== "undefined";
                    if (hasParentNode) {
                        var parentNode = string.substitute(configReport.parentNode, this);
                        var innerHTML = string.substitute(template, templateParams);
                        var report = this._createOrReuseReport({}, parentNode);

                        var node = domConstruct.create("span", { innerHTML: innerHTML });
                        domConstruct.place(node, report.contentNode, "last");
                    }
                } catch (error) {
                    // Do Nothing
                }
            },

            /**
             * Show a busy animation for each report
             *
             */
            _showBusy: function () {
                this.isBusy = true;
                array.forEach(this.reports, function (report) {
                    report.showBusy();
                });
            },

            /**
             * Hide the busy animation for each report
             *
             */
            _hideBusy: function () {
                if (this.isBusy) {
                    array.forEach(this.reports, function (report) {
                        report.hideBusy();
                    });
                    this.isBusy = false;
                }
            },

            /**
             * Query, calculate and show reports metrics for the specified graphics
             *
             * @param  {Array.<external:Graphic>} graphics
             */
            _showMetrics: function (graphics) {
                var isNotBusy = !this.isBusy;
                var hasGraphics = graphics && graphics.length > 0;
                // Check if there is not a calculation in progress and if there are input graphics
                if (isNotBusy && hasGraphics) {
                    var type;
                    var geometries = [];
                    var allSameType = array.every(graphics, function (graphic) {
                        var same = true;
                        if (typeof type !== "undefined") {
                            same = type === graphic.geometry.type;
                        }
                        else {
                            type = graphic.geometry.type;
                        }
                        geometries.push(graphic.geometry);
                        return same;
                    });

                    // If geometries are all the same type, union all geometries in one
                    if (allSameType) {
                        var geometry;
                        if (geometries.length === 1) {
                            geometry = geometries[0];
                        } else if (geometries.length > 1) {
                            geometry = geometryEngine.union(geometries);
                        }
                        this._queryShowMetrics(geometry);
                    }
                    else {
                        // use the last geometry
                        this._queryShowMetrics(graphics[graphics.length - 1].geometry);
                    }
                }
                else {
                    this._showReportsDefaultView();
                }
            },

            /**
             * Query, calculate and show reports metrics for the specified geometry
             *
             * @param  {external:Geometry} inputGeometry
             */
            _queryShowMetrics: function (inputGeometry) {
                this._showBusy();

                //this.tabs.selectChild(this.tabResults, true);
                //this.domNode.scrollTop = 0;

                // Keep inputs and calculation results inside a scope variable
                var scope = {
                    input: {
                        input_geometry: inputGeometry,
                        not_available: i18n.widget.not_available
                    },
                    calc: {},
                    keep: ["not_available"]
                };

                // Calculations on inputs
                this._calculateInputsMetrics(scope);

                var promises = this._calculateLayersMetrics(scope);

                all(promises)
                    .then(lang.hitch(this, function () {
                        // Calculate metrics for each report in the config
                        this._calculateReportsMetrics(scope);

                        // Clear the report view
                        this._clearReports();

                        var result = {};
                        lang.mixin(result, scope.calc);
                        lang.mixin(result, scope.input);

                        // Show all reports in the report view
                        this._showReports(result);
                        this.centerAt(inputGeometry);
                    }))
                    .always(lang.hitch(this, function (callbackOrErrback) {
                        if (callbackOrErrback) {
                            console.log(callbackOrErrback);
                        }
                        this._hideBusy();
                    }));
            }
        }
    }
);
