define([
    "require",
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/on",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dojo/dom-construct",
    "dojox/widget/Standby"
], function (require, array, lang, declare, on, _WidgetBase, _TemplatedMixin, domConstruct, Standby) {

    return declare([_WidgetBase, _TemplatedMixin], {

        isBusy: false,

        _chartSelection: null,

        gauges: [],

        constructor: function (params, srcNodeRef) {
            //jshint unused:false
            if (typeof params.templateString !== "undefined") {
                this.templateString = params.templateString;
            }

            this.gauges = [];
        },

        destroy: function (preserveDom) {
            //jshint unused:false
        },

        postCreate: function () {
            this.inherited(arguments);

            this.handleBusyIndicator = new Standby({ target: this.domNode, duration: 500 });

            document.body.appendChild(this.handleBusyIndicator.domNode);

            this.handleBusyIndicator.startup();
        },

        showBusy: function () {
            if (!this.isBusy) {
                this.handleBusyIndicator.show();
                this.isBusy = true;
            }
        },

        hideBusy: function () {
            if (this.handleBusyIndicator) {
                if (this.isBusy) {
                    this.handleBusyIndicator.hide();
                    this.isBusy = false;
                }
            }
        },

        addChart: function (chart) {
            var hasData = chart.hasData();
            if (hasData) {
                require(["./charting/Selection"], lang.hitch(this, function (Selection) {
                    if (!this._chartSelection) {
                        this.chartSelectionNode = domConstruct.create("div", {}, chart.parentNode, "before");
                        this._chartSelection = new Selection({}, this.chartSelectionNode);
                    }
                    this._chartSelection.addChart(chart);
                }));
            }
        },

        addGauge: function (params) {
            require(["./gauges/SimpleRectangularGauge"], lang.hitch(this, function (SimpleRectangularGauge) {
                var node = domConstruct.create("div", {class: "metric-gauge"}, this.contentNode);
                var gauge = new SimpleRectangularGauge(params, node);
                gauge.startup();
                this.gauges.push(gauge);
            }));
        },

        clear: function () {
            // destroy gauges
            array.forEach(this.gauges, function (gauge) {
                gauge.destroy();
                gauge = null;
            });
            this.gauges = [];

            // destroy charts
            if (this._chartSelection) {
                this._chartSelection.clear();
            }

            // destroy ContentNode childs
            while (this.contentNode.lastChild) {
                this.contentNode.removeChild(this.contentNode.lastChild);
            }
        },

        clean: function () {
            // If there is no chart
            if (this._chartSelection && this._chartSelection.count === 0) {
                this._chartSelection.destroyRecursive(false);
                if (this.chartSelectionNode) {
                    domConstruct.destroy(this.chartSelectionNode);
                }
                this._chartSelection = null;
            }
        },

        uniformizeCharts: function () {
            if (this._chartSelection) {
                this._chartSelection.uniformizeCharts();
            }
        }
    });
});
