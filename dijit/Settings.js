define([
    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/Evented",
    "dojo/on",
    "dojo/string",
    "dijit/_WidgetBase",
    "dojo/i18n!./nls/settings",
], function (array, lang, declare, domConstruct, Evented, on, string, _WidgetBase, i18nSettings) {

    return declare([Evented, _WidgetBase], {
        config: null,
        _selects: null,
        _sliders: null,
        replace: {},
        transform: null,

        _slidersValues: null,
        _selectsValues: null,

        constructor: function (params, srcNodeRef) {
            // jshint unused: false
            this.config = params.config;

            if (params.replace) {
                lang.mixin(this.replace, params.replace);
            }

            if (params.transform) {
                this.transform = params.transform;
            }

            this._selects = [];
            this._sliders = [];
            this._slidersValues = {};
            this._selectsValues = {};
        },

        postCreate: function () {
            this.inherited(arguments);

            this._initOptions();
        },

        _mergeOptions: function (data) {
            // Create more options with options values
            var hasMerge = typeof this.config.merge !== "undefined";
            if (hasMerge) {
                for (var mergeName in this.config.merge) {
                    if (this.config.merge.hasOwnProperty(mergeName)) {
                        var mergeValue = this.config.merge[mergeName];
                        switch (typeof mergeValue) {
                            case "string":
                                data[mergeName] = string.substitute(mergeValue, { options: data });
                                break;

                            case "object":
                                var newValue = JSON.stringify(mergeValue);
                                newValue = string.substitute(newValue, { options: data });
                                data[mergeName] = JSON.parse(newValue);
                                break;

                            default:
                                // Do Nothing
                                break;
                        }
                    }
                }
            }
        },

        _substituteOptions: function (data) {
            // Add ${} around the option value to escape double string.substitute errors
            var hasSubstitute = typeof this.config.substitute !== "undefined";
            if (hasSubstitute) {
                array.forEach(this.config.substitute, function (substitute) {
                    for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                            if (key === substitute) {
                                data[key] = "${" + data[key] + "}";
                            }
                        }
                    }
                });
            }
        },

        _getOptionsSelect: function (option, data) {
            var optionValue = this._selectsValues[option.id];
            if (typeof optionValue !== "undefined") {
                var options = option.items[optionValue].data;
                options = JSON.stringify(options);
                options = string.substitute(options, this.replace);
                options = JSON.parse(options);
                lang.mixin(data, options);
            }
        },

        getOptions: function () {
            var data = {};
            var hasOptions = typeof this.config !== "undefined";
            if (hasOptions) {
                array.forEach(this.config.options, lang.hitch(this, function (option, idx) {
                    switch (option.type) {
                        case "select":
                            this._getOptionsSelect(option, data);
                            break;
                        case "slider":
                            data[option.name] = this._slidersValues[idx];
                            break;
                        default:
                            // Nothing to do, option type not supported
                            break;
                    }
                }));

                if (this.transform) {
                    this.transform(data);
                }

                this._mergeOptions(data);

                this._substituteOptions(data);
            }


            return data;
        },

        /**
         * Add inputs in the options node based on config file
         *
         */
        _initOptions: function () {
            var hasOptions = this.config;
            if (hasOptions) {
                // Add a reset button in the options tab if specified in the config
                this._initResetButton();

                // Add a refresh button in the options tab if specified in the config
                this._initRefeshButton();

                // For each option in config
                array.forEach(this.config.options, lang.hitch(this, function (option, idx) {
                    switch (option.type) {
                        case "select":
                            this._createOptionSelect(option);
                            break;
                        case "slider":
                            this._createOptionSlider(option, idx);
                            break;
                        default:
                            break;
                    }
                }));
            }
        },

        /**
         * Add a reset button in the options tab if specified in the config
         *
         */
        _initResetButton: function () {
            // has reset
            var hasReset = this.config.reset;
            if (hasReset) {
                var title = i18nSettings.reset_title;
                var label = i18nSettings.reset;

                // button btn btn-default btn-sm : Wet3 & Wet4 classes
                var button = domConstruct.create("button", {
                    class: "button btn btn-default btn-sm",
                    innerHTML: label,
                    title: title
                }, this.domNode, "first");

                this.own(on(button, "click", lang.hitch(this, function () {
                    this._reset();
                    this.emit("reset", {});
                })));
            }
        },


        /**
        * Add a refresh button in the options tab if specified in the config
        *
        */
        _initRefeshButton: function () {
            // has refresh
            var hasRefresh = this.config.refresh;
            if (hasRefresh) {
                var title = i18nSettings.refresh_title;
                var label = i18nSettings.refresh;

                // button btn btn-default btn-sm : Wet3 & Wet4 classes
                var button = domConstruct.create("button", {
                    class: "button btn btn-default btn-sm metrics-options-refresh",
                    innerHTML: label,
                    title: title
                }, this.domNode, "first");

                this.own(on(button, "click", lang.hitch(this, function () {
                    this.emit("refresh", {});
                })));
            }
        },

        /**
         * Create a option select element in the options tab with its label
         *
         * @param  {Object} option The option item in the config file
         */
        _createOptionSelect: function (option) {
            // Create the label
            var label = string.substitute(option.label, this.replace);
            domConstruct.create("label", { innerHTML: label }, this.domNode, "last");

            var attributesOptions = { "class": "form-control" };
            var select = domConstruct.create("select", attributesOptions, this.domNode, "last");
            array.forEach(option.items, lang.hitch(this, function (item, idx) {
                var label = string.substitute(item.label, this.replace);
                var attributesOption = { innerHTML: label, value: idx };
                domConstruct.create("option", attributesOption, select, "last");
            }));

            if (option.items && option.items.length > 0) {
                this._selectsValues[option.id] = 0;
            }

            this.own(on(select, "change", lang.hitch(this, function () {
                this._selectsValues[option.id] = parseInt(select.value);
                this.emit("change", { option: option });
            })));

            this._selects.push({ select: select, option: option });
        },

        _createOptionSlider: function (option, idx) {
            // Create the label
            var label = string.substitute(option.label, this.replace);
            domConstruct.create("label", { innerHTML: label }, this.domNode, "last");

            var attributesOptions = { "data-input-option-id": option.id };
            var nodeParent = domConstruct.create("div", attributesOptions, this.domNode, "last");
            var node = domConstruct.create("table", attributesOptions, nodeParent, "last");

            var labels = [option.minimum, option.maximum];

            if (typeof option.increment !== "undefined") {
                var i = option.minimum;
                labels = [];
                while (i <= option.maximum) {
                    labels.push(i);
                    i += option.increment;
                }
            }

            var value = 1;
            if (typeof option.value === "number") {
                value = option.value;
            }
            value = value || 1;
            this._slidersValues[idx] = value;

            require(["esri/dijit/HorizontalSlider"], lang.hitch(this, function (HorizontalSlider) {
                var slider = new HorizontalSlider({
                    value: value,
                    labels: labels,
                    minimum: option.minimum,
                    maximum: option.maximum,
                    discreteValues: labels.length,
                    style: "width:90%; margin-bottom: 15px;",
                    onChange: lang.hitch(this, function (value) {
                        //jshint unused: false
                        this._slidersValues[idx] = value;
                        this.emit("change", { option: option });
                    })
                }, node);

                slider.startup();

                this._sliders.push({ slider: slider, option: option });
                this.own(slider);
            }));
        },

        _reset: function () {
            // Reset each select options to the first one
            array.forEach(this._selects, lang.hitch(this, function (item) {
                var select = item.select;
                if (select.length > 0) {
                    select.selectedIndex = 0;
                    this.emit("change", { option: item.option });
                }
            }));
            // Reset each slide options to the first one
            array.forEach(this._sliders, lang.hitch(this, function (item) {
                var slider = item.slider;
                var option = item.option;
                var value = 1;
                if (typeof option.value === "number") {
                    value = option.value;
                }
                value = value || 1;
                slider.set("value", value);
                this.emit("change", { option: item.option });
            }));
        }

    });

});
