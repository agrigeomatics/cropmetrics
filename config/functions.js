define([
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/number",
    "dojo/date/locale",
    "esri/geometry/geometryEngine",
    "esri/geometry/jsonUtils",
    "dojox/math/stats"
], function (lang, array, numberUtils, dateLocale, geometryEngine, jsonUtils, stats) {

    function storeQuery(store, query, options) {
        var result = [];
        if (store) {
            result = store.query(query, options);
        }
        return result;
    }

    function toNumber(value) {
        if (typeof value === "string") {
            value = value.replace(",", "");
        }
        return Number(value);
    }

    var stats2 = {
        max: function (items) {
            var arrayNumbers = array.map(items, toNumber);
            return stats.max(arrayNumbers);
        },

        sum: function (items) {
            var arrayNumbers = array.map(items, toNumber);
            return stats.sum(arrayNumbers);
        },

        mean: function (items) {
            var arrayNumbers = array.map(items, toNumber);
            return stats.mean(arrayNumbers);
        },

        sd: function (items) {
            var arrayNumbers = array.map(items, toNumber);
            return stats.sd(arrayNumbers);
        },

        dotMultiply: function(a, b) {
            if (a.length !== b.length) {
                throw "dotMultiply: array must have same length.";
            }
            return array.map(a, function(x, idx) {
                return toNumber(x) * toNumber(b[idx]);
            });
        },

        dotDivide: function(a, b) {
            var bNumber = toNumber(b);
            return array.map(a, function(x) {
                return toNumber(x) / bNumber;
            });
        }
    };



    function input(scope) {
        scope.input_geometry_type = scope.input_geometry.type;
        scope.input_geometry_area = geometryEngine.planarArea(scope.input_geometry, "square-kilometers");
        scope.stress_index_light = 0.25;
        scope.stress_index_moderate = 0.4;
        scope.stress_index_severe = 0.6;
        scope.input_year = 2015;
    }

    function droughtSelect(scope) {
        if (scope.input_geometry_type === "point") {
            scope.geometry_report = scope.input_geometry;
            scope.drought_geometry_area = 1;
        }
        else if (scope.input_geometry_type === "polygon") {
            scope.geometry_report = geometryEngine.intersect(scope.geometry, scope.input_geometry);
            scope.drought_geometry_area = geometryEngine.planarArea(scope.geometry_report, "square-kilometers");
        }
    }

    function applyMaskPolygonsWithSamples(scope, maskStoreName, samplesName, sampleGeometryName, polygonsFeatures, geometryName) {

        if (typeof geometryName === "undefined") {
            geometryName = "geometry";
        }

        var maskSamplesArray = storeQuery(scope[maskStoreName], {});

        if (typeof polygonsFeatures !== "undefined" && polygonsFeatures.length > 0) {
            array.forEach(polygonsFeatures, function (feature) {
                feature.sampleCount = 0;
            });

            var isPoint = scope.input_geometry_type === "point";
            var isPolygon = scope.input_geometry_type === "polygon";
            var isExtent = scope.input_geometry_type === "extent";

            scope[samplesName] = [];

            array.forEach(maskSamplesArray, function (sample) {
                array.some(polygonsFeatures, function (feature) {
                    var intersects;
                    if (isPoint) {
                        intersects = sample.value !== "NoData";
                    }
                    else if (feature[geometryName] && (isPolygon || isExtent)) {
                        intersects = geometryEngine.intersects(feature[geometryName], sample.geometry);
                    }
                    if (intersects) {
                        feature.sampleCount += 1;
                        var sampleData = {};
                        lang.mixin(sampleData, feature);
                        lang.mixin(sampleData, sample);
                        scope[samplesName].push(sampleData);
                    }
                    return intersects;
                });
            });

            polygonsFeatures = array.filter(polygonsFeatures, function (item) {
                return item.sampleCount > 0;
            });

            scope[sampleGeometryName] = array.map(scope[samplesName], function (item) {
                return item.geometry;
            });
        }
        return polygonsFeatures;
    }

    function droughtPoint(scope) {
        var optionsQuery = { "sort": [{ "attribute": "DM" }] };
        scope.drought_array = storeQuery(scope.drought_store, {}, optionsQuery);
        scope.getDroughtArea = function (item) { return item.drought_geometry_area; };
        scope.getDroughtGeo = function (item) { return item.geometry_report; };
        scope.getDroughtDm = function (item) { return item.DM; };
        if (scope.drought_array.length === 0) {
            scope.drought_intensity_report = -1;
            scope.no_drought = {
                id: scope.drought_array.length,
                DM: -1,
                drought_geometry_area: 1,
                geometry_report: scope.input_geometry
            };
            scope.drought_store.put(scope.no_drought);
            scope.drought_array = storeQuery(scope.drought_store, {});
            scope.drought_area_select = array.map(scope.drought_array, scope.getDroughtArea);
        }

        scope.drought_array = applyMaskPolygonsWithSamples(scope, "drought_mask_store", "drought_samples", "drought_samples_geo", scope.drought_array, "geometry_report");

        if (scope.drought_array.length > 0) {
            scope.drought_dm_select = array.map(scope.drought_array, scope.getDroughtDm);
            scope.drought_max_dm = stats2.max(scope.drought_dm_select);
            scope.drought_intensity_report = scope.drought_max_dm;
            scope.drought_area_select = array.map(scope.drought_array, scope.getDroughtArea);
        }
        else {
            scope.drought_intensity_report = scope.not_available;
        }
    }

    function droughtPolygon(scope) {
        var optionsQuery = { "sort": [{ "attribute": "DM" }] };
        scope.drought_array = storeQuery(scope.drought_store, {}, optionsQuery);
        scope.getDroughtArea = function (item) { return item.drought_geometry_area; };
        scope.getDroughtGeo = function (item) { return item.geometry_report; };
        scope.getDroughtDm = function (item) { return item.DM; };
        if (scope.drought_array.length === 0) {
            scope.no_drought = {
                id: scope.drought_array.length,
                DM: -1,
                drought_geometry_area: scope.input_geometry_area,
                geometry_report: scope.input_geometry
            };
            scope.drought_store.put(scope.no_drought);
            scope.drought_array = storeQuery(scope.drought_store, {}, optionsQuery);
            scope.drought_area_select = array.map(scope.drought_array, scope.getDroughtArea);
        }
        else {
            scope.drought_area_select = array.map(scope.drought_array, scope.getDroughtArea);
            scope.drought_sum_area = stats2.sum(scope.drought_area_select);
            if (scope.drought_sum_area < scope.input_geometry_area) {
                scope.drought_geometry_select = array.map(scope.drought_array, scope.getDroughtGeo);
                scope.drought_union = geometryEngine.union(scope.drought_geometry_select);
                scope.no_drought_geometry = geometryEngine.difference(scope.input_geometry, scope.drought_union);

                if (scope.no_drought_geometry) {
                    scope.no_drought_area = geometryEngine.planarArea(scope.no_drought_geometry, "square-kilometers");
                    scope.no_drought = {
                        id: scope.drought_array.length,
                        DM: -1,
                        drought_geometry_area: scope.no_drought_area,
                        geometry_report: scope.no_drought_geometry
                    };
                    scope.drought_store.put(scope.no_drought);
                    scope.drought_array = storeQuery(scope.drought_store, {}, optionsQuery);
                }
            }
        }

        scope.drought_array = applyMaskPolygonsWithSamples(scope, "drought_mask_store", "drought_samples", "drought_samples_geo", scope.drought_array, "geometry_report");

        if (scope.drought_array.length > 0) {
            scope.drought_max_area = stats2.max(scope.drought_area_select);
            scope.drought_max_area_array = storeQuery(scope.drought_store, { "drought_geometry_area": scope.drought_max_area });
            scope.drought_intensity_report = scope.drought_max_area_array[0].DM;
        }
        else {
            scope.drought_intensity_report = scope.not_available;
        }
    }

    function drought(scope) {
        if (scope.input_geometry_type === "point") {
            droughtPoint(scope);
        }
        else if (scope.input_geometry_type === "polygon") {
            droughtPolygon(scope);
        }
    }

    function normalizeDifferenceMean(scope, valueAttribute, selectionAttribute, newAttribute) {
        var numberValue = toNumber(scope[valueAttribute]);
        var meanValue = stats2.mean(scope[selectionAttribute]);
        var stdValue = stats2.sd(scope[selectionAttribute]);
        if (stdValue === 0) {
            // Cannot divide by zero...
            scope[newAttribute] = numberValue;
        }
        else {
            scope[newAttribute] = (numberValue - meanValue) / stdValue;
        }
    }

    function chuCsvRelateSelect(scope) {
        normalizeDifferenceMean(scope, "CHU_Acc", "chu_selection", "chu_trend");
    }

    function gdd5CsvRelateSelect(scope) {
        normalizeDifferenceMean(scope, "GDD5_Acc", "gdd5_selection", "gdd5_trend");
    }

    function precipCsvRelateSelect(scope) {
        normalizeDifferenceMean(scope, "Pcpn_Acc", "precip_selection", "precip_acc_trend");
    }

    function cropStageCsvRelateSelect(scope) {
        normalizeDifferenceMean(scope, "CropStage", "crop_stage_selection", "crop_stage_trend");
    }

    function classifyNormalizedTrendSimple(scope, attributeValue, newAttribute) {
        if (scope[attributeValue] <= -1) { scope[newAttribute] = "1"; } else
            if (scope[attributeValue] > -1 && scope[attributeValue] <= -0.25) { scope[newAttribute] = "1"; } else
                if (scope[attributeValue] > -0.25 && scope[attributeValue] <= 0.25) { scope[newAttribute] = "2"; } else
                    if (scope[attributeValue] > 0.25 && scope[attributeValue] <= 1) { scope[newAttribute] = "3"; } else
                        if (scope[attributeValue] > 1) { scope[newAttribute] = "3"; }
    }

    function classifyNormalizedTrend(scope, attributeValue, newAttribute) {
        if (scope[attributeValue] <= -1) { scope[newAttribute] = "1"; } else
            if (scope[attributeValue] > -1 && scope[attributeValue] <= -0.25) { scope[newAttribute] = "2"; } else
                if (scope[attributeValue] > -0.25 && scope[attributeValue] <= 0.25) { scope[newAttribute] = "3"; } else
                    if (scope[attributeValue] > 0.25 && scope[attributeValue] <= 1) { scope[newAttribute] = "4"; } else
                        if (scope[attributeValue] > 1) { scope[newAttribute] = "5"; }
    }

    function classifySiIndex(scope, attributeValue, newAttribute) {
        if (scope[attributeValue] < 0.25) { scope[newAttribute] = "1"; } else
            if (scope[attributeValue] >= 0.25 && scope[attributeValue] < 0.4) { scope[newAttribute] = "2"; } else
                if (scope[attributeValue] >= 0.4 && scope[attributeValue] < 0.6) { scope[newAttribute] = "3"; } else
                    if (scope[attributeValue] >= 0.6) { scope[newAttribute] = "4"; }
    }

    function classifyCropStage(scope, attributeValue, newAttribute) {
        if (scope[attributeValue] < 1) { scope[newAttribute] = "0"; } else
            if (scope[attributeValue] >= 1 && scope[attributeValue] < 2) { scope[newAttribute] = "1"; } else
                if (scope[attributeValue] >= 2 && scope[attributeValue] < 3) { scope[newAttribute] = "1"; } else
                    if (scope[attributeValue] >= 3 && scope[attributeValue] < 4) { scope[newAttribute] = "2"; } else
                        if (scope[attributeValue] >= 4 && scope[attributeValue] < 5) { scope[newAttribute] = "3"; } else
                            if (scope[attributeValue] >= 5) { scope[newAttribute] = "3"; }
    }

    function classifyGrasshopper(scope, attributeValue, newAttribute) {
        if (scope[attributeValue] < 2) { scope[newAttribute] = "1"; } else
            if (scope[attributeValue] >= 2 && scope[attributeValue] < 4) { scope[newAttribute] = "2"; } else
                if (scope[attributeValue] >= 4 && scope[attributeValue] < 8) { scope[newAttribute] = "3"; } else
                    if (scope[attributeValue] >= 8 && scope[attributeValue] < 12) { scope[newAttribute] = "4"; } else
                        if (scope[attributeValue] >= 12 && scope[attributeValue] < 24) { scope[newAttribute] = "5"; } else
                            if (scope[attributeValue] >= 24) { scope[newAttribute] = "6"; } else {
                                scope[newAttribute] = "-1";
                            }
    }

    function classifyNdvi(scope, attributeValue, newAttribute) {
        if (scope[attributeValue] < -0.5) { scope[newAttribute] = "0"; } else
            if (scope[attributeValue] >= -0.5 && scope[attributeValue] < -0.2) { scope[newAttribute] = "1"; } else
                if (scope[attributeValue] >= -0.2 && scope[attributeValue] < 0.2) { scope[newAttribute] = "2"; } else
                    if (scope[attributeValue] >= 0.2 && scope[attributeValue] < 0.5) { scope[newAttribute] = "3"; } else
                        if (scope[attributeValue] >= 0.5) { scope[newAttribute] = "4"; } else {
                            scope[newAttribute] = "-1";
                        }
    }

    function classifyPrecipitation(scope, attributeValue, newAttribute) {
        if (scope[attributeValue] < 0.1) { scope[newAttribute] = "1"; } else
            if (scope[attributeValue] >= 0.1 && scope[attributeValue] < 0.25) { scope[newAttribute] = "2"; } else
                if (scope[attributeValue] >= 0.25 && scope[attributeValue] < 0.5) { scope[newAttribute] = "3"; } else
                    if (scope[attributeValue] >= 0.5 && scope[attributeValue] < 1) { scope[newAttribute] = "4"; } else
                        if (scope[attributeValue] >= 1 && scope[attributeValue] < 2.5) { scope[newAttribute] = "5"; } else
                            if (scope[attributeValue] >= 2.5 && scope[attributeValue] < 5) { scope[newAttribute] = "6"; } else
                                if (scope[attributeValue] >= 5 && scope[attributeValue] < 7.5) { scope[newAttribute] = "7"; } else
                                    if (scope[attributeValue] >= 7.5 && scope[attributeValue] < 10) { scope[newAttribute] = "8"; } else
                                        if (scope[attributeValue] >= 10 && scope[attributeValue] < 15) { scope[newAttribute] = "9"; } else
                                            if (scope[attributeValue] >= 15 && scope[attributeValue] < 20) { scope[newAttribute] = "10"; } else
                                                if (scope[attributeValue] >= 20 && scope[attributeValue] < 25) { scope[newAttribute] = "11"; } else
                                                    if (scope[attributeValue] >= 25 && scope[attributeValue] < 50) { scope[newAttribute] = "12"; } else
                                                        if (scope[attributeValue] >= 50 && scope[attributeValue] < 100) { scope[newAttribute] = "13"; } else
                                                            if (scope[attributeValue] >= 100) { scope[newAttribute] = "14"; }
    }

    function classifyTemperature(scope, attributeValue, newAttribute) {
        if (scope[attributeValue] < -15) { scope[newAttribute] = "1"; } else
            if (scope[attributeValue] >= -15 && scope[attributeValue] < -10) { scope[newAttribute] = "2"; } else
                if (scope[attributeValue] >= -10 && scope[attributeValue] < -5) { scope[newAttribute] = "3"; } else
                    if (scope[attributeValue] >= -5 && scope[attributeValue] < -0) { scope[newAttribute] = "4"; } else
                        if (scope[attributeValue] >= 0 && scope[attributeValue] < 5) { scope[newAttribute] = "5"; } else
                            if (scope[attributeValue] >= 5 && scope[attributeValue] < 10) { scope[newAttribute] = "6"; } else
                                if (scope[attributeValue] >= 10 && scope[attributeValue] < 15) { scope[newAttribute] = "7"; } else
                                    if (scope[attributeValue] >= 15 && scope[attributeValue] < 20) { scope[newAttribute] = "8"; } else
                                        if (scope[attributeValue] >= 20 && scope[attributeValue] < 25) { scope[newAttribute] = "9"; } else
                                            if (scope[attributeValue] >= 25 && scope[attributeValue] < 30) { scope[newAttribute] = "10"; } else
                                                if (scope[attributeValue] >= 30) { scope[newAttribute] = "11"; }
    }

    function csvStationsSelect(scope) {
        scope.gdd5_current = toNumber(scope.GDD5_Acc);
        scope.si_current = toNumber(scope.SI);

        if (scope.si_current < 0) {
            scope.si_current = 0;
        }

        scope.crop_stage_current = toNumber(scope.CropStage);
        scope.precip_acc_current = toNumber(scope.Pcpn_Acc);

        scope.gdd5_trend = toNumber(scope.gdd5_trend);
        classifyNormalizedTrend(scope, "gdd5_trend", "class_value_heat_trend");

        scope.precip_acc_trend = toNumber(scope.precip_acc_trend);
        classifyNormalizedTrend(scope, "precip_acc_trend", "class_value_precip_acc_trend");

        scope.crop_stage_trend = toNumber(scope.crop_stage_trend);
        classifyNormalizedTrendSimple(scope, "crop_stage_trend", "class_value_crop_stage_trend");

        classifySiIndex(scope, "si_current", "class_value_si");
        classifyCropStage(scope, "crop_stage_current", "class_value_crop_stage");
    }

    function csvStationsCHUSelect(scope) {
        scope.chu_current = toNumber(scope.CHU_Acc);
        scope.si_current = toNumber(scope.SI);

        if (scope.si_current < 0) {
            scope.si_current = 0;
        }

        scope.crop_stage_current = toNumber(scope.CropStage);
        scope.precip_acc_current = toNumber(scope.Pcpn_Acc);

        scope.chu_trend = toNumber(scope.chu_trend);
        classifyNormalizedTrend(scope, "chu_trend", "class_value_heat_trend");

        scope.precip_acc_trend = toNumber(scope.precip_acc_trend);
        classifyNormalizedTrend(scope, "precip_acc_trend", "class_value_precip_acc_trend");

        scope.crop_stage_trend = toNumber(scope.crop_stage_trend);
        classifyNormalizedTrendSimple(scope, "crop_stage_trend", "class_value_crop_stage_trend");

        classifySiIndex(scope, "si_current", "class_value_si");
        classifyCropStage(scope, "crop_stage_current", "class_value_crop_stage");
    }

    function dateFromDay(year, day) {
        var date = new Date(year, 0); // initialize a date in `year-01-01`
        return new Date(date.setDate(day)); // add the number of days
    }

    function csvStations30Year(scope) {
        if (scope.input_geometry_type === "point" && scope.MASK === "NoData") {
            // Nothing to add
        }
        else {
            var formatDate = function (item) {
                var dateItem = dateFromDay(scope.input_year, item.dateLabel);
                item.dateLabel = dateLocale.format(dateItem, {
                    selector: 'date',
                    datePattern: "M-dd"
                });
                return item;
            };

            var isNumber = function (item) { return !isNaN(item.P_acc_avg) && item.P_acc_avg > 0; };
            scope.pcpn_acc_30_year_array = storeQuery(scope["store30yearPAccAvg"], isNumber);
            scope.pcpn_acc_30_year_array = array.map(scope.pcpn_acc_30_year_array, formatDate);

            isNumber = function (item) { return !isNaN(item.heat_acc_avg) && item.heat_acc_avg > 0; };
            scope.heat_acc_30_year_array = storeQuery(scope["store30yearHeatAvg"], isNumber);
            scope.heat_acc_30_year_array = array.map(scope.heat_acc_30_year_array, formatDate);
        }
    }

    function csvStations30YearSelect(scope) {
        if (typeof scope.P_acc_avg !== "undefined") {
            scope.P_acc_avg = toNumber(scope.P_acc_avg);
        }
        if (typeof scope.GDD5_acc_avg !== "undefined") {
            scope.heat_acc_avg = toNumber(scope.GDD5_acc_avg);
        }
        if (typeof scope.CHU_acc_avg !== "undefined") {
            scope.heat_acc_avg = toNumber(scope.CHU_acc_avg);
        }
    }

    function csvStationsHistoricSelect(scope) {
        scope.SI = toNumber(scope.SI);
        if (scope.SI < 0) {
            scope.SI = 0;
        }
        scope.Pcpn_Acc = toNumber(scope.Pcpn_Acc);
        if (scope.Pcpn_Acc < 0) {
            scope.Pcpn_Acc = 0;
        }
        if (typeof scope.GDD5_Acc !== "undefined") {
            scope.heat_acc = toNumber(scope.GDD5_Acc);
        }
        if (typeof scope.CHU_Acc !== "undefined") {
            scope.heat_acc = toNumber(scope.CHU_Acc);
        }
    }

    function precipitationsSelect(scope) {
        scope.number_value = toNumber(scope.value);
        classifyPrecipitation(scope, "number_value", "class_value");
    }

    function temperatureSelect(scope) {
        scope.number_value = toNumber(scope.value);
        scope.temperature_celcius = scope.number_value;
        classifyTemperature(scope, "temperature_celcius", "class_value");
    }

    function grasshopperSelect(scope) {
        scope.number_value = toNumber(scope.value);
        classifyGrasshopper(scope, "number_value", "class_value");
    }

    function ndviSelect(scope) {
        if (scope.input_geometry_type === "point") {
            scope.geometry = scope.input_geometry;
        }
        else if (scope.location) {
            scope.geometry = jsonUtils.fromJson(scope.location);
        }
        scope.number_value = toNumber(scope.value);
        classifyNdvi(scope, "number_value", "class_value");
    }

    function getArrayValueAndMean(scope, attributeValue, storeName, newArrayName,
        newMeanName, classifyName, classifyFunction) {
        var isNumber = function (item) { return !isNaN(item[attributeValue]); };
        scope[newArrayName] = storeQuery(scope[storeName], isNumber);
        var getValue = function (item) { return item[attributeValue]; };
        var selection = array.map(scope[newArrayName], getValue);
        if (selection.length > 0) {
            if (selection.length === 1) {
                scope[newMeanName] = selection[0];
            }
            else {
                scope[newMeanName] = stats2.mean(selection);
            }

            if (typeof classifyName !== "undefined" && typeof classifyFunction !== "undefined") {
                classifyFunction(scope, newMeanName, classifyName);
            }
        }
        else {
            scope[newMeanName] = scope.not_available;
            if (typeof classifyName !== "undefined" && typeof classifyFunction !== "undefined") {
                scope[classifyName] = 0;
            }
        }
    }

    function csvStationsHistoric(scope) {
        if (scope.input_geometry_type === "point" && scope.MASK === "NoData") {
            // Nothing to add
        }
        else {
            var isNumber = function (item) { return !isNaN(item.SI); };
            scope.si_historic_array = storeQuery(scope["storeSiHistoric"], isNumber);
            isNumber = function (item) { return !isNaN(item.Pcpn_Acc); };
            scope.pcpn_acc_historic_array = storeQuery(scope["storePcpnAcc"], isNumber);
            isNumber = function (item) { return !isNaN(item.heat_acc); };
            scope.heat_historic_array = storeQuery(scope["storeHeatHistoric"], isNumber);
        }
    }

    function csvStations(scope) {
        if (scope.input_geometry_type === "point" && scope.MASK === "NoData") {
            scope.heat_acc_current_mean = scope.not_available;
            scope.heat_acc_historical_mean_class = scope.not_available;
            scope.precip_acc_historical_mean_class = scope.not_available;
            scope.si_current_mean = scope.not_available;
            scope.si_current_mean_class = scope.not_available;
            scope.precip_acc_current_mean = scope.not_available;
            scope.crop_stage_current_mean_class = scope.not_available;
            scope.crop_stage_historical_mean_class = scope.not_available;
        }
        else {
            getArrayValueAndMean(scope,
                "gdd5_current",
                "storeGdd5Current",
                "heat_current_array",
                "heat_acc_current_mean");

            getArrayValueAndMean(scope,
                "gdd5_trend",
                "storeGdd5Trend",
                "heat_historical_array",
                "heat_acc_historical_mean",
                "heat_acc_historical_mean_class",
                classifyNormalizedTrend);

            getArrayValueAndMean(scope,
                "precip_acc_trend",
                "storePrecipAccTrend",
                "precip_acc_historical_array",
                "precip_acc_historical_mean",
                "precip_acc_historical_mean_class",
                classifyNormalizedTrend);

            getArrayValueAndMean(scope,
                "crop_stage_trend",
                "storeCropStageTrend",
                "crop_stage_historical_array",
                "crop_stage_historical_mean",
                "crop_stage_historical_mean_class",
                classifyNormalizedTrendSimple);

            getArrayValueAndMean(scope,
                "si_current",
                "storeSiCurrent",
                "si_current_array",
                "si_current_mean",
                "si_current_mean_class",
                classifySiIndex);

            getArrayValueAndMean(scope,
                "crop_stage_current",
                "storeCropStage",
                "crop_stage_current_array",
                "crop_stage_current_mean",
                "crop_stage_current_mean_class",
                classifyCropStage);

            getArrayValueAndMean(scope,
                "precip_acc_current",
                "storePrecipAccCurrent",
                "precip_acc_current_array",
                "precip_acc_current_mean");
        }
    }

    function csvStationsCHU(scope) {
        if (scope.input_geometry_type === "point" && scope.MASK === "NoData") {
            scope.heat_acc_current_mean = scope.not_available;
            scope.heat_acc_historical_mean_class = scope.not_available;
            scope.precip_acc_historical_mean_class = scope.not_available;
            scope.si_current_mean = scope.not_available;
            scope.si_current_mean_class = scope.not_available;
            scope.precip_acc_current_mean = scope.not_available;
            scope.crop_stage_current_mean_class = scope.not_available;
            scope.crop_stage_historical_mean_class = scope.not_available;
        }
        else {
            getArrayValueAndMean(scope,
                "chu_current",
                "storeChu5Current",
                "heat_current_array",
                "heat_acc_current_mean");

            getArrayValueAndMean(scope,
                "chu_trend",
                "storeChuTrend",
                "heat_historical_array",
                "heat_acc_historical_mean",
                "heat_acc_historical_mean_class",
                classifyNormalizedTrend);

            getArrayValueAndMean(scope,
                "precip_acc_trend",
                "storePrecipAccTrend",
                "precip_acc_historical_array",
                "precip_acc_historical_mean",
                "precip_acc_historical_mean_class",
                classifyNormalizedTrend);

            getArrayValueAndMean(scope,
                "crop_stage_trend",
                "storeCropStageTrend",
                "crop_stage_historical_array",
                "crop_stage_historical_mean",
                "crop_stage_historical_mean_class",
                classifyNormalizedTrendSimple);

            getArrayValueAndMean(scope,
                "si_current",
                "storeSiCurrent",
                "si_current_array",
                "si_current_mean",
                "si_current_mean_class",
                classifySiIndex);

            getArrayValueAndMean(scope,
                "crop_stage_current",
                "storeCropStage",
                "crop_stage_current_array",
                "crop_stage_current_mean",
                "crop_stage_current_mean_class",
                classifyCropStage);

            getArrayValueAndMean(scope,
                "precip_acc_current",
                "storePrecipAccCurrent",
                "precip_acc_current_array",
                "precip_acc_current_mean");
        }
    }

    function precipitations(scope) {
        if (scope.input_geometry_type === "point" && scope.MASK === "NoData") {
            scope.precipitation_current = scope.not_available;
        } else {
            getArrayValueAndMean(scope,
                "number_value",
                "store",
                "precipitation_current_array",
                "precipitation_current");
        }
    }

    function temperature(scope) {
        if (scope.input_geometry_type === "point" && scope.MASK === "NoData") {
            scope.temperature_current = scope.not_available;
        } else {
            getArrayValueAndMean(scope,
                "temperature_celcius",
                "store",
                "temperature_current_array",
                "temperature_current");
        }
    }

    function grasshopper(scope) {
        getArrayValueAndMean(scope,
            "number_value",
            "store",
            "array_values",
            "selection_mean",
            "grasshopper_class",
            classifyGrasshopper);
    }

    function ndvi(scope) {
        getArrayValueAndMean(scope,
            "number_value",
            "ndvi_store",
            "array_values",
            "ndvi_mean",
            "ndvi_class",
            classifyNdvi);

        scope.ndvi_array = storeQuery(scope.ndvi_store, {});
    }

    function tooltipDrought(text) {
        var formated = numberUtils.format(text, {
            places: 0
        });
        return formated + ' km²';
    }

    function tooltipPrecip(text) {
        var formated = numberUtils.format(text, {
            places: 1
        });
        return formated + ' mm';
    }

    function maskSelect(scope) {
        if (scope.location) {
            scope.geometry = jsonUtils.fromJson(scope.location);
        }
    }

    function joinRelate(features, relateStores, joinAttribute) {

        array.forEach(features, lang.hitch(this, function (feature) {
            var id = feature[joinAttribute];
            if (typeof relateStores[id] !== "undefined") {
                if (relateStores[id].data.length === 1) {
                    var relateData = storeQuery(relateStores[id], {});
                    lang.mixin(feature, relateData[0]);
                }
            }
        }));

    }

    function preparePonderateRemoveNull(element, areas) {
        var items = [];
        var ponderation = [];
        var idx = 0;
        for (var key in element) {
            if (element.hasOwnProperty(key)) {
                if (element[key] !== null) {
                    items.push(element[key]);
                    ponderation.push(areas[idx]);
                }
            }
            idx += 1;
        }

        var sum = stats2.sum(ponderation);
        ponderation = stats2.dotDivide(ponderation, sum);
        return {
            items: items,
            ponderation: ponderation
        };
    }

    function yieldImperialToMetric(crop, cropYield) {
        var converted = cropYield;
        switch (crop) {
            case "spring wheat":
                converted = cropYield * 0.067250435;
                break;
            case "canola":
                converted = cropYield * 0.056043046;
                break;
            case "oats":
                converted = cropYield * 0.038108788;
                break;
            case "barley":
                converted = cropYield * 0.053800348;
                break;
            case "soybeans":
                converted = cropYield * 0.067250435;
                break;
            case "dutwht":
                converted = cropYield * 0.067250435;
                break;
            case "corn":
                converted = cropYield * 0.062767984;
                break;
            default:
                break;
        }
        return converted;
    }

    function historicYield(scope, features, relateStores, joinAttribute) {
        scope.yield_array = [];
        var conditionYield = function (item) {
            return !isNaN(item.Yield) &&
                item.Yield > 0 &&
                item.YEAR < scope.option_date_day_month_year.Year;
        };

        var data = {};

        var uniqueKeys = [];
        var optionsQueryYield = { "sort": [{ "attribute": "YEAR" }] };

        array.forEach(features, lang.hitch(this, function (feature) {
            var id = feature[joinAttribute];
            if (typeof relateStores[id] !== "undefined") {
                var relateData = storeQuery(relateStores[id], conditionYield, optionsQueryYield);
                array.forEach(relateData, lang.hitch(this, function (relate) {
                    var year = relate["YEAR"];
                    if (uniqueKeys.indexOf(year) === -1) {
                        uniqueKeys.push(year);
                    }
                }));
            }
        }));

        array.forEach(features, lang.hitch(this, function (feature) {
            var id = feature[joinAttribute];
            if (typeof relateStores[id] !== "undefined") {
                var relateData = storeQuery(relateStores[id], conditionYield, optionsQueryYield);
                array.forEach(uniqueKeys, function (year) {
                    if (typeof data[year] === "undefined") {
                        data[year] = [];
                    }
                    var isFound = array.some(relateData, function (relate) {
                        var found = relate["YEAR"] === year;
                        if (found) {
                            data[year].push(relate["Yield"]);
                        }
                        return found;
                    });
                    if (!isFound) {
                        data[year].push(null);
                    }
                });
            }
        }));

        scope.yield_array = [];

        // Ponderate each yield by CAR area
        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                var element = data[key];
                var result = preparePonderateRemoveNull(element, scope.yield_area_select);

                var ponderate = stats2.dotMultiply(result.items, result.ponderation);
                var sumPonderate = stats2.sum(ponderate);

                sumPonderate = numberUtils.format(sumPonderate, {
                    places: 1
                });

                if (scope.option_yield_unit === "ton_hectare") {
                    sumPonderate = yieldImperialToMetric(scope.option_crop, sumPonderate);
                }

                var obj = {
                    YEAR: key,
                    Yield: sumPonderate
                };
                scope.yield_array.push(obj);
            }
        }
    }

    function yieldCar(scope) {

        //yield_store
        //"MEI", "YIELD_HIGH90", "YIELD_MEDIAN", "YIELD_LOW10"

        var carArray = storeQuery(scope.car_store, {});
        carArray = applyMaskPolygonsWithSamples(scope, "car_mask_store", "yield_samples", "yield_samples_geo", carArray);

        var getCarArea = function (item) { return item.sampleCount; };
        scope.yield_area_select = array.map(carArray, getCarArea);
        if (scope.yield_area_select.length > 0) {

            scope.sum_area_yield = stats2.sum(scope.yield_area_select);
            scope.pctg_area = stats2.dotDivide(scope.yield_area_select, scope.sum_area_yield);

            historicYield(scope, carArray, scope.historic_stores, "OBJECTID_1");

            // Join yield data to the car data
            joinRelate(carArray, scope.yield_stores, "OBJECTID_1");

            var getCarWorst = function (item) { return item.YIELD_LOW10; };
            scope.yield_worst_select = array.map(carArray, getCarWorst);
            scope.yield_worst_ponderate = stats2.dotMultiply(scope.yield_worst_select, scope.pctg_area);
            scope.yield_worst = stats2.sum(scope.yield_worst_ponderate);
            scope.yield_worst_no_unit = scope.yield_worst;
            if (scope.option_yield_unit === "ton_hectare") {
                scope.yield_worst = yieldImperialToMetric(scope.option_crop, scope.yield_worst);
                scope.yield_worst_no_unit = yieldImperialToMetric(scope.option_crop, scope.yield_worst_no_unit);
            }

            var getCarMost = function (item) { return item.YIELD_MEDIAN; };
            scope.yield_most_select = array.map(carArray, getCarMost);
            scope.yield_most_ponderate = stats2.dotMultiply(scope.yield_most_select, scope.pctg_area);
            scope.yield_most = stats2.sum(scope.yield_most_ponderate);
            scope.yield_most_no_unit = scope.yield_most;
            if (scope.option_yield_unit === "ton_hectare") {
                scope.yield_most = yieldImperialToMetric(scope.option_crop, scope.yield_most);
                scope.yield_most_no_unit = yieldImperialToMetric(scope.option_crop, scope.yield_most_no_unit);
            }

            var getCarBest = function (item) { return item.YIELD_HIGH90; };
            scope.yield_best_select = array.map(carArray, getCarBest);
            scope.yield_best_ponderate = stats2.dotMultiply(scope.yield_best_select, scope.pctg_area);
            scope.yield_best = stats2.sum(scope.yield_best_ponderate);
            scope.yield_best_no_unit = scope.yield_best;
            if (scope.option_yield_unit === "ton_hectare") {
                scope.yield_best = yieldImperialToMetric(scope.option_crop, scope.yield_best);
                scope.yield_best_no_unit = yieldImperialToMetric(scope.option_crop, scope.yield_best_no_unit);
            }

            var getCPI = function (item) { return item.CPI; };
            scope.cpi_select = array.map(carArray, getCPI);
            scope.cpi_ponderate = stats2.dotMultiply(scope.cpi_select, scope.pctg_area);
            scope.cpi_ponderate_sum = stats2.sum(scope.cpi_ponderate);

            classifyNormalizedTrend(scope, "cpi_ponderate_sum", "cpi_class");
        }
        else {
            scope.yield_worst = scope.not_available;
            scope.yield_most = scope.not_available;
            scope.yield_best = scope.not_available;
            scope.cpi_class = scope.not_available;
        }
    }

    function searchReplace(options, scope) {
        //Format overall outlook
        if (options.options.option_cpi_name) {
            classifyNormalizedTrend(scope, options.options.option_cpi_name, "cpi_class");
            scope[options.options.option_cpi_name] = options.i18nTemplate.cpi_class[scope.cpi_class];
        }
    }

    function ndviHistogram(scope) {
        if (scope.histograms) {
            scope.ndviHistogram = scope.histograms;
        }
    }

    return {
        input: input,
        droughtSelect: droughtSelect,
        drought: drought,

        chuCsvRelateSelect: chuCsvRelateSelect,
        gdd5CsvRelateSelect: gdd5CsvRelateSelect,
        precipCsvRelateSelect: precipCsvRelateSelect,
        cropStageCsvRelateSelect: cropStageCsvRelateSelect,

        csvStationsSelect: csvStationsSelect,
        csvStations: csvStations,

        csvStationsCHUSelect: csvStationsCHUSelect,
        csvStationsCHU: csvStationsCHU,

        precipitationsSelect: precipitationsSelect,
        precipitations: precipitations,

        temperatureSelect: temperatureSelect,
        temperature: temperature,

        grasshopperSelect: grasshopperSelect,
        grasshopper: grasshopper,

        ndviSelect: ndviSelect,
        ndvi: ndvi,

        maskSelect: maskSelect,
        yieldCar: yieldCar,

        csvStationsHistoric: csvStationsHistoric,
        csvStationsHistoricSelect: csvStationsHistoricSelect,

        csvStations30Year: csvStations30Year,
        csvStations30YearSelect: csvStations30YearSelect,

        tooltipDrought: tooltipDrought,
        tooltipPrecip: tooltipPrecip,

        searchReplace: searchReplace,
        ndviHistogram: ndviHistogram
    };

});
