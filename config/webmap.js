define({
   "item":{

   },
   "itemData":{
      "baseMap":{
         "title":"Default Basemap",
         "baseMapLayers":[
            {
               "id":"Light gray",
               "layerType":"ArcGISTiledMapServiceLayer",
               "opacity":0.5,
               "url":"http://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer"
            },
            {
               "id":"World Elevation",
               "layerType":"ArcGISImageServiceLayer",
               "opacity":0.50,
               "url":"http://elevation.arcgis.com/arcgis/rest/services/WorldElevation/Terrain/ImageServer"
            },
            {
                "id": "Basemap Labels",
                "layerType": "ArcGISTiledMapServiceLayer",
                "opacity": 1,
                "url": "http://geoappext.nrcan.gc.ca/arcgis/rest/services/BaseMaps/CBMT_TXT_3857/MapServer",
                "isReference": true
            }
         ]
      },
      "operationalLayers":[

      ]
   }
});
