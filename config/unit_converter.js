define([
    "dojo/date",
    "dojo/date/locale"
  ],function (date,locale) {

    //incoming value is bushels/acre
    //factors convert from this

    units = [
      {
        convertTo: "tonnes_ha",
        appliesTo: ["wheat","soybean","peas","beans"],
        factor: 0.067250435
      },
      {
        convertTo: "tonnes_ha",
        appliesTo: ["oats"],
        factor: 0.038108788
      },
      {
        convertTo: "tonnes_ha",
        appliesTo: ["barley","buckwheat"],
        factor: 0.053800348
      },
      {
        convertTo: "tonnes_ha",
        appliesTo: ["corn","rye","flaxseed"],
        factor: 0.062767984
      },
      {
        convertTo: "tonnes_ha",
        appliesTo: ["canola"],
        factor: 0.056043046
      },
      {
        convertTo: "tonnes_ha",
        appliesTo: ["triticale"],
        factor: 0.058279481
      },
      {
        convertTo: "tonnes_ha",
        appliesTo: ["safflower"],
        factor: 0.042168089
      }
    ];

    function convert(value, crop, toUnit) {
      for (i in units) {
        if (units[i].convertTo == toUnit && units[i].appliesTo.includes(crop)) {
          return value * units[i].factor;
        }
      }
    }

    return {
      convertCropUnit: function (value, key, data, args) {
        new_value = convert(value, data.crop, args.toUnit);
        return new_value.toFixed(2);
      }
    };
  });