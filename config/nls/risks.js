define({
    root: {
        title: "Risks",
        description: "Display Drought Intensity",
        date: "July 2015",
        instruction_title: "Instructions",
        instruction: "Please select graphics to analyse.",
        available_information: "Available information:",
        drought: "Drought Intensity",
        drought_bar_chart: "Drought Intensity Area",
        drought_pie_chart: "Drought Intensity (% area)",
        drought_class: {
            "-1": "No drought",
            "0": "Abnormally dry",
            "1": "Moderate",
            "2": "Severe ",
            "3": "Extreme ",
            "4": "Exceptional "
        },
        grasshopper: "Grasshopper",
        grasshopper_class: {
            "-1": "No Grasshopper",
            "0": "Not Available",
            "1": "None to Very Light",
            "2": "Very Light",
            "3": "Light",
            "4": "Moderate",
            "5": "Severe",
            "6": "Very Severe"
        },

        drought_severity: "Drought Severity",
        drought_area: "Area (km²)",
        count: "Count",
        percent: "% Area"
    },
    fr: true
});
