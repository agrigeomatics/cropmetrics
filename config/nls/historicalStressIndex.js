define({
    root: {
        title: "Stress Index",
        date_title: "Date (month - day)",
        stress_index_title: "Stress Index",
        light: "light",
        moderate: "moderate",
        severe: "severe"
    },
    fr: true
});
