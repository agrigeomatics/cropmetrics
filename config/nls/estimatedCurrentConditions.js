define({
    root: {
        title: "Estimated Current Conditions",
        description: "Estimated Current Conditions",
        instruction_title: "Instructions",
        instruction: "Please select graphics to analyse.",
        available_information: "Available information:",

        date_title: "Date (month - day)",

        climate_stress: "Climate Stress",

        precip_acc: "Accumulated Precipitation",
        precip_acc_historical_pie_chart: "Accumulated Precipitation Trend (% area)",
        precip_acc_historical_class: {
            0: "Not Available",
            1: "Poor",
            2: "Below Avg",
            3: "Average",
            4: "Favourable",
            5: "Excellent"
        },

        heat_acc: "Accumulated Heat",
        heat_acc_line_chart: "Accumulated Heat (GDD)",
        heat_acc_historical_pie_chart: "Accumulated Heat Trend (% area)",
        accumulated_historic_30_year: "30 years average",
        heat_acc_historical_class: {
            0: "Not Available",
            1: "Poor",
            2: "Below Avg",
            3: "Average",
            4: "Favourable",
            5: "Excellent"
        },

        crop_stage: "Crop Stage",
        crop_stage_pie_chart: "Crop Stage (% area)",
        crop_stage_current_class: {
            0: "Not available",
            1: "Emergence",
            2: "Jointing",
            3: "Heading",
            4: "Soft Dough",
            5: "Ripening"
        },

        stress_index: "Stress Index",
        stress_index_pie_chart: "Stress Index (% area)",
        stress_index_current_class: {
            0: "Not available",
            1: "No Stress",
            2: "Light",
            3: "Moderate",
            4: "Severe"
        },

        crop_stage_historical_pie_chart: "Crop Stage Compared to Historical Trend",
        crop_stage_historical_class: {
            0: "Not available",
            1: "Behind",
            2: "Normal",
            3: "Ahead"
        },

        count: "Count",
        percent: "% Area",

        ndvi: "Satellite Vegetation Condition",
        ndvi_bar_chart: "Satellite Vegetation Condition",
        ndvi_pie_chart: "Satellite Vegetation Condition (% area)",
        ndvi_class: {
            "-1": "No Data",
            "0": "Much Lower",
            "1": "Lower",
            "2": "Average",
            "3": "Higher",
            "4": "Much Higher"
        }
    },
    fr: true
});
