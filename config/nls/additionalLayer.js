define({
    root: {
        drought_intensity: "Drought Intensity",
        accumulated_precipitation: "Accumulated Precipitations",
        accumulated_heat: "Accumulated Heat",
        poor: "Poor",
        below_average: "Below Average",
        average: "Average",
        favourable: "Favourable",
        excellent: "Excellent",
        spring_wheat: "Spring Wheat",
        overall_outlook: "Overall Outlook",
		historical_yield: "Historical Yield",
        historical_canadian_drought_monitor: "Historical Canadian Drought Monitor",
        stress_index: "Stress Index",
        crop_stage: "Crop Stage",
        forecasted_yield_high90: "Forecasted Yield: Best Case",
        forecasted_yield_low10:  "Forecasted Yield: Worst Case",
        forecasted_yield_median: "Forecasted Yield: Most Likely",
        forecasted_yield_cpi:    "Forecasted Yield: CPI",
        satellite_vegetation: "Satellite Vegetation Condition",
        evaporative_stress_index_2_week: "Evaporative Stress Index (2 weeks)",
        evaporative_stress_index_4_week: "Evaporative Stress Index (4 weeks)",
        ndvi: {
            much_lower: "Much Lower",
            lower: "Lower",
            average: "Average",
            higher: "Higher",
            much_higher: "Much Higher"
        },
        masks: "Agriculture Region",
        mask: "Mask",
        grasshopper: {
            forecast: "Grasshopper Forecast",
            none: "None to Very Light",
            very_light: "Very Light",
            light: "Light",
            moderate: "Moderate",
            severe: "Severe",
            very_severe: "Very Severe"
        },
        acc_precip: {
            "1": "< 25 mm",
            "2": "25 mm - 50 mm",
            "3": "50 mm - 100 mm",
            "4": "100 mm - 150 mm",
            "5": "150 mm - 200 mm",
            "6": "200 mm - 250 mm",
            "7": "250 mm - 300 mm",
            "8": "300 mm - 400 mm",
            "9": "400 mm - 500 mm",
            "10": "500 mm - 600 mm",
            "11": "600 mm - 700 mm",
            "12": "> 700 mm "
        }
    },
    fr: true
});
