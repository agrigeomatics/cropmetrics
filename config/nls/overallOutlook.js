define({
    root: {
        title: "Overall Outlook",
        overall_outlook_title: "Show calculation details",
        description: "Display a Overall Outlook Rating",
        instruction_title: "Instructions",
        instruction: "Please select graphics to analyse.",
        available_information: "Available information:",
        overall_outlook: "Overall Outlook",
        cpi_class: {
            0: "Not Available",
            1: "Poor",
            2: "Below Avg",
            3: "Average",
            4: "Favourable",
            5: "Excellent"
        },
    },
    fr: true
});
