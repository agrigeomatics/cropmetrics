define({
    root: {
        overall_outlook: "Overall Outlook:",
        car: "CAR",
        province: "Province:",
        cpi_class: {
            0: "Not Available",
            1: "Poor",
            2: "Below Avg",
            3: "Average",
            4: "Favourable",
            5: "Excellent"
        },
        selectAlt: "Selection button icon"
    },
    fr: true
});
