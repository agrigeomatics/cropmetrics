define({
    root: {
        date_label: "Date:",
        crop_label: "Crop Type:",
        yield_units_label: "Yield Units:",
        crop_spring_wheat: "Spring Wheat",
        crop_canola: "Canola",
        crop_oats: "Oats",
        crop_barley: "Barley",
        crop_soybeans: "Soybeans",
        crop_dutwht: "Durum Wheat",
        crop_corn: "Corn",
        date_july_15: "July 15, 2015",
        date_august_15: "August 15, 2015",
        date_july: "July 2015",
        date_august: "August 2015",
        yield_units_metric: "Metric",
        yield_units_imperial: "Imperial"
    },
    fr: true
});
