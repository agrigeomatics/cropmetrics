define({
    root: {
        title: "Forecast Yield Potential",
        spring_wheat: "Spring Wheat",
        description: "Spring Wheat",
        date: "July 15, 2015",
        instruction_title: "Instructions",
        instruction: "Please select graphics to analyse.",
        available_information: "Available information:",
        worst_case: "Worst Case",
        most_likely: "Most likely",
        best_case: "Best Case",
        separator: ":"
    },
    fr: true
});
