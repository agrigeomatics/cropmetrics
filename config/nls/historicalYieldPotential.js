define({
    root: {
        title: "Historical Yield by Forecast",
        description: "Display a Spring Wheat Historical Yield Chart",
        instruction_title: "Instructions",
        instruction: "Please select graphics to analyse.",
        available_information: "Available information:",
        year_title: "Year",
        yield_title: "Yield",
        best: "best case",
        most: "most likely",
        worst: "worst case"
    },
    fr: true
});
