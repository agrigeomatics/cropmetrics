define({
    root: {
        title: "Forecast Conditions",
        description: "48 hour forecast",
        instruction_title: "Instructions",
        instruction: "Please select graphics to analyse.",
        available_information: "Available information:",

        current_temperature: "Forecast Air Temperature",
        current_temperature_pie_chart: "Forecast Air Temperature (% area)",

        current_precipitation: "Forecast Precipitation",
        current_precipitation_pie_chart: "Forecast Precipitation (% area)",
        current_precipitation_bar_chart: "Forecast Precipitation",

        precipitation: "Precipitation",
        count: "Count",
        percent: "% Area",

        temperature_current_class: {
            0: "Not Available",
            1: "< -15 °C",
            2: "-15 - -10 °C",
            3: "-10 - -5 °C",
            4: "-5 - 0 °C",
            5: "0 - 5 °C",
            6: "5 - 10 °C",
            7: "10 - 15 °C",
            8: "15 - 20 °C",
            9: "20 - 25 °C",
            10: "25 - 30 °C",
            11: "> 30 °C"
        },

        precip_current_class: {
            0: "Not Available",
            1: "< 0.10 mm",
            2: "0.10 - 0.25 mm",
            3: "0.25 - 0.50 mm",
            4: "0.50 - 1.0 mm",
            5: "1.0 - 2.5 mm",
            6: "2.5 - 5.0 mm",
            7: "5.0 - 7.5 mm",
            8: "7.5 - 10 mm",
            9: "10 - 15 mm",
            10: "15 - 20 mm",
            11: "20 - 25 mm",
            12: "25 - 50 mm",
            13: "50 - 100 mm",
            14: "> 100.0 mm"
        },

    },
    fr: true
});
