define({
    root: {
        title: "Accumulated Precipitations",
        date_title: "Date (month - day)",
        y_title: "Accumulated Precipitations (mm)",
        accumulated_historic: "Accumulated Precipitations",
        accumulated_historic_30_year: "30 years average"
    },
    fr: true
});
