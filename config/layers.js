/*
Configuration File for Layers
*/
define([
  "esri/layers/support/RasterFunction",
  "config/unit_converter.js"
], function(RasterFunction,unit_converter) {
  return {

    layerGroups: [
      {
        title: "Generalized 2016 Statistics Canada Boundaries",
        visible: true,
        opacity: 0.5
      },
      {
        title: "Indices",
        visible: true,
        opacity: 0.75
      },
      {
        title: "Climate Services",
        visible: true,
        opacity: 0.75
      },
      {
        title: "Crop Yield Information",
        visible: true,
        opacity: 0.75
      }
    ],

    layers: [
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/imageservices/crop_yield/ImageServer",
        title: "Forecasted Median Crop Yield (Barley)",
        frequency: "monthly",
        expressionTemplate: "\"metric\" = 'yield_median' AND \"crop\" = '@@@cropname@@@' AND (\"obs_date\" <= date '@@@date@@@ 12:00:00' AND \"obs_date\" > date '@@@startDate@@@ 12:00:00')",
        definitionExpression: "\"metric\" = 'yield_median' AND \"crop\" = 'barley' AND (\"obs_date\" <= date '2017-07-15 12:00:00' AND \"obs_date\" > date '2016-06-15 12:00:00')",
        visible: true,
        renderingFunction: function(cropname){
          return ["yield_"+cropname, cropname];
        },
        renderingRule: new RasterFunction ({functionName: "yield_barley"}),
        popupTemplate: {
          title: "Forecasted Median Crop Yield",
          content: "Crop: {crop}<br>Metric: {metric}<br>Value: {Raster.ItemPixelValue} bushels/acre ({Raster.ItemPixelValue:convertCropUnit(toUnit: 'tonnes_ha')} tonnes/ha) <br>Data Date: {obs_date:DateFormat(selector: 'date', formatLength: 'long')}"
        },
        typeString: "imagery",
        groupLayer: "Crop Yield Information",
        cropMetrics: true
      },
      // {
      //   url: "http://external3.intelecgeomatics.com/arcgis/rest/services/crops_metrics/historical_yield/ImageServer",
      //   title: "Historical Yield (Barley)",
      //   frequency: "annual",
      //   expressionTemplate: "\"crop\" = '@@@cropname@@@' AND (\"obs_date\" <= date '@@@date@@@ 12:00:00' AND \"obs_date\" > date '@@@startDate@@@ 12:00:00')",
      //   definitionExpression: "\"crop\" = 'barley' AND (\"obs_date\" <= date '2017-07-17 12:00:00' AND \"obs_date\" > date '2016-07-15 12:00:00')",
      //   visible: false,
      //   renderingFunction: function(cropname){
      //     return ["yield_"+cropname, cropname];
      //   },
      //   renderingRule: new RasterFunction ({functionName: "yield_barley"}),
      //   popupTemplate: {
      //     title: "Historical Crop Yield",
      //     content: "Crop: {crop}<br>Value: {Raster.ItemPixelValue}<br>Data Date: {obs_date:DateFormat(selector: 'date', formatLength: 'long')}"
      //   },
      //   typeString: "imagery",
      //   groupLayer: "Crop Yield Information",
      //   cropMetrics: false
      // },
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/imageservices/crop_health_indices/ImageServer",
        title: "Crop Stage (Small Grains)",
        frequency: "weekly",
        expressionTemplate: "\"metric\" = 'growth_stage' AND (\"obs_date\" <= date '@@@date@@@ 12:00:00' AND \"obs_date\" > date '@@@startDate@@@ 12:00:00')",
        definitionExpression: "\"metric\" = 'growth_stage' AND (\"obs_date\" <= date '2017-07-17 12:00:00' AND \"obs_date\" > date '2017-07-08 12:00:00')",
        visible: false,
        renderingFunction: function(cropname){
          if (cropname == "corn" || cropname =="soybean") {
            return ["growth_stage_corn_soybeans", "Corn/Soybeans"];
          } else {
            return ["growth_stage_small_grains","Small Grains"];
          }
        },
        renderingRule: new RasterFunction ({functionName: "growth_stage_small_grains"}),
        popupTemplate: {
          title: "Growth Stage",
          content: "Growth Stage:  {Raster.ItemPixelValue}<br>Data Date: {obs_date:DateFormat(selector: 'date', formatLength: 'long')}"
        },
        typeString: "imagery",
        groupLayer: "Indices",
        cropMetrics: true
      },{
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/imageservices/crop_health_indices/ImageServer",
        title: "Crop Stress Index",
        frequency: "weekly",
        expressionTemplate: "\"metric\" = 'stress_index' AND (\"obs_date\" <= date '@@@date@@@ 12:00:00' AND \"obs_date\" > date '@@@startDate@@@ 12:00:00')",
        definitionExpression: "\"metric\" = 'stress_index' AND (\"obs_date\" <= date '2017-07-17 12:00:00' AND \"obs_date\" > date '2017-07-08 12:00:00')",
        visible: false,
        renderingRule: new RasterFunction ({functionName: "stress_index"}),
        popupTemplate: {
          title: "Crop Stress Index",
          content: "Crop Stress index:  {Raster.ItemPixelValue}<br>Data Date: {obs_date:DateFormat(selector: 'date', formatLength: 'long')}"
        },
        typeString: "imagery",
        groupLayer: "Indices",
        cropMetrics: true
      },
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/app_agclimate_agclimat/agclimate_ac/ImageServer",
        title: "Accumulated Precipitation (mm)",
        frequency: "weekly",
        expressionTemplate: "\"tType\" = 'ac_gs' AND (\"dStart\" <= date '@@@date@@@ 12:00:00' AND \"dStart\" > date '@@@startDate@@@ 12:00:00')",
        definitionExpression: "\"tType\" = 'ac_gs' AND (\"dStart\" <= date '2017-07-17 12:00:00' AND \"dStart\" > date '2017-07-08 12:00:00')",
        visible: false,
        renderingRule: new RasterFunction ({functionName: "ac_gs"}),
        popupTemplate: {
          title: "Accumulated Precipitation",
          content: "Accumulated Precipitation:  {Raster.ItemPixelValue}mm<br>Data Date: {dStart:DateFormat(selector: 'date', formatLength: 'long')}"
        },
        typeString: "imagery",
        groupLayer: "Climate Services",
        cropMetrics: {
          enabled: true,
          functionName: "computeStatisticsHistograms",
          popupTemplate: {
            title: "Accumulated Precipitation",
            content: "<strong>{expression/title}:</strong> Mean={mean:NumberFormat(places:2)} <a id='' class='esri-icon-chart' title='Click to see histogram of values' aria-label='Click to see histogram of values'></a>",
            // content: function() {
            //   //what about a function that handles format string?  can we return that
            //   // without timing issues?
            //   return ""
            // },
            expressionInfos: [{
              name: "title",
              expression: "Text('Accumulated Precipitation')",
            }]
          },
          classes: [
            [0,10,"#990000","< 10"],
            [10,20,"#8C410B","10 - 20"],
            [20,35,"#A64C14","20 - 35"],
            [35,65,"#BF7F2C","35 - 65"],
            [65,110,"#E0C47E","65 - 110"],
            [110,155,"#F0E59E","110 - 155"],
            [155,200,"#FFFFBD","155 - 200"],
            [200,250,"#E8FFBD","200 - 250"],
            [250,300,"#C1E697","250 - 300"],
            [300,350,"#77C777","300 - 350"],
            [350,400,"#31A359","350 - 400"],
            [400,475,"#BFFFFF","400 - 475"],
            [475,550,"#73E1FF","475 - 550"],
            [550,650,"#4295C7","550 - 650"],
            [650,750,"#08529C","650 - 750"],
            [750,2000,"#072F6B","> 750"]
           ],
           renderingRule: "<classes/default>"  //use classes, or revert to one in use.
        }
      },
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/app_agclimate_agclimat/agclimate_ch/ImageServer",
        title: "Accumulated Heat (CHU)",
        expressionTemplate: "\"tType\" = 'ch_gs' AND (\"dStart\" <= date '@@@date@@@ 12:00:00' AND \"dStart\" > date '@@@startDate@@@ 12:00:00')",
        frequency: "weekly",
        definitionExpression: "\"tType\" = 'ch_gs' AND (\"dStart\" <= date '2017-07-15 12:00:00' AND \"dStart\" > date '2017-07-08 12:00:00')",
        visible: false,
        renderingRule: new RasterFunction ({functionName: "ch"}),
        popupTemplate: {
          title: "Accumulated Heat",
          content: "Crop (Corn) Heat Units: {Raster.ItemPixelValue}<br>Data Date: {dStart:DateFormat(selector: 'date', formatLength: 'long')}"
        },
        typeString: "imagery",
        groupLayer: "Climate Services",
        cropMetrics: true
      },
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/app_agclimate_agclimat/agclimate_gd/ImageServer",
        title: "Accumulated Heat (GDD)",
        expressionTemplate: "\"tType\" = 'gd_5' AND (\"dStart\" <= date '@@@date@@@ 12:00:00' AND \"dStart\" > date '@@@startDate@@@ 12:00:00')",
        frequency: "weekly",
        definitionExpression: "\"tType\" = 'gd_5' AND (\"dStart\" <= date '2017-07-15 12:00:00' AND \"dStart\" > date '2017-07-08 12:00:00')",
        visible: false,
        renderingRule: new RasterFunction ({functionName: "gd_5"}),
        popupTemplate: {
          title: "Accumulated Heat",
          content: "Growing Degreee Days (GDD): {Raster.ItemPixelValue}<br>Data Date: {dStart:DateFormat(selector: 'date', formatLength: 'long')}"
        },
        typeString: "imagery",
        groupLayer: "Climate Services",
        cropMetrics: true
      },
      {
        //url: "http://devproxyint.agr.gc.ca/atlas/rest/services/imageservices/weekly_maximum_ndvi_anomaly/ImageServer",
        url: "http://devproxyint/atlas/rest/services/users_mcburneym/crop_metrics_ndvi_anomaly/ImageServer",
        title: "Satellite Vegetation Condition",
        expressionTemplate: "\"date_start\" <= date '@@@date@@@ 12:00:00' AND \"date_start\" > date '@@@startDate@@@ 12:00:00'",
        frequency: "weekly",
        definitionExpression: "\"date_start\" <= date '2017-07-15 12:00:00' AND \"date_start\" > date '2017-07-08 12:00:00'",
        visible: false,
        renderingRule: new RasterFunction ({functionName: "classfied_scale"}),
        popupTemplate: {
          title: "Satellite Vegetation Condition",
          content: "Satellite Vegetation Condition: {Raster.ItemPixelValue}<br>Data Date: {date_start:DateFormat(selector: 'date', formatLength: 'long')}"
        },
        typeString: "imagery",
        groupLayer: "Indices",
        cropMetrics: true
      },
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/imageservices/evaporative_stress_index_2_week/ImageServer",
        title: "Evaporative Stress Index (2 week)",
        expressionTemplate: "\"dDate\" <= date '@@@date@@@ 12:00:00' AND \"dDate\" > date '@@@startDate@@@ 12:00:00'",
        frequency: "weekly",
        definitionExpression: "\"dDate\" <= date '2017-07-15 12:00:00' AND \"dDate\" > date '2017-07-08 12:00:00'",
        visible: false,
        //default?
        //renderingRule: new RasterFunction ({functionName: "ch"}),
        popupTemplate: {
          title: "Evaporative Stress Index (2 week)",
          content: "ESI 2 weeks: {Raster.ItemPixelValue}<br>Data Date: {dDate:DateFormat(selector: 'date', formatLength: 'long')}"
        },
        typeString: "imagery",
        groupLayer: "Indices",
        cropMetrics: true
      },
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/imageservices/evaporative_stress_index_4_week/ImageServer",
        title: "Evaporative Stress Index (4 week)",
        expressionTemplate: "\"dDate\" <= date '@@@date@@@ 12:00:00' AND \"dDate\" > date '@@@startDate@@@ 12:00:00'",
        frequency: "weekly",
        definitionExpression: "\"dDate\" <= date '2017-07-15 12:00:00' AND \"dDate\" > date '2017-07-08 12:00:00'",
        visible: false,
        //default?
        //renderingRule: new RasterFunction ({functionName: "ch"}),
        popupTemplate: {
          title: "Evaporative Stress Index (4 week)",
          content: "ESI 4 weeks: {Raster.ItemPixelValue}<br>Data Date: {dDate:DateFormat(selector: 'date', formatLength: 'long')}"
        },
        typeString: "imagery",
        groupLayer: "Indices",
        cropMetrics: true
      },
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/mapservices/aafc_canadian_drought_monitor/MapServer",
        title: "Canadian Drought Monitor",
        listMode: "hide-children",
        sublayers: [{
          id: 0,
          definitionExpression: "OBS_DATE <= date '2017-07-15 12:00:00' AND OBS_DATE > date '2017-06-15 12:00:00'",
          expressionTemplate: "OBS_DATE <= date '@@@date@@@ 12:00:00' AND OBS_DATE > date '@@@startDate@@@ 12:00:00'",
          frequency: "monthly",
          popupTemplate: {
            title: "Canadian Drought Monitor",
            content: "{DM:droughtLabel}<br>Data Date: {OBS_DATE:DateFormat(selector: 'date', formatLength: 'long')}"
          }
        }],
        visible: false,
        typeString: "map-image",
        groupLayer: "Indices",
        cropMetrics: false
      },
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/mapservices/statistics_canada_census_boundaries/MapServer",
        title: "Census of Agriculture Region",
        listMode: "hide-children",
        sublayers: [{
          id: 1,
          popupTemplate: {
            title: "Census of Agriculture Region",
            content: "Region ID: {CARUID} <br>Region Name: {CARENAME}"
            // content: function(target) {
            //   var geo = target.graphic.geometry;
            //   return "<b>this is some text</b>";
            // }
          }
        }],
        visible: true,
        typeString: "map-image",
        groupLayer: "Generalized 2016 Statistics Canada Boundaries"
      },
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/mapservices/statistics_canada_census_boundaries/MapServer",
        title: "Census Division",
        listMode: "hide-children",
        sublayers: [{
          id: 2,
          popupTemplate: {
            title: "Census Division",
            content: "Division ID: {CDUID} <br>Division Name: {CDNAME}"
          }
        }],
        visible: false,
        typeString: "map-image",
        groupLayer: "Generalized 2016 Statistics Canada Boundaries"
      },
      {
        url: "http://devproxyint.agr.gc.ca/atlas/rest/services/mapservices/statistics_canada_census_boundaries/MapServer",
        title: "Census Consolidated Subdivision",
        listMode: "hide-children",
        sublayers: [{
          id: 3,
          popupTemplate: {
            title: "Census Consolidated Subdivision",
            content: "Subdivision ID: {CCSUID} <br>Subdivision Name: {CCSNAME}"
          }
        }],
        visible: false,
        typeString: "map-image",
        groupLayer: "Generalized 2016 Statistics Canada Boundaries"
      }
    ]

  }
});



