define(["dojo/promise/all", "dojo/request/xhr", "dojo/string",
        "esri/symbols/SimpleMarkerSymbol",
        "esri/graphic",
        "esri/layers/MosaicRule",
        "esri/layers/RasterFunction",
        "esri/tasks/ImageServiceIdentifyTask",
        "esri/tasks/ImageServiceIdentifyParameters",
        "esri/tasks/query",
        "esri/tasks/QueryTask",
        "CM2/js/mapManager"
       ],
       function(promiseAll, xhr, string,
                SimpleMarkerSymbol,
                Graphic,
                MosaicRule,
                RasterFunction,
                ImageServiceIdentifyTask,
                ImageServiceIdentifyParameters, 
                Query,
                QueryTask,
                mapManager
               )

    {
        let selLayersInfo = null;
        let aPromises = null;
      
        return {

            identify: function(event) {
                let sms = new SimpleMarkerSymbol(CM2.servicesJSON.identifyMarker);
                let graphic = mapManager.addGraphic(event.mapPoint, sms, "IdentifyMarker");

                $('#paneIdentifyResults .identData').each(function(index, elem) {
                    elem.innerHTML = "";
                });            

                for (let i = 0; i < CM2.aFeatureLayersInfo.length; i++) {
                    identifyFeatureLayer(CM2.aFeatureLayersInfo[i], event.mapPoint);
                }
                
                let crop = $('#lboCropType').find(":selected").val();
                selLayersInfo = [];
                aPromises = [];
                for (let i = 0; i < CM2.aLayersInfo.length; i++) {
                    identifyImageService(CM2.aLayersInfo[i], event.mapPoint);
                }
                promiseAll(aPromises).then(function (aResults) {
                    let sHTML = '<div class="identTable">';
                    for (let i = 0; i < aResults.length; i++) {
                        let sLayer = selLayersInfo[i].title;
                        let sValue = getDescription(selLayersInfo[i], aResults[i].value);
                        if (sLayer) {
                            if (selLayersInfo[i].layer == "agclimate_gd") {
                                if (!(crop == "corngr" || crop == "soybns"))
                                    sHTML += ' <div><div>' + sLayer + '</div><div>' + sValue + '</div></div>';
                            } else if (selLayersInfo[i].layer == "agclimate_ch") {
                                if (crop == "corngr" || crop == "soybns")
                                    sHTML += ' <div><div>' + sLayer + '</div><div>' + sValue + '</div></div>';
                            } else
                                sHTML += ' <div><div>' + sLayer + '</div><div>' + sValue + '</div></div>';
                        }
                    }
                    sHTML += '</div>';
                    byId("identImage").innerHTML += sHTML;
                });

            }

        };

        function identifyImageService(layerInfo, geometry) {
            let url = layerInfo.url + '/identify';
            let mosaicRule = new MosaicRule({mosaicMethod: "esriMosaicNone",
                                             where: layerInfo.condition2 });
            
            let parameter = new ImageServiceIdentifyParameters();
            parameter.geometry = geometry;
            parameter.mosaicRule = mosaicRule;

            let task = new ImageServiceIdentifyTask(url);
            aPromises.push(task.execute(parameter));
            selLayersInfo.push(layerInfo);
        };


        function identifyFeatureLayer(layerInfo, geometry) {
            let query = new Query();
            query.returnGeometry = false;
            query.outFields = ['*'];
            query.geometry = geometry;
            let queryTask = new QueryTask(layerInfo.url);
            queryTask.execute(query, function(results) {
                let feat = results.features[0];
                let sHTML = '<div class="identLayerName">' + layerInfo.title + '</div>';
                sHTML += '<div class="identTable">';
                for (let prop in feat.attributes) {
                    if (!prop.match(/.*[a-z].*/))
                        sHTML += ' <div><div>' +  prop + '  </div><div>' + feat.attributes[prop ]+ '</div></div>';
                }
                sHTML += '</div>';
                byId("identBoundary").innerHTML += sHTML;
            });
        };

    }
);
