define([
  "dojo/date",
  "dojo/date/locale"
],function (date,locale) {
  function format(date, fmt){ return locale.format( date, {selector:"date", datePattern:fmt } ); };
  return {
    //return date before date by frequency
    getLastDate: function (datestring, frequency) {
      var endDate = new Date(datestring + " 00:00:00 GMT");
      var lastDate = new Date(datestring + " 00:00:00 GMT");
      fmt = "yyyy-MM-dd";
      switch (frequency) {
        case "weekly":
          //date - 7
          lastDate = date.add(endDate,"week",-1);
          break;
        case "biweekly":
          //date - 1 month
          lastDate = date.add(endDate,"week",-2);
          break;
        case "monthly":
          //date - 1 month
          lastDate = date.add(endDate,"month",-1);
          //I wanted one less month, default is month less a day, so I'm adding back,
          lastDate = date.add(lastDate,"day",1);
          break;
        case "annual":
          //date - 1 year
          lastDate = date.add(endDate,"year",-1);
          break;
      }
      return format(lastDate, fmt);
    },
    epochToDateStamp: function(value, key, data) {
      var date = new Date(data[key]);
      fmt = "yyyy-MM-dd";
      return format(date,fmt);
    },
    getSomething: function () {
      return "something";
    }

  };
});