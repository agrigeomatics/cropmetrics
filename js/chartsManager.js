define(["dojo/promise/all",
        "dojo/request/xhr",
        "dojo/string",
        "dojo/number",

        "dojox/charting/Chart",
        "dojox/charting/plot2d/Pie",
        "dojox/charting/plot2d/Markers",
        "dojox/charting/plot2d/Columns",
        "dojox/charting/action2d/Highlight",
        "dojox/charting/action2d/MoveSlice",
        "dojox/charting/action2d/Tooltip",
        "dojox/charting/themes/MiamiNice",
        "dojox/charting/widget/Legend",
        
        "esri/Color",
        "esri/tasks/query",
        "esri/tasks/QueryTask"
    ],
       function(promiseAll,
                xhr,
                string,
                number,

                Chart, Pie, Markers, Columns, Highlight, MoveSlice, Tooltip, MiamiNice, Legend,

                Color,
                Query,
                QueryTask
               )

    {

        let graphsCategoryId;
        let logicalLayer;

        return { 

            createCharts: function(_graphsCategoryId) {
                graphsCategoryId = _graphsCategoryId;
                logicalLayer = null;
                console.log(graphsCategoryId);
                let crop = $('#lboCropType').find(":selected").val();

                $.each($('#graphsControl > div'), function( key, elem) {
                    elem.style.display = 'none';
                });

                if (!(graphsCategoryId == "chartsHistoYieldForecast"))
                    $.each($('#graphsControl > div.graphTypeSelect'), function( key, elem) {
                        elem.style.display = 'block';
                    });
                
                switch(graphsCategoryId) {
                    case "chartsHistoYieldForecast":
                        byId("divHistoYieldForecast").style.display = 'block';
                        HistoYieldForecastChart();
                        break;
                    case "chartsCropStage":
                        logicalLayer = "crop_stage";
                        byId("divCropStage").style.display = 'block';
                        break;
                    case "chartsAccHeat":
                        if (crop == "corngr" || crop == "soybns")
                            logicalLayer = "agclimate_ch";
                        else
                            logicalLayer = "agclimate_gd";
                        byId("divAccHeat").style.display = 'block';
                        break;
                    case "chartsAccPrecp":
                        logicalLayer = "agclimate_ac";
                        byId("divAccPrecp").style.display = 'block';
                        break;
                    case "chartsNDVI":
                        logicalLayer = "ndvi";
                        byId("divNDVI").style.display = 'block';
                        break;
                    case "chartsSI":
                        logicalLayer = "stress_index";
                        byId("divSI").style.display = 'block';
                        break;

                    case "chartsCDM":
                        logicalLayer = "CDM";
                        byId("divCDM").style.display = 'block';
                        break;

                    default:
                        alert("To Do");
                        return;
                }
                computeCharts(graphsCategoryId, logicalLayer);
            },

            computeCharts: function() {
                computeCharts(graphsCategoryId, logicalLayer);
            }

        }

        function computeCharts(graphsCategoryId, logicalLayer) {
            function formatValue(dataType, value, tot) {
                let sValue;
                if (dataType == "Pct")
                    sValue = ': ' + number.format((value / tot * 100), {places: 1}) + '%';
                else
                    sValue = ': ' + value * 25 + ' km2';
                return sValue;
            }

            let crop = $('#lboCropType').find(":selected").val();
            let colors = ["#ffffff", "#d3d3d3", "#ffb6c1", "#90ee90", "#add8e6", "#f08080", 
                          "#ffa07a", "#ffffe0", "#e0ffff", "c0c0c0"];
            // ["White", "LightGrey", "LightPink", "LightGreen", "LightBlue", "LightCoral", 
            //   "LightSalmon", "LightYellow", "LightCyan", "Sylver"];
            let chartData = [];
            let tableData = [];
            let histogram = null;
            let layerInfo;
            let legend = null;
 
            for (let i = 0; i < CM2.aLayersInfo.length; i++) {
                if (CM2.aLayersInfo[i].logicalName == logicalLayer) {
                    console.log (logicalLayer, i);
                    histogram = CM2.aLayersInfo[i].histogram;
                    layerInfo = CM2.aLayersInfo[i];
                    break;
                }
            }
            let tot = 0;
            for (let i = 0; i < histogram.length; i++) {
                tot += histogram[i];
            }
            let dataType = $('input[name=graphTypeData]:checked').val();

            switch (logicalLayer) {
                case "crop_stage":
                    if (crop == "corngr" || crop == "soybns")
                        labels = layerInfo.labels.corngr_soybns;
                    else
                        labels = layerInfo.labels.small_grains;
                    break;
                default:
                    labels = layerInfo.labels;
                    break;
            }
            
            let histogram2;
            switch (logicalLayer) {
                case "CDM":
                case "crop_stage":
                    for (let i = 0; i < histogram.length; i++) {
                        if (histogram[i] > 0) {
                            let category = i-128;
                            label = labels[category+1];
                            label += formatValue(dataType, histogram[i], tot)
                            chartData.push({
                                y: histogram[i],
                                text: label,
                                color: colors[category+1]
                            });
                            tableData.push({
                                Category: labels[ (i-128) + 1], // -1 is at 127 in histogram, 0 in labels[], etc...
                                Percentage: number.format((histogram[i] / tot * 100), {places: 1}) + '%',
                                Area: histogram[i] * 25 + ' (km2)'
                            });
                        }
                    }
                    break;
                case "stress_index":
                    histogram2 = [0,0,0,0,0];
                    for (let i = 0; i < histogram.length; i++) {
                        if (histogram[i] > 0) {
                            let category = 0;
                            let cat = i;
                          //if (cat >   0 && cat < 25) category = 1;
                            if              (cat < 25) category = 1;
                            if (cat >= 25 && cat < 40) category = 2;
                            if (cat >= 40 && cat < 60) category = 3;
                            if (cat >= 60            ) category = 4;
                          //if (cat >= 60 && cat <100) category = 4;
                            histogram2[category] += histogram[i];
                        }
                    }
                    for (let i = 0; i < histogram2.length; i++) {
                        label = labels[i];
                        label += formatValue(dataType, histogram2[i], tot)
                        chartData.push({
                            y: histogram2[i],
                            text: label,
                            color: colors[i]
                        });
                        tableData.push({
                            Category: labels[i],
                            Percentage: number.format((histogram2[i] / tot * 100), {places: 1}) + '%',
                            Area: histogram2[i] * 25 + ' (km2)'
                        });
                    }
                    break;
                case "ndvi":
                    histogram2 = [0,0,0];
                    for (let i = 0; i < histogram.length; i++) {
                        if (histogram[i] > 0) {
                            let category = 0;
                            let cat = i - 128;
                            if (cat  < 0) category = 0;
                            if (cat== 0) category = 1;
                            if (cat > 0) category = 2;
                            histogram2[category] += histogram[i];
                        }
                    }
                    for (let i = 0; i < histogram2.length; i++) {
                        label = labels[i];
                        label += formatValue(dataType, histogram2[i], tot)
                        chartData.push({
                            y: histogram2[i],
                            text: label,
                            color: colors[i]
                        });
                        tableData.push({
                            Category: labels[i],
                            Percentage: number.format((histogram2[i] / tot * 100), {places: 1}) + '%',
                            Area: histogram2[i] * 25 + ' (km2)'
                        });
                    }
                    break;
                case "agclimate_ac":
                case "agclimate_gd":
                case "agclimate_ch":
                    let layerInfo = null;
                    for (let i = 0; i < CM2.aLayersInfo.length; i++) {
                        if (CM2.aLayersInfo[i].logicalName == logicalLayer) {
                            layerInfo = CM2.aLayersInfo[i];
                            break;
                        }
                    }
                    
                    histogram2 = new Array(layerInfo.classes.length);
                    for (let i = 0; i < histogram2.length; i++) {
                        histogram2[i] = 0;
                    }
                    for (let i = 0; i < histogram.length; i++) {
                        if (histogram[i] > 0) {
                            let category = 0;
                            for (let j = 0; j < layerInfo.classes.length; j++) {
                                let aclass = layerInfo.classes[j];
                                if (histogram[i] >= aclass[1] && histogram[i] < aclass[2]) {
                                    category = j;
                                    break;
                                }
                            }
                            histogram2[category] += histogram[i];
                        }
                    }
                    for (let i = 0; i < histogram2.length; i++) {
                        label = layerInfo.classes[i][1] + ' - ' + layerInfo.classes[i][2];
                        label += formatValue(dataType, histogram2[i], tot)
                        chartData.push({
                            y: histogram2[i],
                            text: label,
                            color: layerInfo.classes[i][0]
                        });
                        tableData.push({
                            Category: layerInfo.classes[i][1] + ' - ' + layerInfo.classes[i][2],
                            Percentage: number.format((histogram2[i] / tot * 100), {places: 1}) + '%',
                            Area: histogram2[i] * 25 + ' (km2)'
                        });
                    }
                    break;
            }

            for (let i = 0; i < chartData.length; i++) {
                chartData[i].stroke = "silver";
                //chartData[i].tooltip = chartData[i].label + ": "  + chartData[i].y;
            }

            layerInfo.chartData = chartData;
            layerInfo.tableData = tableData;

            let chartType = $('input[name=graphTypeChart]:checked').val();
            byId("graphsTitle").innerHTML = layerInfo.title;
            byId("graphsChart").innerHTML = "";
            byId("graphsLegend").innerHTML = "";

            if (chartType != "Table") {
                let chart1 = new Chart("graphsChart");
                chart1.setTheme(MiamiNice);
                chart1.addPlot("default", {
                    type: chartType,
                    gap: 10,
                    //font: "normal normal 9pt Tahoma",
                    //fontColor: "black",
                    //labelOffset: -30,
                    //radius: 80
                });
                chart1.addSeries("Crop Stage", chartData);

                new MoveSlice(chart1, "default");
                new Highlight(chart1, "default");
                new Tooltip(chart1, "default");
                
                chart1.render(); // graphsLegend the chart!
                if (dijit.byId("graphsLegend"))
                    dijit.byId("graphsLegend").destroyRecursive(true);
                new Legend({chart: chart1, horizontal: false}, "graphsLegend");   

            } else {  // TABLE
                
                let sHTML = '<table class="wb-tables table">';
                sHTML +=  ' <thead>';
                sHTML +=  '  <tr>';
                for (let prop in tableData[0]) {
                    sHTML +=  '  <th>' + prop + '</th>';
                }    
                sHTML +=  '  </tr>';
                sHTML +=  ' </thead>';
                sHTML +=  ' <tbody>';
                for (let i = 0; i < tableData.length; i++) {
                    sHTML +=  '  <tr>';
                    for (let prop in tableData[i]) {
                        sHTML +=  '  <td>' + tableData[i][prop] + '</td>';
                    }    
                    sHTML +=  '  </tr>';
                }
                sHTML +=  ' </tbody>';
                sHTML +=  '</table>';
                
                byId("graphsChart").innerHTML = sHTML;
            }
        }

        function HistoYieldForecastChart() {
            byId("graphsTitle").innerHTML = "";
            byId("graphsChart").innerHTML = "TO DO !";
            byId("graphsLegend").innerHTML = "";
        }

    }
)
