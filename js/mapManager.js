define(["dojo/promise/all", "dojo/request/xhr", "dojo/string",
        "esri/map", "esri/geometry/Point", "esri/graphic",
        "esri/layers/ArcGISImageServiceLayer", "esri/layers/RasterFunction",
        "esri/layers/FeatureLayer", "esri/renderers/SimpleRenderer", "esri/symbols/SimpleFillSymbol",
        "esri/tasks/query", "esri/tasks/QueryTask"
       ],
       function(promiseAll, xhr, string,
                Map, Point, Graphic,
                ArcGISImageServiceLayer, RasterFunction,
                FeatureLayer,  SimpleRenderer,  SimpleFillSymbol,
                Query, QueryTask
               )

    {
        let proxyUrl = "/atlas/wl_domain/esriproxy/proxy.jsp";

        return {

            initMap: function() {
                CM2.urls = {
                    /*boundaries: "http://external3.intelecgeomatics.com/arcgis/rest/services/" +
                             "Boundaries/StatCanBoundaries2016/MapServer/",
                    CDM: "http://external3.intelecgeomatics.com/arcgis/rest/services/" + 
                         "crops_metrics/historical_canadian_drought_monitor/ImageServer",*/
                    regions: "http://external3.intelecgeomatics.com/arcgis/rest/services/" +
    				         "Boundaries/StatCan/MapServer/0"
                };

                CM2.map = new Map("divMap", {
                    basemap: "dark-gray",
                    center: [-95, 57],
                    zoom: 3
                });

                CM2.map.on("extent-change", function (params) {
                    let map = CM2.map;
                    if (map.getLayer("agclimate_ac") || params.levelChange) {
                        if (map.getLevel() >= 5) {  // get the region
                            let ext = map.extent;
                            let pt = new Point(ext.xmin + ((ext.xmax - ext.xmin) / 2),
                                               ext.ymin + ((ext.ymax - ext.ymin) / 2),
                                               map.spatialReference);
                            let query = new Query();
                            query.where = "region <> 'National'";
                            query.returnGeometry = true;
                            query.geometry = pt;
                            let queryTask = new QueryTask(CM2.urls.regions);
                            queryTask.execute(query, setRasterFunction);
                        } else  // National
                            setRasterFunction(null);
                    }
                });

                CM2.map.on("click", function(event) {
                    let method = $('input[name=rdoPolygonMethod]:checked').val();
                    if (method == "rdoIdentify") {
                        require(["CM2/js/identify"], function(identify) {
                            identify.identify(event);
                        });
                    } else {
                        if (method == "selectPoly")
                            findPolygon(event);
                        else 
                            alert("To do !");
                    }
                });
            },

            addLayer: function (TOC_Id) {
                let layerInfo = getLayerInfo(TOC_Id);

                let imageServiceLayer = new ArcGISImageServiceLayer(
                        layerInfo.url, {id: layerInfo.logicalName});
                imageServiceLayer.setDefinitionExpression(layerInfo.condition2);
        
                CM2.map.addLayer(imageServiceLayer);
            },

            removeLayer: function (TOC_Id) {
                let layerInfo = getLayerInfo(TOC_Id);
                let layer = CM2.map.getLayer(layerInfo.logicalName);
                CM2.map.removeLayer(layer);
            },

            addFeatureLayer: function (TOC_Id) {
                let layerInfo = getFeatureLayerInfo(TOC_Id);

                let featureLayer = new FeatureLayer(layerInfo.url, 
                    {id: layerInfo.logicalName, outFields: ["*"]} );
                if (layerInfo.symbol) {
                    let sfs = new SimpleFillSymbol(layerInfo.symbol);
                    let renderer = new SimpleRenderer(sfs);
                    featureLayer.setRenderer(renderer);
                }
                CM2.map.addLayer(featureLayer);
            },
            
            removeFeatureLayer: function (TOC_Id) {
                let layerInfo = getFeatureLayerInfo(TOC_Id);
                let layer = CM2.map.getLayer(layerInfo.logicalName);
                CM2.map.removeLayer(layer);
            },

            changeBoundaryLayer: function () {
                let lyr;
                let map = CM2.map;

                for (let i = 0; i < map.graphicsLayerIds.length; i++) {
                    lyr = map.getLayer(map.graphicsLayerIds[i]);
                    if (lyr.url.indexOf("Boundaries") > 0) {
                        map.removeLayer(lyr);
                        break;
                    }
                }

                let TOCLayerId = "TOC_" +$("#lboBoundaryLayer option:selected").val();
                this.addFeatureLayer(TOCLayerId);

                $(".TOC_FeatureLayer").each(function(idx, elem) {
                    elem.checked = elem.id == TOCLayerId;
                });
            },

            addGraphic: function (geometry, symbol, id) {
                addGraphic(geometry, symbol, id);
            }

        }

        function removeGraphic(graphicId) {
            let graphics = CM2.map.graphics.graphics;
            for (let i = 0; i < graphics.length; i++) {
                if (graphics[i].id == graphicId)
                    CM2.map.graphics.remove(graphics[i]);
            }
        }

        function findGraphic(graphicId) {
            let graphics = CM2.map.graphics.graphics;
            for (let i = 0; i < graphics.length; i++) {
                if (graphics[i].id == graphicId)
                    return graphics[i];
            }
        }
    
        function addGraphic(geometry, symbol, id) {
            removeGraphic(id);
            let graphic = new Graphic(geometry, symbol);
            graphic.id = "Poly2Process";
            CM2.map.graphics.add(graphic);
            return graphic;
        }
    
        function findPolygon(event) {
            let selMethod = $('input[name=rdoPolygonMethod]:checked').val();
            if (selMethod != "selectPoly") {
                alert(selMethod + ": To do!");
                return;
            }

            let lyrId = $('#lboBoundaryLayer').find(":selected").val();
            let layerInfo = getFeatureLayerInfo("TOC_" + lyrId);
        
            let query = new Query();
            query.returnGeometry = true;
            query.outFields = ['*'];
            query.geometry = event.mapPoint;
            let queryTask = new QueryTask(layerInfo.url);
            queryTask.execute(query, function(results) {
                let feat = results.features[0];
                let sfs = new SimpleFillSymbol(layerInfo.hiliteSymbol);
                let graphic = addGraphic(feat.geometry, sfs, "Poly2Process");
                graphic.attributes = {layer: lyrId, uid: feat.attributes[lyrId + "UID"]}

                updateTopSubTitle();

                computeResults();
            });
        }

        function computeResults() {
            var aLyrs = ["CAR", "CD", "CCS"];
            var sBND = $('#lboBoundaryLayer').val();
            var query = new Query();
            query.returnGeometry = true;
            //query.where = sBND + "UID = '" + CM2.map.graphics.graphiscs[0].attributes.uid + "'";
            query.where = sBND + "UID = '" + findGraphic("Poly2Process").attributes.uid + "'";
            //var url = CM2.urls.boundaries + aLyrs.indexOf(sBND);
            var url = CM2.servicesJSON.servers["IGCMBoundaries"] + aLyrs.indexOf(sBND);
            var queryTask = new QueryTask(url);
            queryTask.execute(query, computeResults2);
        }
        function computeResults2(results) {
            let geometry = results.features[0].geometry;
            CM2.currentGeometry = geometry;
            let post_data = {
                geometryType: "esriGeometryPolygon",
                geometry: JSON.stringify(geometry.toJson()),
                f: "pjson"                    
            };

            let aPromises = new Array();
            let crop = $('#lboCropType').find(":selected").val();

            for (let i = 0; i < CM2.aLayersInfo.length; i++) {
                let lyrInfo = CM2.aLayersInfo[i];
                if (byId(lyrInfo.domId))
                    byId(lyrInfo.domId).innerHTML = "&nbsp;";

                let month = $('#lboMonth').find(":selected").val();
                lyrInfo.date = CM2.servicesJSON.dates[month][lyrInfo.tDate];
                lyrInfo.condition2 = lyrInfo.condition.replace("?????", lyrInfo.date)

                if ( lyrInfo.logicalName == "historical_yield" ||
                     lyrInfo.logicalName.indexOf("forecasted") == 0)
                    lyrInfo.condition2 = string.substitute(lyrInfo.condition2, [crop]);

                lyrInfo.skip = false;
                if (lyrInfo.logicalName == "historical_yield")
                    lyrInfo.skip = true;
                if (lyrInfo.logicalName == "agclimate_ch" && !(crop=="corngr" || crop=="soybns"))
                    lyrInfo.skip = true;
                if (lyrInfo.logicalName == "agclimate_gd" &&  (crop=="corngr" || crop=="soybns"))
                    lyrInfo.skip = true;

                if (!lyrInfo.skip) {
                    if (!lyrInfo.group)
                        lyrInfo.skip = true;
                    else
                        if (!byId(lyrInfo.group).checked)
                            lyrInfo.skip = true;
                }

                console.log('  ' + lyrInfo.url + '\n  ' + lyrInfo.condition2);
                url = proxyUrl + "?" + lyrInfo.url  + "/computeStatisticsHistograms";
                post_data.mosaicRule =  JSON.stringify({
                    mosaicMethod: "esriMosaicNone",
                    where: lyrInfo.condition2
                });

                let xhrRes = xhr(url, {
                    method: "POST",
                    data: post_data,
                    timeout: 60000,
                    handleAs: "json"
                });

                if (!lyrInfo.skip) 
                    aPromises.push(xhrRes);
                else
                    aPromises.push("skip");
            }
            CM2.map.setMapCursor("wait");
            document.body.style.cursor = "wait";

            promiseAll(aPromises).then(function (aResults) {
                CM2.map.setMapCursor("auto");
                document.body.style.cursor = "auto";
                let msg = "";

                console.log("ARESULTS");
                for (let i = 0; i < aResults.length; i++) {
                    let mean = null;
                    if (aResults[i].statistics) {
                        console.log('', CM2.aLayersInfo[i].layer);
                        mean = precisionRound(aResults[i].statistics[0].mean, 2);
                        if (CM2.aLayersInfo[i].domId) {
                            let description = getDescription(CM2.aLayersInfo[i], mean);
                            byId(CM2.aLayersInfo[i].domId).innerHTML = description;
                        }
                        CM2.aLayersInfo[i].histogram = aResults[i].histograms[0].counts;
                        CM2.aLayersInfo[i].statistics = aResults[i].statistics[0];
                    } else {
                        if (aResults[i] != "skip") {
                            msg += CM2.aLayersInfo[i].layer + '\n' +
                                CM2.aLayersInfo[i].condition2 + '\n' +
                                aResults[i].error.message + '\n\n';
                            byId(CM2.aLayersInfo[i].domId).innerHTML = "No Data";
                        }
                    }   
                }
                if (msg != "")
                    alert(msg);

                byId("paneSummaryResults").style.display = "block";
            });
                
        }
        
        function setRasterFunction(results) {
            let regionCode = "";  // National
            if (results) { // Region found
                regionCode = results.features[0].attributes.region;
                regionCode = regionCode.substr(0,2).toLowerCase() + "_";
            }
            let layer = CM2.map.getLayer("agclimate_ac");
            if (layer) {
                let functionName = regionCode + "ac_gs";
                let rasterFunction = new RasterFunction()
                rasterFunction.functionName = functionName;
                layer.setRenderingRule(rasterFunction);
            }
        };

        function getLayerInfo(TOClayerId) {
            let layerInfo = null;
            for (let i = 0; i < CM2.aLayersInfo.length; i++) {
                if (CM2.aLayersInfo[i].tocId == TOClayerId) {
                    return CM2.aLayersInfo[i];
                }
            }
        };

        function getFeatureLayerInfo(TOClayerId) {
            let layerInfo = null;
            for (let i = 0; i < CM2.aFeatureLayersInfo.length; i++) {
                if (CM2.aFeatureLayersInfo[i].tocId == TOClayerId) {
                    return CM2.aFeatureLayersInfo[i];
                }
            }
        };

        function precisionRound(number, precision) {
            let factor = Math.pow(10, precision);
            return Math.round(number * factor) / factor;
        };

    }
);

function getDescription(layerInfo, value)  {
    //console.log(layerInfo.logicalName, value);

    let crop = $('#lboCropType').find(":selected").val();
    let labels;
    switch (layerInfo.logicalName) {
        case "crop_stage":
            if (crop == "corngr" || crop == "soybns")
                labels = layerInfo.labels.corngr_soybns;
            else
                labels = layerInfo.labels.small_grains;
            break;
        default:
            labels = layerInfo.labels;
            break;
    }

    let description = null;
    let category = null;
    switch (layerInfo.logicalName) {
        case "forecasted_CPI":
            description = value + ' ???';
            break;
        case "forecasted_high90":
        case "forecasted_median":
        case "forecasted_low10":
            description = value + ' Kilograms by hectare';
            break;
        case "CDM":
        case "crop_stage":
            let idx = Math.round(value);
            description = labels[idx + 1] + ' (' + value + ')';
            break;
            // heat, precipitation
        case "agclimate_ac":
            description = value + ' mm';
            break;
        case "agclimate_gd":
            description = value + ' GDD';
            break;
        case "agclimate_ch":
            description = value + ' CHU';
            break;
        case "ndvi":
            if (value < 0) category = 0;
            if (value== 0) category = 1;
            if (value > 0) category = 2;
            description = labels[category] + ' (' + value + ')';
            break;
        case "stress_index":
            //if (value >   0 && value < 25) category = 1;
            if               (value < 25) category = 1;
            if (value >= 25 && value < 40) category = 2;
            if (value >= 40 && value < 60) category = 3;
            if (value >= 60             ) category = 4;
            //if (value >= 60 && value <100) category = 4;
            description = labels[category] + ' (' + value + ')';
            break;
        default:
            description = value;
            break;
    }
    return description;
}
