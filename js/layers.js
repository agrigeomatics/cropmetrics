define([
    "esri/Map",
    "esri/views/MapView",
    "esri/layers/GroupLayer", //can we use dynamic loading?
    "esri/layers/FeatureLayer", // for these
    "esri/layers/ImageryLayer", //
    "esri/layers/MapImageLayer", //
    "esri/layers/support/RasterFunction",
    "config/layers.js" //
],function (Map,
            MapView,
            GroupLayer,
            FeatureLayer,
            ImageryLayer,
            MapImageLayer,
            RasterFunction,layerConfig) {

  var layers = layerConfig.layers;
  var groupLayers = layerConfig.groupLayers;

  function newLayer(properties) {
    switch(properties.typeString) {
      case "imagery":
        layer = new ImageryLayer(properties);
        return layer;
      case "map-image":
        layer = new MapImageLayer(properties);
        return layer;
      case "feature":
        layer = new FeatureLayer(properties);
        return layer;
      case "group":
        layer = new GroupLayer(properties);
        return layer;
      default:
        return null //better error handling than this
    }
  }

  function getGroupLayer (groupname,map) {
    var foundLayer = map.allLayers.find(function(layer) {
      return layer.title === groupname;
    });
    return foundLayer;
  }

  function addLayer (properties,map) {
    layer = newLayer(properties);
    if (properties.groupLayer) {
      lyrGroup = getGroupLayer(properties.groupLayer,map);
      lyrGroup.add(layer);
    }
  }

  return {
    //these are just dump groups
    initMapLayers: function(map) {
      layerConfig.layerGroups.reverse();  //so that order in config is same as map.
      layerConfig.layerGroups.forEach(function (properties) {
        group = new GroupLayer(properties);
        map.add(group);
      });
      layerConfig.layers.reverse(); //so that order in config is same as map.
      layerConfig.layers.forEach(function(properties){
        addLayer(properties, map);
      });
    },

    getLayerByTitle: function (title,map) {
      var foundLayer = map.allLayers.filter(function(layer) {
        return layer.title === title;
      });
      return foundLayer.items[0];
    },

    getCropMetricsLayers: function(map) {
      var foundLayers = map.allLayers.filter(function(layer){
        if (layer.cropMetrics && layer.cropMetrics.enabled) {
          return layer.cropMetrics.enabled === true;
        }

      });
      return foundLayers;
    }
  };


});