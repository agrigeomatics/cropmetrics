function handleEvents() {

    // paneLeft Criterias
    $('#lboCropType').change(function(evt) {
        let crop = $('#lboCropType').find(":selected").val();
        adjustGDD_CHU(crop);
    });
    $("#lboBoundaryLayer").change(function(evt) {
        require(["CM2/js/mapManager"], function(mapManager) {
            mapManager.changeBoundaryLayer();
        });
    });
    $("#btnTabularFormat").click(function() {
        alert("To Do!");
    });
    $("#btnTabularFormat").click(function() {
        alert("To Do!");
    });

    $("#formResults2Compute input:checkbox").change(function() {
        let elem = $(this)[0];
        let divId = elem.id.replace('chk', 'div');
        byId(divId).style.display = elem.checked ? "block" : "none";
        //console.log("Change: " + name + " to " + check);
    });

    $("input[name=rdoPolygonMethod]").click(function() {
        if (this.value == "rdoIdentify") {
            $("#tabsMainBottom").tabs("disable", 0);
            $("#tabsMainBottom").tabs("disable", 1);
            $("#tabsMainBottom").tabs("enable", 2);
            $("#tabsMainBottom").tabs( "option", "active", 2);
        } else {
            $("#tabsMainBottom").tabs("disable", 2);
            $("#tabsMainBottom").tabs("enable", 0);
            $("#tabsMainBottom").tabs("disable", 1);
            $("#tabsMainBottom").tabs( "option", "active", 0);
        }
    });

    $(".partOfSubTitle").each(function(index, elem) {
        elem.onchange = function() {
            updateTopSubTitle();
        };
    });

    // paneLeft TOC
    $(".TOC_Layer").click(function(evt) {
        require(["CM2/js/mapManager"], function(mapManager) {
            if (evt.target.checked)
                mapManager.addLayer(evt.target.id);
            else
                mapManager.removeLayer(evt.target.id);
        });
    });
    $(".TOC_FeatureLayer").click(function(evt) {
        require(["CM2/js/mapManager"], function(mapManager) {
            if (evt.target.checked)
                mapManager.addFeatureLayer(evt.target.id);
            else
                mapManager.removeFeatureLayer(evt.target.id);
        });
    });

    // paneMap
    $("#btnExpandReduce").click(function(evt) {
        let elem;
        let src = evt.target.getAttribute("src");
        if (src.indexOf("expand") > 0) {
            elem = byId("paneRight").parentElement;
            $(elem).removeClass("col-md-8 col-lg-8")
                     .addClass("col-md-11 col-lg-11");
            byId("divMap").style.height = "6in";

            elem = byId("tabsMainLeft").parentElement;
            $(elem).removeClass("col-md-4 col-lg-4")
                      .addClass("col-md-1 col-lg-1");
            $(".formsLeft").each(function( index, elem ) {
                elem.style.display = "none";
            });

            evt.target.setAttribute("src", src.replace("expand", "reduce"));
        } else {
            elem = byId("paneRight").parentElement;
            $(elem).removeClass("col-md-11 col-lg-11")
                       .addClass("col-md-8 col-lg-8");
            byId("divMap").style.height = "4.5in";

            elem = byId("tabsMainLeft").parentElement;
            $(elem).removeClass("col-md-1 col-lg-1")
                      .addClass("col-md-4 col-lg-4");
            $(".formsLeft").each(function( index, elem ) {
                elem.style.display = "block";
            });

            evt.target.setAttribute("src", src.replace("reduce", "expand"));
        }
    });

    // paneBottom GraphsResults
    $(".aChart").click(function(evt) {
        require(["CM2/js/chartsManager"], function(chartsManager) {
            $("#tabsMainBottom").tabs("enable", 1);
            $("#tabsMainBottom").tabs( "option", "active", 1);
            chartsManager.createCharts(evt.target.parentElement.id);
        });
        window.setTimeout(function() {
            var wbCont_top = byId("wb-cont").getBoundingClientRect().top;
            window.scrollBy(0,  wbCont_top + 25);
        }, 500);
    });

    // paneBottom SummaryResults
    $( "input[name=graphTypeChart]").on("click",  function() {
        require(["CM2/js/chartsManager"], function(chartsManager) {
            chartsManager.computeCharts();
        });
    });
    $( "input[name=graphTypeData]").on("click",  function() {
        require(["CM2/js/chartsManager"], function(chartsManager) {
            chartsManager.computeCharts();
        });
    });
}

function updateTopSubTitle() {
    let sTopSubTitle = "2017";
    sTopSubTitle += ', ' + $("#lboMonth option:selected").text().substr(3);
    sTopSubTitle += ', ' + $("#lboCropType option:selected").text();
    let rdoPolygonMethod = $('input[name=rdoPolygonMethod]:checked').val();
    if (rdoPolygonMethod == "selectPoly") {
        let attrs = CM2.map.graphics.graphics[0].attributes;
        if (attrs && attrs.uid)
            sTopSubTitle += ', ' + $("#lboBoundaryLayer option:selected").text() + " #" + attrs.uid;
    } else
        sTopSubTitle += ", User defined Polygon";

    byId("topSubTitle").innerHTML = "Statistics and Forecast for: " + sTopSubTitle;

}

function adjustGDD_CHU(crop) {
    if (crop == "corngr" || crop == "soybns") {
        $("#TOC_NotCornSoybeans input")[0].disabled = true;
        $("#TOC_NotCornSoybeans label")[0].style.color = 'grey';
        $("#TOC_CornSoybeans input")[0].disabled = false;
        $("#TOC_CornSoybeans label")[0].style.color = 'black';
    } else {
        $("#TOC_CornSoybeans input")[0].disabled = true;
        $("#TOC_CornSoybeans label")[0].style.color = 'grey';
        $("#TOC_NotCornSoybeans input")[0].disabled = false;
        $("#TOC_NotCornSoybeans label")[0].style.color = 'black';
    }
}

function genLayersInfo() {
    $.getJSON("config/services.json", function(data) {
        CM2.aLayersInfo = [];
        let crop = data.init.crop;

        for (let prop in data.logicalLayers) {
            //console.log(prop);
            let lyrInfo = data.logicalLayers[prop];
            lyrInfo.logicalName = prop;
            let month = data.init.month; // let month = $('#lboMonth').find(":selected").val();
            lyrInfo.date = data.dates[month][lyrInfo.tDate];
            lyrInfo.condition2 = lyrInfo.condition.replace("?????", lyrInfo.date)
            let url = lyrInfo.server;
            for (let prop2 in data.servers) {
                if (prop2 == url) {
                    url = data.servers[prop2] + lyrInfo.layer;
                    lyrInfo.url = url + "/ImageServer";
                    break;
                }
            }

            if ( lyrInfo.logicalName == "historical_yield" ||
                lyrInfo.logicalName.indexOf("forecasted") == 0)
                lyrInfo.condition2 = lyrInfo.condition2.replace('${0}', crop);

            CM2.aLayersInfo.push(lyrInfo);
        }

        let layerInfo = null;
        for (let prop in data.featureLayers) {
            layerInfo = data.featureLayers[prop];
            layerInfo.logicalName = prop;
            layerInfo.url = data.servers.IGCMBoundaries + layerInfo.layer;
            CM2.aFeatureLayersInfo.push(layerInfo);
        }

        CM2.servicesJSON = data;
    })
}

function initApp() {

    $("#tabsMainLeft").tabs();
    byId('tabCriterias').appendChild(byId('formCriterias'));
    byId('tabTOC').appendChild(byId('formTOC'));
    byId('tabLegend').appendChild(byId('formLegend'));

    $("#tabsMainBottom").tabs();
    $( "#tabsMainBottom" ).tabs( "disable", 1);
    $( "#tabsMainBottom" ).tabs( "disable", 2);
    byId('tabSummaryResults').appendChild(byId('paneSummaryResults'));
    byId('tabGraphsResults').appendChild(byId('paneGraphsResults'));
    byId('tabIdentifyResults').appendChild(byId('paneIdentifyResults'));

    window.setTimeout(function() {
        let crop = $('#lboCropType').find(":selected").val();
        adjustGDD_CHU(crop);

        require(["CM2/js/mapManager"], function(mapManager) {
            mapManager.changeBoundaryLayer();
        });

        //updateTopSubTitle();
    }, 1000);

    handleEvents();
}
