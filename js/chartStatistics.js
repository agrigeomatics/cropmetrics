define([
    "chartjs",
], function (Chart) {

    var config = {
        type: "bar",
        options: {
            legend: {
                display: false
            },
            responsive: true,
            scales: {
                xAxes: [{
                    barPercentage: 1.0,
                    categoryPercentage: 1.0,
                    display: true,
                    gridLines: {
                        display: false
                    },
                    scaleLabel: {
                        display: true,
                        labelString: ""
                    },
                    position: 'bottom',
                    ticks: {
                        autoSkip: false
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    };

    function match(value, classes) {
        for (i in classes) {
            if (classes[i][0] <= value == value < classes[i][1]) {
                return classes[i];
            }
        }
    }
    function matchIndex(value, classes) {
        for (i in classes) {
            if (classes[i][0] <= value == value < classes[i][1]) {
                return i;
            }
        }
    }

    function map_range(value, low1, high1, low2, high2) {
        return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
    }


    //public ones.
    return {

        makeHistogramChart: function (lyr, ctx, statistics, params = null) {

            var min = statistics.statistics[0].min;
            var max = statistics.statistics[0].max;

            //Need a deep copy
            var classes = JSON.parse(JSON.stringify(lyr.cropMetrics.classes));

            //build up counts in classes table
            for (i in classes) {
                classes[i].push(0);
            }
            for (i in statistics.histograms[0].counts) {
                value = map_range(i,0,255,min,max);
                count = statistics.histograms[0].counts[i];

                classes[matchIndex(value, classes)][4] += count;
            }

            config.options.scales.xAxes[0].scaleLabel.labelString = lyr.title;

            config.data = {
                labels: classes.map(function(x) {return x[3]}),
                datasets: [{
                    label: false,
                    data: classes.map(function(x) {return x[4]}),
                    backgroundColor: classes.map(function(x) {return x[2]}),
                    borderWidth: 0
                }],
            };

            if (params && params.xAxesCallback) {
                config.options.scales.xAxes.ticks.callback = params.xAxesCallback;
            }

            if (window.myLine = new Chart(ctx, config)) {
                return true;
            } else {
                return false;
            }
        }

    };
});