var app = null;

dojoConfig = {
  parseOnLoad: true,
  //cacheBust: "20130618",
  async: true,
  //baseUrl: "/",
  locale: location.search.match(/language=([\w\-]+)/i) ? (RegExp.$1).toLowerCase() : 'en',
  packages: [
    { name: "app", location: location.pathname.replace(/\/[^\/]+$/, "/js/")}, //location.pathname.replace(/\/[^\/]+$/, "/js/")},
    { name: "config", location: location.pathname.replace(/\/[^\/]+$/,"/config/")},
    { name: "chartjs", location: "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/", main: "Chart.min" }
  ]
};

$(document).ready(function () {
  $(document).on("wb-ready.wb", function (event) {

    // load dojo/esri API
    $.getScript("https://js.arcgis.com/4.8/",
      function () {

        require([
          "dojo/dom-construct",
          "esri/config",
          "esri/Graphic",
          "esri/Map",
          "esri/views/MapView",
          "esri/layers/GroupLayer",
          "esri/layers/FeatureLayer",
          "esri/layers/ImageryLayer",
          "esri/layers/MapImageLayer",
          "esri/layers/support/RasterFunction",
          "esri/tasks/IdentifyTask",
          "esri/tasks/support/IdentifyParameters",
          "esri/tasks/Locator",
          "esri/layers/GraphicsLayer",
          "esri/tasks/QueryTask",
          "esri/tasks/support/Query",
          "esri/widgets/Expand",
          "esri/widgets/BasemapGallery",
          "esri/widgets/Feature",
          "esri/widgets/Home",
          "esri/widgets/Legend",
          "esri/widgets/LayerList",
          "esri/widgets/Popup",
          "esri/widgets/Search",
          "esri/core/watchUtils",
          "app/utilities",
          "app/chartStatistics",
          "app/layers",
          "config/unit_converter.js",
          "dojo/_base/array",
          "esri/request",
          "dojo/domReady!"
        ], function (
          domConstruct,
          esriConfig,
          Graphic,
          Map,
          MapView,
          GroupLayer,
          FeatureLayer,
          ImageryLayer,
          MapImageLayer,
          RasterFunction,
          IdentifyTask,
          IdentifyParameters,
          Locator,
          GraphicsLayer,
          QueryTask,
          Query,
          Expand,
          BasemapGallery,
          Feature,
          Home,
          Legend,
          LayerList,
          Popup,
          Search,
          watchUtils,
          utilities, chartStatistics, layers, unit_converter, arrayUtils, esriRequest) {

          esriConfig.request.proxyUrl = "/proxy/proxy.php";
          esriConfig.request.forceProxy = false;

          //Extend Map, necessary for watching custom properties, taking advantage of Accessor
          var CropMap = Map.createSubclass({
            declaredClass: 'esri.Map',

            constructor: function() {
              this.cropname = "",
              this.cropdate = ""
            },
            properties: {
              cropname: {},
              cropdate: {}
            }
          });

          var map = new CropMap ({
            basemap: "dark-gray",
            cropname: 'barley',
            cropdate: '2017-07-15'
          });

          var view = new MapView({
            container: "viewDiv", // Reference to the DOM node that will contain the view
            map: map, // References the map object created in step 3
            center: [-90,50],
            zoom: 5,
            popup: {
              dockEnabled: false,
              dockOptions: {
                buttonEnabled: false,
                breakpoint: false,
                position: "top-right"
              }
            }
          });



          function updateLayerRendering(lyr,value) {
            var fn = lyr.renderingFunction;
            var newRendering = fn(value);
            lyr.renderingRule.functionName = newRendering[0];
            lyr.title = lyr.title.replace(/ *\([^]*\) */g," (" + newRendering[1] + ")");
          }

          function updateLayerExpression(lyr,replacements) {
            newExpression = lyr.expressionTemplate;
            $.each(replacements, function(key,value) {
              newExpression = newExpression.replace("@@@"+key+"@@@",value)
            });
            lyr.definitionExpression = newExpression;
          }

          view.map.watch(['cropname','cropdate'], function(newValue,oldValue,property,object){

            //Cannot change only rendering rule on a layer, it will not refresh.
            // change also the mosaic rule and it will refresh, redrawing both
            for (let i in map.allLayers.items) {
              let lyr = map.allLayers.items[i];

              if (property == 'cropname' && lyr.renderingFunction) {
                updateLayerRendering(lyr,newValue);
              } else if (lyr.sublayers) {
                for (let i = 0; i < lyr.sublayers.items.length; i++ ) {
                  let slyr = lyr.sublayers.items[i];
                  if (property == 'cropname' && slyr.renderingFunction) {
                    updateLayerRendering(lyr,newValue);
                  }
                }
              }

              replacements = {
                date: map.cropdate,
                startDate: utilities.getLastDate(map.cropdate, lyr.frequency),
                cropname: map.cropname
              };

              if (lyr.expressionTemplate) {
                updateLayerExpression(lyr,replacements);
              } else if (lyr.sublayers) {
                for (let i = 0; i < lyr.sublayers.items.length; i++ ) {
                  let slyr = lyr.sublayers.items[i];
                  if (slyr.expressionTemplate) {
                    updateLayerExpression(slyr,replacements);
                  }
                }
              }

            }
          });

          /*
          These functions are for formatting query results.  They shouldn't be here.
          */
          //BETTER: need to explicitly declare formatters, can't reference in popup template
          convertCropUnit = unit_converter.convertCropUnit;


          droughtLabel = function (value,key,data) {
            switch (value) {
              case 0:
                return 'D0 - Abnormally dry';
              case 1:
                return 'D1 - Moderate drought';
              case 2:
                return 'D2 - Severe drought';
              case 3:
                return 'D3 - Extreme drought';
              case 4:
                return 'D4 - Exceptional drought';
              case 5:
                return 'Drought not analyzed';
            }

          };

          /*
          HTML object events
          */
          $('#selectCropType').change(function(event) {
            map.set('cropname', this.value);
          });

          $('#selectDate').change(function(event) {
            map.set('cropdate', this.value);
          });

          $("#deleteQueryResult").on("click", function (event) {
            var selBox = $("#queryResultsList").get(0);
            var deleteIndex = selBox.selectedIndex;
            var deleteId = selBox.options[deleteIndex].id;
            if (deleteId) {
              var newIndex = 0;
              $("#queryResultsList #"+deleteId).remove();  //not working
              selBox.options[newIndex].selected = true;
              var newId = selBox.options[newIndex].id
              $("#queryResultDisplay #"+deleteId).remove();
              if (newId) {
                $("#queryResultDisplay #"+newId).show();
              }
            }
          });

          //TODO: also need a function that will load report based on data attached to select element


          $("#queryResultsList").on("change", function(event) {
            $("#queryResultDisplay div.result").not("#"+this[this.selectedIndex].id).hide();
            $("#queryResultDisplay #"+this[this.selectedIndex].id).show();
          });



          layers.initMapLayers(map);

          /**
           * Widgets
           */
          var basemapGallery = new BasemapGallery({
            view: view,
            container: document.createElement("div")
          });

          var bgExpand = new Expand({
            view: view,
            content: basemapGallery.domNode,
            expandIconClass: "esri-icon-basemap"
          });
          var homeWidget = new Home({
            view: view
          });

          var legend = new Legend({
            view: view,
            container: "legendDiv"
          });

          function defineActions(event) {

            var item = event.item;

              // An array of objects defining actions to place in the LayerList.
              // By making this array two-dimensional, you can separate similar
              // actions into separate groups with a breaking line.

              if (item.layer.layers) {
                item.actionsSections = [{
                  title: "Increase opacity",
                  className: "esri-icon-up",
                  id: "increase-opacity"
                }, {
                  title: "Decrease opacity",
                  className: "esri-icon-down",
                  id: "decrease-opacity"
                }];
              } else {
                 item.actionsSections = [
                [{
                  title: "Go to full extent",
                  className: "esri-icon-zoom-out-fixed",
                  id: "full-extent"
                }, {
                  title: "Layer information",
                  className: "esri-icon-description",
                  id: "information"
                }],
                [{
                  title: "Increase opacity",
                  className: "esri-icon-up",
                  id: "increase-opacity"
                }, {
                  title: "Decrease opacity",
                  className: "esri-icon-down",
                  id: "decrease-opacity"
                }]
              ];
              }


          }

          //need to test
          var layerList = new LayerList({
            view: view,
            container: "layerlistDiv",
            listItemCreatedFunction: defineActions
          });

          layerList.on("trigger-action", function(event) {

            var layer = event.item.layer;
            var id = event.action.id;

            if (layer && id === "full-extent") {
              view.goTo(layer.fullExtent);
            } else if (layer && id === "information") {
              window.open(layer.url);
            } else if (id === "increase-opacity") {
              if (layer.opacity < 1) {
                layer.opacity += 0.25;
              }
            } else if (id === "decrease-opacity") {
              if (layer.opacity > 0) {
                layer.opacity -= 0.25;
              }
            }
          });

          //consider grouping
          view.ui.add(homeWidget, "top-left");
          view.ui.add(bgExpand, "top-left");


          var searchWidget = new Search({
            view: view,
            sources: [
              {
                featureLayer: {
                  url: "http://devproxyint.agr.gc.ca/atlas/rest/services/mapservices/statistics_canada_census_boundaries/MapServer/1",
                  popupTemplate: {
                    title: "Census of Agriculture Region",
                    content: "Region ID: {CARUID} <br>Region Name: {CARENAME}"
                  }
                },
                searchFields: ["CARUID"],
                displayField: "CARENAME",
                exactMatch: false,
                outFields: ["*"],
                name: "Census of Agriculture Regions",
                placeholder: "example: 4702"
              },
              {
                locator: new Locator({ url: "//geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/"}),
                countryCode: "CA",
                singleLineFieldName: "SingleLine",
                outFields: ["Addr_type"],
                name: "ESRI World Geocoder",
                localSearchOptions: {
                  minScale: 300000,
                  distance: 50000
                },
                placeholder: "example: regina",
                resultSymbol: {
                   type: "picture-marker",  // autocasts as new PictureMarkerSymbol()
                   url: "https://js.arcgis.com/4.7/esri/images/search/search-symbol-32.svg",
                   size: 24,
                   width: 24,
                   height: 24,
                   xoffset: 0,
                   yoffset: 0
               }
              }
            ]
          });

          // Add the search widget to the top right corner of the view
          view.ui.add(searchWidget, {
            position: "top-right"
          });


          view.when(function(){

            view.on("click", setupViewEvents);

            //set up identify task here?

            view.on("mouse-wheel", function(evt){
              // prevents zooming with the mouse-wheel event
              evt.stopPropagation();
            });

          });

          function setupViewEvents(event) {
//            event.stopPropagation();
            var method = $('input[name=optQuery]:checked').val();
           //alert(method);
            switch(method) {
                case "drawPoint":  //this is really identify?
                  //event.stopPropagation();
                  alert("TODO: Draw Point");

                  break;
                case "drawRegion":
                  //event.stopPropagation();
                  alert("TODO: Draw Region");
                  break;
                case "selectRegion":
                  event.stopPropagation();
                  //if we do this here... make CAR only selectable layer.
                  //TODO: See this: https://developers.arcgis.com/javascript/latest/sample-code/sandbox/index.html?sample=tasks-identify
                  //TASK: setup identify task on CAR - in sample this was in view.on, but it will vary by radio...

                  carLayer = layers.getLayerByTitle("Census of Agriculture Region",map);

                  var params = new Query({
                    returnGeometry: true,
                    outFields: ["CARUID","CARENAME"],
                    spatialRelationship: "intersects",
                    geometry: event.mapPoint
                  });

                  var queryCarTask = new QueryTask({
                    url: carLayer.url+"/"+carLayer.allSublayers.items[0].id
                  });

                  document.getElementById("viewDiv").style.cursor = "wait";

                  queryCarTask.execute(params).then(function(response) {
                    var results = response.features;

                    return arrayUtils.map(results,function(result) {

                      var feature = result;
                      var layerName = result.layerName;

                      feature.layerId = carLayer.id;
                      feature.title = carLayer.title;
                      feature.popupTemplate = carLayer.allSublayers.items[0].popupTemplate;

                      return feature;

                    });

                  }).then(function(response) {

                    //FIXME: this could be cleaned up
                    for (r in response) {
                      var g = response[r];
                      //make a panel utility?
                      //pass it summary title, ID, returns dom node?
                      var myID = g.layerId+"_"+g.attributes.CARUID;
                      var titleBar = "CAR:" + g.attributes.CARUID +" ("+ g.attributes.CARENAME +")"
                      $("#queryResultDisplay div.result").not("#result_"+myID).hide();
                      $("#queryResultsList").prepend("<option selected='true' id='result_"+myID+"'>"+titleBar+"</option>");
                      $("#queryResultDisplay").prepend("<div id='result_"+myID+"' class='result' data-queryresult=''><h4>"+titleBar+"</h4><div id='car_details'></div></div>");
                      var child = $("#queryResultDisplay #result_"+myID+" #car_details").get(0);

                      var cropMetricsLayers = layers.getCropMetricsLayers(map);

                      cropMetricsLayers.forEach(function(lyr) {
                        //check for startup problems -
                        histoGeometry = g.geometry.toJSON();
                        histoWhere = {
                          mosaicMethod: "esriMosaicNone",
                          where: lyr.mosaicRule.where
                        };
                        histoDiv = document.createElement('div');
                        histoDiv.id = myID+'_'+lyr.id;
                        child.appendChild(histoDiv);

                        if (lyr.loadStatus == 'failed'){
                          histoDiv.innerHTML = "<strong>"+lyr.title + "</strong>: Load error.";
                        } else {
                          histoDiv.innerHTML = "<strong>"+lyr.title + "</strong>: Loading...";
                        //check for sublayers


                          histoRequestParams = {
                            query: {
                              geometry: JSON.stringify(histoGeometry),  //from result
                              geometryType: "esriGeometryPolygon",
                              f: "json",
                              //these need to be more generic
                              mosaicRule: JSON.stringify(histoWhere),

                            },
                            responseType: "json",
                            parentDivId: histoDiv.id,
                            layerId: lyr.id
                          };
                          if (lyr.renderingRule) {
                            histoRequestParams.renderingRule = lyr.renderingRule.functionName; //TODO: change to use a custom rule
                          }
                          return esriRequest(lyr.url+"/computeStatisticsHistograms", histoRequestParams).then(function(response){
                            container = document.getElementById(response.requestOptions.parentDivId);
                            container.innerHTML = "";

                            container.dataset.queryrequest = JSON.stringify(response);

                            g = new Graphic({
                              attributes: {
                                statistics: response.data.statistics[0],  //this was all the attributes.
                                mean: response.data.statistics[0].mean,
                                response: JSON.stringify(response.data)
                               },
                              geometry: null,
                              popupTemplate: {
                                title: lyr.title,
                                expressionInfos: [{
                                  name: "title",
                                  expression: "Text('"+lyr.title+"')",
                                },{
                                  name: "classLookup",
                                  expression: "Text('will show meaning based on legend.')"
                                },{
                                  name: 'compare',
                                  expression: "Text('compare to historical value')"
                                }],
                                //content: "<strong>{expression/title}:</strong> Mean={mean:NumberFormat(places:2)} <a href='#' data-layerid='"+lyr.id+"' class='esri-icon-chart' title='Click to see histogram of values' aria-label='Click to see histogram of values'></a>"
                                content: "<strong>{expression/title}:</strong> Mean={mean:NumberFormat(places:2)}"
                              }
                            });
                            mypopup = new Feature({
                              container: container,
                              graphic: g,
                              view: view,
                            });
                            //FIXME: could probably choose children with jquery a little better here.
                            // make up new data attributes for what type of link it is.  assign
                            // callback function to all 'a' children elelemnts

                            //l = document.getElementById("result_"+lyr.id);
                            //l.onclick = function(event) {
                            container.onclick = function(event) {
                              var id = event.currentTarget.id;
                              var el = document.getElementById(id);

                              //FIXME:
                              //since the target is the link, and the div several levels up is the true 'el'
                              // need to grab the parent matching.  use jquery parents()?

                              //maybe there's a better way overall.

                              var j = el.dataset.queryrequest;
                              var obj = JSON.parse(j);
                              var statistics = obj.data;
                              targetLayer = obj.requestOptions.layerId;

                              //TODO: also capture queried method.
                              // - may need to add into query paramaters.


                              var foundLayer = map.allLayers.find(function(layer) {
                                return layer.id === targetLayer;
                              });
                              var chart = $("#queryChart").get(0);
                              chart.innerHTML = "";
                              chartTitle = document.createElement("h4");
                              chartDescription = document.createElement("p");
                              //TODO: get better title from layers, and how built.
                              // might have to store with request options
                              // --- title.
                              // --- chart description???
                              // This layer, histogram analyzed using boundary of <layer/feature id>.  or <user defined area>
                              chartTitle.innerHTML = "Histogram of Values for "+foundLayer.title;
                              chartDescription.innerHTML = "subtitle";
                              chartDetail = document.createElement("canvas");
                              chart.appendChild(chartTitle);
                              chart.appendChild(chartDescription);
                              chart.appendChild(chartDetail);
                              ctx = chartDetail;
                              // TODO: need better error handling in module so it will through error in promise.

                              chartStatistics.makeHistogramChart (foundLayer, ctx, statistics)
                            }

                            document.getElementById("viewDiv").style.cursor = "auto";

                          }).catch(function(error) {
                            container = document.getElementById(error.details.requestOptions.parentDivId);
                            container.innerHTML = "<strong>"+lyr.title+":</strong> <span style='color: red'>Error: </span>"+error.message;
                            document.getElementById("viewDiv").style.cursor = "auto";
                          });


                        }

                      });
                    }

                  }).catch(function(err){
                    console.error("Promise rejected: ", err.message);
                  });

                  //setup part 2 of IdentifyTask
                  //execute.then function(response)
                    //  return a promise
                  //what's this? arrayUtils.map(results, function(result) {
                  //  - i think it's running a function on each result, but we should have just one.
                  // popup CAR identify.
                  // view.popup.open
                  //then?
                  //loop through all queryable layers.
                  // - prepare a QueryTask on them.
                  // - perhaps come up with my own template for this. At app level.
                  // - so that it can generic.  defines which layers are in report, and what values

                  //start with one.
                  //create querytask for accumulated precipitation
                  //also for historical 30 year average  precipitation
                  // want to get histogram of each.


                  // compare median values.
                  // depict if it's above or below 30 year average

                  //next - create a callable function for opening up chart for each histogram


                  //findPolygon(event);  //FROM CM2
                  //alert('select CAR Region');

                  //TODO: looking into this sample





                  // const graphic = response.results[0].graphic;
                  // var contDiv = jQuery("#queryResults");
                  // contDiv.classList.add("popup-container");

                  // f = new Feature({
                  //   graphic: graphic,
                  //   view: view,
                  //   container: contDiv
                  // });

                  break;
                case "queryPoint":
                  //using popup automatically.
                  //all layers are selectable.
                  break;
            }
          }

        });
      });

  });
});