/*
 * @external Geometry
 * @see {@link https://developers.arcgis.com/javascript/3/jsapi/geometry-amd.html Geometry}
 */

/*
 * @external Graphic
 * @see {@link https://developers.arcgis.com/javascript/3/jsapi/graphic-amd.html Graphic}
 */

/*
 * @external Symbol
 * @see {@link https://developers.arcgis.com/javascript/3/jsapi/symbol-amd.html Symbol}
 */

define([
    "dojo/_base/array",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/Deferred",
    "dojo/Evented",
    "dojo/on",
    "dojo/Stateful",
    "dojo/when",
    "dijit/Destroyable",

    "esri/toolbars/draw",
    "esri/layers/GraphicsLayer",
    "esri/graphic",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/tasks/query",
], function (array, declare, lang, Deferred, Evented, on, Stateful, when, Destroyable, Draw, GraphicsLayer,
    Graphic, SimpleMarkerSymbol, SimpleLineSymbol, SimpleFillSymbol, Query) {

        return declare([Stateful, Evented, Destroyable], {

            // ESRI draw toolbar
            toolbar: undefined,

            // Add an attribute on the graphic itself to identify which are selected
            // and to store the original symbol before it get selected
            _graphicAddedProperty: "_drawGraphicsWidgetData",

            // Add graphic in a graphic layer to isolate graphics created within this widget
            graphicsLayer: undefined,

            SELECTION_MODE_ADD_TO_SELECTION: "add",
            SELECTION_MODE_NEW_SELECTION: "new",
            selectionMode: this.SELECTION_MODE_NEW_SELECTION,

            DRAWING_MODE_ADD: "add",
            DRAWING_MODE_ONE: "one",
            drawingMode: this.DRAWING_MODE_ADD,

            selectDrawing: false,

            // Default Selection fill color
            selectionFillColor: [128, 255, 255, 0],
            selectionOutlineColor: [0, 255, 255, 1],

            selectLayer: false,
            featureLayer: null,

            popupManagerDesactivated: false,

            GEOMETRY_TYPE_POINT: Draw.POINT,
            GEOMETRY_TYPE_POLYGON: Draw.POLYGON,
            GEOMETRY_TYPE_RECTANGLE: Draw.RECTANGLE,

            promiseReady: null,

            handleFeatureSelection: null,


            constructor: function (params) {
                lang.mixin(this, params);
                this.own(this.watch("featureLayer", lang.hitch(this, "featureLayerChanged")));
                this.init();
            },

            init: function () {
                this.promiseReady = this._whenMapIsReady().then(lang.hitch(this, function () {

                    this.toolbar = new Draw(this.map);
                    this.own(this.toolbar.on("draw-complete", lang.hitch(this, this._onDrawComplete)));

                    if (this.activateOnLoad) {
                        this.activateDraw(this.activateOnLoad);
                    }
                }));
            },

            _whenMapIsReady: function () {
                var deferred = new Deferred();
                if (this.map.loaded) {
                    deferred.resolve(this.map);
                } else {
                    on.once(this.map, "load", lang.hitch(this, function () {
                        deferred.resolve(this.map);
                    }));
                }
                return deferred.promise;
            },

            hide: function () {
                if (this.graphicsLayer && this.graphicsLayer.visible) {
                    this.graphicsLayer.hide();
                }
                if (this.featureLayer && this.featureLayer.visible) {
                    this.featureLayer.hide();
                }
            },

            show: function () {
                if (this.graphicsLayer && !this.graphicsLayer.visible) {
                    this.graphicsLayer.show();
                }
                if (this.featureLayer && !this.featureLayer.visible) {
                    this.featureLayer.show();
                }
            },

            /**
             * @param  {Array.<external:Geometry>} geometries
             */
            addGraphics: function (geometries) {
                this.show();
                this._addGraphics(geometries);
            },

            featureLayerChanged: function (name, oldFeatureLayer, newFeatureLayer) {

                if (this.handleFeatureSelection) {
                    this.handleFeatureSelection.remove();
                    this.handleFeatureSelection = null;
                }

                if (oldFeatureLayer) {
                    oldFeatureLayer.clearSelection();
                    this.map.removeLayer(oldFeatureLayer);
                }
                if (newFeatureLayer) {
                    var selectionSymbol = this.createSelectionSymbol(newFeatureLayer);
                    newFeatureLayer.setSelectionSymbol(selectionSymbol);

                    if (!this.handleFeatureSelection) {
                        this.handleFeatureSelection = newFeatureLayer.on("selection-complete", lang.hitch(this, function (evt) {
                            this.emit("selection-complete", evt.features);
                        }));
                        this.own(this.handleFeatureSelection);
                    }

                    this.map.addLayer(newFeatureLayer);
                }
            },

            _activateDesactivatePopupManager: function (isChecked) {
                //jshint unused:false
            },

            destroy: function () {
                // Remove graphic layer from map
                this._removeGraphicLayer();

                // Remove feature layer from map
                this._removeFeatureLayer();

                // Remove the drawing toolbar
                this._removeDrawToolbar();
            },

            clear: function () {
                this._deleteAllGraphics();
            },

            _removeGraphicLayer: function () {
                if (typeof this.graphicsLayer !== "undefined") {
                    // Clear all graphics
                    this.graphicsLayer.clear();

                    this.map.removeLayer(this.graphicsLayer);
                }
                this.graphicsLayer = undefined;
            },

            _removeFeatureLayer: function () {
                if (this.featureLayer) {
                    // Clear all graphics
                    this.featureLayer.clearSelection();
                    this.map.removeLayer(this.featureLayer);
                }
                this.featureLayer = null;
            },

            _removeDrawToolbar: function () {
                this.toolbar.deactivate();
                this.toolbar = undefined;
            },

            onDrawComplete: function () {
                var graphics = [];
                if (typeof this.graphicsLayer !== "undefined") {
                    graphics = array.filter(this.graphicsLayer.graphics, function (graphic) {
                        return graphic.visible;
                    });
                }
                return graphics;
            },

            getSelectedGraphics: function () {
                return this._getSelectedGraphics();
            },

            /**
             * Create a graphic layer if it doesn't exist and add it to the map
             *
             */
            _createGraphicsLayerAddToMap: function () {
                // Create a graphic layer if it doesn't already exist
                if (typeof this.graphicsLayer === "undefined") {

                    // Create the Graphic Layer
                    this.graphicsLayer = new GraphicsLayer();

                    // Add it to the map
                    this.map.addLayer(this.graphicsLayer);
                }
            },

            _deleteAllGraphics: function () {
                if (typeof this.graphicsLayer !== "undefined") {
                    if (this.graphicsLayer.graphics.length > 0) {
                        this.graphicsLayer.clear();
                    }
                }
                if (this.featureLayer) {
                    if (this.featureLayer.getSelectedFeatures().length > 0) {
                        this.featureLayer.clearSelection();
                    }
                }
            },

            /**
             * Get all selected graphics
             */
            _getSelectedGraphics: function () {
                var selectedGraphics = [];
                if (typeof this.graphicsLayer !== "undefined") {
                    selectedGraphics = array.filter(this.graphicsLayer.graphics, lang.hitch(this, function (graphic) {
                        var isSelected = this._isGraphicSelected(graphic);
                        return isSelected;
                    }));
                }
                return selectedGraphics;
            },

            _createGraphic: function (geometry) {
                var symbol;
                switch (geometry.type) {
                    case "point":
                    case "multipoint":
                        symbol = new SimpleMarkerSymbol();
                        break;
                    case "polyline":
                        symbol = new SimpleLineSymbol();
                        break;
                    default:
                        symbol = new SimpleFillSymbol();
                        break;
                }
                var graphic = new Graphic(geometry, symbol);
                return graphic;
            },

            /**
             * Is the graphic selected by this tool. Uses an added property on the graphic.
             *
             * @param  {external:Graphic} graphic
             */
            _isGraphicSelected: function (graphic) {
                return (typeof graphic[this._graphicAddedProperty] !== "undefined" &&
                    typeof graphic[this._graphicAddedProperty].selection !== "undefined");
            },

            /**
             * Store the original symbol within a custom property added directly on the graphic.
             *
             * @param  {external:Graphic} graphic
             * @param  {external:Symbol} originalSymbol
             */
            _setOriginalSymbol: function (graphic, originalSymbol) {
                if (typeof graphic[this._graphicAddedProperty] === "undefined") {
                    graphic[this._graphicAddedProperty] = {
                        selection: {
                            originalSymbol: originalSymbol
                        }
                    };
                }
                else {
                    graphic[this._graphicAddedProperty].selection = {
                        originalSymbol: originalSymbol
                    };
                }
            },

            selectFeatures: function (geometry) {

                if (this.drawingMode === this.DRAWING_MODE_ONE) {
                    this._deleteAllGraphics();
                }

                // Create a graphic layer if it doesn't already exist
                var featureLayer = this.featureLayer;
                if (featureLayer) {
                    var param = new Query();
                    param.geometry = geometry;
                    param.returnGeometry = true;
                    return featureLayer.selectFeatures(param).promise;
                } else {
                    return when(null);
                }
            },

            _addGraphics: function (geometries) {
                var onDrawComplete = false;

                // Create a graphic layer if it doesn't already exist
                this._createGraphicsLayerAddToMap();

                // Automatic graphic selection when drawing
                if (this.selectDrawing) {
                    if (this.selectionMode === this.SELECTION_MODE_NEW_SELECTION) {
                        this._unselectGraphics();
                    }
                }

                if (this.drawingMode === this.DRAWING_MODE_ONE) {
                    this._deleteAllGraphics();
                }

                var selected = false;

                array.forEach(geometries, lang.hitch(this, function (geometry) {
                    // Create a graphic from the geometry
                    var graphic = this._createGraphic(geometry);

                    // Add graphic to the graphic layer
                    this.graphicsLayer.add(graphic);
                    onDrawComplete = true;

                    // Automatic graphic selection when drawing
                    if (this.selectDrawing) {
                        this._selectGraphic(graphic);
                        selected = true;
                    }
                }));

                if (selected) {
                    this.emit("selection-complete", this._getSelectedGraphics());
                }
                if (onDrawComplete) {
                    this.onDrawComplete();
                }
            },

            /**
             * Selects graphic when drawing is completed
             *
             * @param  {{geometry: external:Geometry}} evt
             */
            _onDrawComplete: function (evt) {
                if (typeof evt.geometry !== "undefined") {
                    this.map.showZoomSlider();

                    if (this.selectLayer) {
                        this.selectFeatures(evt.geometry);
                    }
                    else {
                        this._addGraphics([evt.geometry]);
                    }
                }
            },

            createSelectionSymbol: function (featureLayer) {
                // Clone the symbol to change some properties
                var newSymbol = lang.clone(featureLayer.renderer.getSymbol());

                // Change outline color
                newSymbol.outline.color.setColor(this.selectionOutlineColor);

                // Change outline width (x2)
                if (newSymbol.outline && typeof newSymbol.outline.width !== "undefined") {
                    newSymbol.outline.setWidth(newSymbol.outline.width * 2);
                }

                // Change symbol color
                newSymbol.color.setColor(this.selectionFillColor);

                return newSymbol;
            },

            /**
             * Change the graphic outline and fill color and increase the outline width (x2)
             *
             * @param  {external:Graphic} graphic The graphic to change symbology
             */
            _selectGraphic: function (graphic) {
                // Keep the original symbol (will use it when unselecting the graphic)
                var originalSymbol = lang.clone(graphic.symbol);
                // Clone the symbol to change some properties
                var newSymbol = lang.clone(graphic.symbol);

                // Change outline color
                newSymbol.outline.color.setColor(this.selectionOutlineColor);

                // Change outline width (x2)
                if (newSymbol.outline && typeof newSymbol.outline.width !== "undefined") {
                    newSymbol.outline.setWidth(newSymbol.outline.width * 2);
                }

                // Change symbol color
                newSymbol.color.setColor(this.selectionFillColor);

                // Modify symbol
                graphic.setSymbol(newSymbol);

                // Keep the original symbol (will use it when unselecting the graphic)
                this._setOriginalSymbol(graphic, originalSymbol);
            },

            /**
             * Unselect each selected graphics if they are selected
             *
             */
            _unselectGraphics: function () {
                var graphics = this._getSelectedGraphics();
                if (graphics.length > 0) {
                    // For each graphic
                    array.forEach(graphics, lang.hitch(this, function (graphic) {
                        // Unselect the graphic if it is selected
                        this._unselectGraphic(graphic);
                    }));
                }
            },

            /**
             * Unselect the given graphic if it is selected by setting back it's old symbol
             * and removing the selection added data.
             *
             * @param  {external:Graphic} graphic
             */
            _unselectGraphic: function (graphic) {
                if (typeof graphic[this._graphicAddedProperty] !== "undefined" &&
                    typeof graphic[this._graphicAddedProperty].selection !== "undefined" &&
                    typeof graphic[this._graphicAddedProperty].selection.originalSymbol !== "undefined") {

                    // The original symbol before it was selected
                    var originalSymbol = graphic[this._graphicAddedProperty].selection.originalSymbol;

                    // The the original symbol back on the graphic
                    graphic.setSymbol(originalSymbol);

                    // Remove selection added data
                    delete graphic[this._graphicAddedProperty].selection;
                }
            },

            /**
             * Desactivate the drawing functionality
             *
             */
            desactivateDraw: function () {
                if (this.map && this.map.popupManager) {
                    if (this.popupManagerDesactivated) {
                        this.popupManagerDesactivated = false;
                        this.map.setInfoWindowOnClick(true);
                    }
                    if (typeof this.toolbar !== "undefined") {
                        this.toolbar.deactivate();
                    }
                }

            },

            /**
             * Activate the drawing functionality
             *
             * @param  {string} geometryType The drawing geometry type
             */
            activateDraw: function (geometryType) {
                if (this.map.popupManager.enabled) {
                    this.popupManagerDesactivated = true;
                    this.map.setInfoWindowOnClick(false);
                }
                if (typeof this.toolbar !== "undefined") {
                    this.toolbar.activate(geometryType);
                }
            }

        });

    });
